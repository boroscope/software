#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import os
import glob
import subprocess

from setuptools import setup, find_packages
from pip._internal.req import parse_requirements


def get_version(cmd):
    output = subprocess.check_output(cmd, shell=True).strip()
    return output.decode('utf-8')


def get_install_reqs(file):
    reqs = parse_requirements(file, session=False)
    return [req.requirement for req in reqs]


def get_long_description(file):
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, file), encoding='utf-8') as fp:
        long_description = fp.read()

    return long_description


def data_files():
    if os.environ.get("READTHEDOCS", False):
        dest = 'borescope'
    else:
        dest = os.path.join(os.sep, 'borescope')

    df = [(dest, glob.glob(os.path.join('python', 'bin', 'borescope.conf')))]

    return df

setup(
    name='borescope',
    version=get_version('git describe --tags --abbrev=0'),
    description='Borescope App',
    long_description=get_long_description('README.md'),
    author='CIII',
    license='GNU GPLv3',
    package_dir={'borescope': 'python/borescope',
                 'borescope.gui': 'python/borescope/gui',
                 'borescope.modules': 'python/borescope/modules',
                 'borescope.bstester': 'python/borescope/bstester',
                  },
    packages=['borescope',
              'borescope.gui',
              'borescope.modules',
              'borescope.bstester'],
    include_package_data=True,
    data_files=data_files(),
    scripts=[os.path.join('python', 'bin', 'borescope'),
             os.path.join('python', 'bin', 'borescope-tester')],
    install_requires=get_install_reqs('requirements.txt'),
)

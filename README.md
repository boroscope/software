# Borescope

The Borescope Project consists of a long and flexible probe with a camera and screen that reproduces the augmented image with information about the orientation of the probe.

## Hardware Requirements (proven)

* Raspberry Pi 4B
* USB Camera
* Inercial Measurement Unit MPU6050/MPU9250

Note: It is possible to use without an IMU but this way it lacks the most important features.

## Software Requirements
* python3-pyqt5
* qttools5-dev-tools

Example:
```
sudo apt install python3-pyqtgraph
```

## Software Installation

To install python3 in a terminal type.
```
sudo apt update
sudo apt install python3 python3-pip3 python3-setuptools
```

### Development

To download this repository, in a terminal type
```
mkdir borescope
cd borescope
git clone https://gitlab.com/boroscope/software.git
cd software
```

Switch to **devel** branch
```
git switch devel
sudo pip3 install -U .
```

## Run borescope
Start the app with
```
borescope
```

In case you want to use special ports and devs you can execute borescope with a configuration file
```
borescope --config-file borescope.conf
```

In case "qt.qpa.plugin: Could not load the Qt platform plugin "xcb" even though it was found." appears, reinstall the following packages
```
sudo apt install libxcb1-dev --reinstall
sudo apt install libxcb-xinerama0 --reinstall
sudo pip3 install opencv-python-headless --ignore-installed

```

Note: It is possible to use it on a personal computer but Raspberry Pi is recommended.

### Branchs description

* **master** this branch has stable release. It can be used safely. Features under test are not available here.

* **devel** this branch is under test. It can be used at your own risk.

* other branches change almost every day and many times they do not work. They should not be used.

### Roadmap so far (past and future)

* v2.0   Change CA for a three camera system
* v1.0   add Continuum Arm to change camera orientation
* v0.9   augmented video with orientation of camera
* v0.01  video start with app

## Contributors

* Alejo Romero (Developer)
* Francisco Santillan (Developer)
* Bruno Sesini (Developer)
* Mateo Loaces (Developer)
* Simón Soardo (Developer)
* Florencia Panici (Designer)
* Ignacio Carranza (Developer)
* Francisco Javier Díaz Durán (Developer)
* Valentín Apa (Developer)
* Enzo Manolucos (Developer)
* Matías Lamas (Developer)
* Anglelo Beladelli (Developer)
* Eros Nucifora (Developer)
* Lucila Pedrazzi (Developer)
* Paula Brizzio (Developer)
* Juan Manuel Pautasso (Developer)
* Matias Mollecker (Mantainer, Developer)
* Mariano Vatcoff (Mantainer, Developer)
* Joaquín Miranda (Mantainer, Developer)
* Martin Nievas (Mantainer, Developer)
* Claudio J. Paz (Team leader, Developer)

## Sponsor

Centro de Invertigación en Informática para la ingeniería (CIII) [https://ciii.frc.utn.edu.ar/]



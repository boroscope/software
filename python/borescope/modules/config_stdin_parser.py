#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.
"""config_stdin_parser

This module is used to parse options given to borescope application through
command line.
"""

import argparse
from typing import List

class ConfigStdinParser:
    """Class to catch command line options

    Options:
        * ``--no-X``: Use without Xserver

        * ``--no-imu``: Use without IMU
        * ``--emu-imu``: Use emulated IMU

        * ``--camera``: Open VIDEO_DEV as input. i.e. /dev/video0
        * ``--no-camera``: Use without camera. Grey frame will be shown instead

        * ``--config-file``: Use selected configuration file
        * ``--generate-config-file``: Generate configuration file and try to \
                copy in to ~/.config/borescope/ then exit

    Args:
        args: List of strings containing command line arguments

    Attributes:
        raw_args: List of strings containing command line arguments without
            proccesing
        parsed_args: Namespace object containing attributes named like parsed
            options with its respective values

    """
    def __init__(self, args:List[str]):
        self.raw_args = args
        self.parsed_args = None

        self.setup()

    def setup(self):
        """This method is used to configurate ArgumentParser class from
        argparse module

        All options will be saved in a Namespace object:
        * ``--no-X`` will be store in no_x attribute as True or False
        * ``--no-imu`` and ``--emu-imu`` are mutually exclusive but both can \
            be absent. They will be stored in no_imu and emu_imu attributes \
            respectively. Default value is False to both cases. If \
            ``--no-imu`` is selected non of orientation estimation features \
            will be availables. If ``--emu-imu`` is selected all of these \
            features will be simulated.
        * ``--no-camera`` and ``--camera`` are mutually exclusive but both \
            can be absent. They will be stored in no_camera and video_dev \
            attributes with False and None values by default respectively. \
            If ``--no-camera`` is selected a gray frame will be shown instead \
            of camera frame. Option ``--camera`` need an extra value \
            corresponding to a file containing video stream (Ex /dev/video0). \
            Integer refering to index of video is allowed (Ex 0 for \
            /dev/video0).

        """
        name = 'borescope'
        description = 'Borescope camera for rescue missions'
        self.parser = argparse.ArgumentParser(
                prog=name,
                description=description)

        self.parser.add_argument('--no-X',
                                 action='store_true',
                                 dest='no_x',
                                 default=False,
                                 help='Use without Xserver')

        group_imu = self.parser.add_mutually_exclusive_group()
        group_imu.add_argument('--no-imu',
                               action='store_true',
                               dest='no_imu',
                               default=False,
                               help='Use without IMU')
        group_imu.add_argument('--emu-imu',
                               action='store_true',
                               default=False,
                               dest='emu_imu',
                               help='Use emulated IMU')

        group_camera = self.parser.add_mutually_exclusive_group()
        group_camera.add_argument('--camera',
                                  action='store',
                                  dest='video_dev',
                                  default=None,
                                  help='Open VIDEO_DEV as input. i.e. \
                                  /dev/video0')
        group_camera.add_argument('--no-camera',
                                  action='store_true',
                                  default=False,
                                  dest='no_camera',
                                  help='Use without camera. Grey frame will \
                                  be shown instead')

        group_file = self.parser.add_mutually_exclusive_group()
        group_file.add_argument('--config-file',
                                action='store',
                                default=None,
                                dest='config_file',
                                help='Use selected configuration file')
        group_file.add_argument('--generate-config-file',
                                action='store_true',
                                default=False,
                                dest='generate_config_file',
                                help='Generate configuration file and \
                                    try to copy in to ~/.config/borescope/ \
                                    then exit')

        self.parsed_args = self.parser.parse_args(self.raw_args)

    def get_args(self):
        """Method to return parser arguments

        Returns:
            parsed_args: Namespace with parsed command line options
        """
        return self.parsed_args

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import cv2
import numpy as np


class Camera:
    def __init__(self, options):
        self.options = options

        self._camera_path = None
        self._camera_id = None

        self.camera_height = self.options.screen_height
        self.camera_width = self.options.screen_width

        self.frame = None
        self.grey_frame = None
        self.cap = None
        self.device = None
        self.fps = 0.0
        self._is_active = False

        self.init_frame()

    def init_frame(self):
        self.grey_frame = np.zeros((
            self.options.screen_height,
            self.options.screen_width,
            self.options.channels), np.uint8) + 100

        #self.grey_frame = cv2.imread('/home/claudiojpaz/.config/borescope/debris.jpg')

        self.frame = np.copy(self.grey_frame)

    def set_capturing_device(self):
        self._is_active = True
        self.device = self.set_cap_video()
        if not self.device:
            self.device = self.set_emu_video()
            return False

        return self.device

    def set_cap_video(self):
        if type(self._camera_path) is str:
            print(f'Trying to capture from {self._camera_path}')
            attempts = 3
            for attempt in range(attempts):
                self.cap = cv2.VideoCapture(self._camera_path)
                if self.cap is not None and self.cap.isOpened():
                    print(f'Device {self._camera_path} is seted.')
                    break
            else:
                print(f'After {attempts} attempts device can not be opened.')
                if self._camera_path == self.options.front_camera_path:
                    self.options.front_camera_path = False
                if self._camera_path == self.options.left_camera_path:
                    self.options.left_camera_path = False
                if self._camera_path == self.options.right_camera_path:
                    self.options.right_camera_path = False

        return self._camera_path

    def set_emu_video(self):
        return 'emulated'

    def set_cap_frame_width(self, width):
        if self.cap is not None:
            self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)

    def set_cap_frame_height(self, height):
        if self.cap is not None:
            self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    def set_cap_fps(self, fps):
        if self.cap is not None:
            self.cap.set(cv2.CAP_PROP_FPS, fps)
            self.fps = fps

    def flip_frame(self):
        self.frame = cv2.flip(self.frame, 0)

    def set_frame(self):
        if type(self._camera_path) is str:
            if self._is_active and self.cap.isOpened():
                # Capture frame-by-frame
                ret, frame = self.cap.read()

                if ret is False:
                    print(f'Error getting frame from {self._camera_path}')
                    frame = None
                else:
                    frame = cv2.resize(frame,
                                   (self.camera_width,
                                    self.camera_height),
                                   fx=0,
                                   fy=0,
                                   interpolation=cv2.INTER_CUBIC)

            else:
                frame = None

        else:
            frame = None

        if frame is None:
            frame = np.copy(self.grey_frame)

        self.frame = frame

    def get_rgb_frame(self):
        return cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)

    def release_capturing_device(self):
        self._is_active = False
        if self.device != 'emulated':
            try:
                self.cap.release()
                print('Capture device was release.')
            except:
                print('Capture device was release with an unknown error')
                raise


    @property
    def is_active(self):
        return self._is_active

    @property
    def camera_path(self):
        return self._camera_path

    @camera_path.setter
    def camera_path(self, path):
        self.set_camera_path(path)

    def set_camera_path(self, path):
        self._camera_path = path

    @property
    def camera_id(self):
        return self._camera_id

    @camera_id.setter
    def camera_id(self, camera_id):
        self.set_camera_id(camera_id)

    def set_camera_id(self, camera_id):
        self._camera_id = camera_id

    def set_camera_size(self, width, height):
        self.camera_width = width
        self.camera_height = height

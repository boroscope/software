#!/usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

from borescope.modules.rotation_tools import Rb2n_quaternion, Rb2n_euler

class ArtificialHorizon:
    def __init__(self, options):
        self.options = options
        self.relative_tilt = 0

        x1 = 0
        x2 = self.options.screen_width - 1
        y1 = y2 = self.options.screen_height // 2

        self.points = ((x1, y1), (x2, y2))

    def project(self, R):
        row = self.options.screen_height
        col = self.options.screen_width

        cl, cr = 0, col-1

        # plane defined with normal vector and a point
        # vector
        nb = np.array([[0],[0],[1]])
        n = np.dot(R,nb)
        # point
        ppx = float(-row/2.)
        ppy = float(-(col-1)/2.)
        ppz = float(-row/2.)
        pp = np.array([[ppx],[ppy],[ppz]])

        # Ax + By + Cz + D = 0
        # A, B and C are normal vector components
        D = np.dot(n.T, pp)

        # vector for line 1 and 2
        vl = np.array([[0],[0],[1]])

        # line 1 (left margin of screen) point and vector
        # point
        pl1x = 0.0
        pl1y = 0.0
        pl1z = 0.0
        pl1 = np.array([[pl1x],[pl1y],[pl1z]])

        S1 = np.dot(n.T, pl1)
        lmbda1 = (D - S1)/n[2][0]
        rl = row - 1 + lmbda1

        # line 2 (right margin of screen) point and vector
        # point
        pl2x = 0.0
        pl2y = float(-(col-1))
        pl2z = 0.0
        pl2 = np.array([[pl2x],[pl2y],[pl2z]])

        S2 = np.dot(n.T, pl2)
        lmbda2 = (D-S2) / n[2][0]
        rr = row - 1 + lmbda2

        left = (int(cl), int(rl))
        right = (int(cr), int(rr))
        self.points = (left, right)

        if rl < 0 and rr < 0:
            self.relative_tilt = -1 # looking down

        if rl > row and rr > row:
            self.relative_tilt = 1 # looking up

        self.relative_tilt = 0 # visible horizon


    def calculate(self, euler_angles=None, quaternion=None):
        if quaternion is None:
            R = Rb2n_euler(*euler_angles)

        if euler_angles is None:
            quaternion[3] = 0
            R = Rb2n_quaternion(quaternion)

        self.project(R)


if __name__ == '__main__':
    img = np.zeros((480,640,3),np.uint8)+100

    roll = 50
    pitch = 0
    yaw = 0
    angles = (roll*3.14/180, pitch*3.14/180, yaw*3.14/180)

    add_horizon(img, euler_angles=angles)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()

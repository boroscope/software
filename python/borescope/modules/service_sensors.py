#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.


from borescope.modules.driver_imu import InertialMeasurementUnit
from borescope.modules.driver_camera import Camera
from borescope.modules.driver_battery import Battery


class Sensors:
    def __init__(self, options, device='all'):
        self.options = options

        if device in ['imu', 'all']:
            self.imu = InertialMeasurementUnit(self.options)

        if device in ['camera', 'all']:
            self.front_camera = Camera(self.options)
            self.front_camera.camera_path = self.options.front_camera_path
            self.front_camera.camera_id = 'front'
           # self.left_camera = Camera(self.options)
           # self.left_camera.camera_path = self.options.left_camera_path
           # self.left_camera.camera_id = 'left'
           # self.right_camera = Camera(self.options)
           # self.right_camera.camera_path = self.options.right_camera_path
           # self.right_camera.camera_id = 'right'

        if device in ['battery', 'all']:
            self.battery = Battery(self.options)

    def discover(self, device='all'):
        if device in ['imu', 'all']:
            self.imu.discover()

        if device in ['camera', 'all']:
            self.front_camera.set_capturing_device()
            #self.left_camera.set_capturing_device()
            #self.right_camera.set_capturing_device()

        if device in ['battery', 'all']:
            self.battery.discover()

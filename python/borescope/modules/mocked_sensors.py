#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import time
from mock import MagicMock

from borescope.modules.rotation_tools import Rn2b_euler

class MockImu:

    available_sensors = [
            'gyro',
            'gyro_rad',
            'accel',
            #'magneto',
            #'baro'
            ]

    def __init__(self):
        degree2rad = 3.14 / 180.

        self.roll = 0
        self.pitch = 30 * degree2rad
        self.yaw = 0

        self.gyro_data = [0.,0.,0.]
        self.gyro_sigma = 1.0
        self.gyro_offset = 0.0

        accel = np.array([[0.],[0.],[1.]])
        accel = np.dot(Rn2b_euler(self.roll, self.pitch, self.yaw), accel)
        self.accel_data = [accel[0][0], accel[1][0], accel[2][0]]
        self.accel_sigma = 0.002
        self.accel_offset = 0.0

        self._model = 'Emulated'

    def setup(self):
        self.get_gyro_rad = MagicMock(side_effect=self.cycle_data_gyro_rad)
        self.get_accel = MagicMock(side_effect=self.cycle_data_accel)

        return self._model

    @property
    def model(self):
        return self._model

    def get_gyro(self):
        pass

    def get_gyro_rad(self):
        pass

    def get_accel(self):
        pass

    def cycle_data_gyro(self):
        noise = np.random.normal(self.gyro_offset, self.gyro_sigma, 3)
        gx = self.gyro_data[0] + noise[0]
        gy = self.gyro_data[1] + noise[1]
        gz = self.gyro_data[2] + noise[2]

        return (gx, gy, gz)

    def cycle_data_gyro_rad(self):
        # deegre to rad -> degree * pi/180 = degree * 0.0174532925
        gx, gy, gz = self.cycle_data_gyro()

        gx *= 0.0174532925
        gy *= 0.0174532925
        gz *= 0.0174532925

        return gx, gy, gz

    def cycle_data_accel(self):
        noise = np.random.normal(self.accel_offset, self.accel_sigma, 3)
        ax = self.accel_data[0] + noise[0]
        ay = self.accel_data[1] + noise[1]
        az = self.accel_data[2] + noise[2]

        return ax, ay, az

class MockIna:
    def __init__(self):
        rshunt = 0.1
        points = 100
        points = int(points/2)*2

        current_1 = np.linspace(0.5, 6, num=int(points/2), endpoint=False)
        current_2 = np.linspace(6, 0.5, num=int(points/2), endpoint=False)
        current_array = np.concatenate((current_1, current_2,
                                            current_1*(-1), current_2*(-1)))

        vshunt_array = np.dot(current_array, rshunt)

        vbus_1 = np.linspace(8.7, 7.1, num=points, endpoint=False)
        vbus_2 = np.linspace(7.1, 8.7, num=points, endpoint=False)
        vbus_array = np.concatenate((vbus_1, vbus_2)) + vshunt_array

        self.data_value = {
            'vbus': vbus_array,
            'vshunt': vshunt_array,
            'current': current_array}

        self.last_item = {
            'vbus': 0,
            'vshunt': 0,
            'current': 0}

        self.last_update = time.time()
        self.time_update = 1

    def setup(self):
        self._model = 'mock_ina219'
        return self._model

    def mock_data(self, data = None):
        self.update_data()
        item = self.last_item.get(data)
        value = self.data_value.get(data)[item]

        return float(value)

    def update_data(self):
        if time.time() - self.last_update > self.time_update:
            self.last_update = time.time()
            for i in self.data_value.keys():
                if self.last_item.get(i) >= len(self.data_value.get(i))-1:
                    self.last_item[i] = 0
                else:
                    self.last_item[i] += 1
        return 0

    def vbus(self):
        return self.mock_data('vbus')

    def vshunt(self):
        return self.mock_data('vshunt')

    def current(self):
        return self.mock_data('current')

    def power(self):
        return self.mock_data('current')*self.mock_data('vbus')

    def sensing(self):
        data = {
            "V shunt": self.vshunt(),
            "V bus": self.vbus(),
            "Current": self.current(),
            "Power": self.power()
            }
        return data

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from math import cos, sin, atan2, asin, pi

def Rb2n_euler(phi, theta, psi):

    R = np.array([
           [cos(psi)*cos(theta),
            sin(phi)*cos(psi)*sin(theta)-cos(phi)*sin(psi),
            cos(phi)*cos(psi)*sin(theta)+sin(phi)*sin(psi)],
           [sin(psi)*cos(theta),
            sin(phi)*sin(psi)*sin(theta)+cos(phi)*cos(psi),
            cos(phi)*sin(psi)*sin(theta)-sin(phi)*cos(psi)],
           [-sin(theta),
            sin(phi)*cos(theta),
            cos(phi)*cos(theta)]
           ])

    R = R.reshape(3,3)
    return R


def Rn2b_euler(phi, theta, psi):

    R = np.array([
           [cos(psi)*cos(theta),
            sin(psi)*cos(theta),
            -sin(theta)],
           [sin(phi)*cos(psi)*sin(theta)-cos(phi)*sin(psi),
            sin(phi)*sin(psi)*sin(theta)+cos(phi)*cos(psi),
            sin(phi)*cos(theta)],
           [cos(phi)*cos(psi)*sin(theta)+sin(phi)*sin(psi),
            cos(phi)*sin(psi)*sin(theta)-sin(phi)*cos(psi),
            cos(phi)*cos(theta)]
           ])

    R = R.reshape(3,3)
    return R

def Rn2b_quaternion(q):
    q = np.array(q).reshape(4,1)

    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]

    R = np.array([
        [-q3**2-q2**2+q1**2+q0**2,2*(q0*q3+q1*q2),2*(q1*q3-q0*q2)],
        [2*(q1*q2-q0*q3),-q3**2+q2**2-q1**2+q0**2,2*(q2*q3+q0*q1)],
        [2*(q1*q3+q0*q2),2*(q2*q3-q0*q1),q3**2-q2**2-q1**2+q0**2]
        ])

    R = R.reshape(3,3)
    return R

def Rb2n_quaternion(q):
    q = np.array(q).reshape(4,1)

    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]

    R = np.array([
        [-q3**2-q2**2+q1**2+q0**2,2*(q1*q2-q0*q3),2*(q1*q3+q0*q2)],
        [2*(q0*q3+q1*q2),-q3**2+q2**2-q1**2+q0**2,2*(q2*q3-q0*q1)],
        [2*(q1*q3-q0*q2),2*(q2*q3+q0*q1),q3**2-q2**2-q1**2+q0**2]
        ])

    R = R.reshape(3,3)
    return R

def get_euler(q):
    q = np.array(q).reshape(4,1)

    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]

    roll = atan2(2*(q0*q1+q2*q3),(q0**2+q3**2-q1**2-q2**2))
    pitch = asin(2*(q0*q2-q1*q3))
    yaw = atan2(2*(q0*q3+q1*q2),(q0**2+q1**2-q2**2-q3**2))

    return roll, pitch, yaw

def get_euler_degrees(q):
    q = np.array(q).reshape(4,1)

    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]

    roll = atan2(2*(q0*q1+q2*q3),(q0**2+q3**2-q1**2-q2**2)) * 180 / pi
    pitch = asin(2*(q0*q2-q1*q3)) * 180 / pi
    yaw = atan2(2*(q0*q3+q1*q2),(q0**2+q1**2-q2**2-q3**2)) * 180 / pi

    return roll, pitch, yaw

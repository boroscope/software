#! /usr/bin/env python3
#
#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from math import *
from typing import Union
from typing import Tuple, List

from borescope.modules.kalman import Kalman

class quaternionEKF:
    """Extended Kalman Filter to estimate orientation of camera

    See :cite:p:`Paz2016PhDThesis` for details of Kalman filter and quaternions

    """
    def __init__(self):

        self.x = np.array([[1.],[0.],[0.],[0.]])
        self.P = np.identity(4)

        self.Q = np.identity(4)
        self.Q *= 0.1

        self.Ra = np.identity(3)
        self.Ra *= 0.03

        self.Rm = np.identity(3)
        self.Rm *= 0.007

        self.kf = Kalman(self.x, self.P)

    def predict(self, gyro, dt):
        self.setF(gyro, dt)
        self.kf.predict(self.F, self.Q)

        self.x = self.kf.x

    def update(self, z, sensor):
        self.setH(sensor)
        self.setR(sensor)
        self.setz_hat(sensor)

        if sensor == 'mag':
            # rotate to navigation frame
            zm_r = np.dot(self.Rb2n(self.kf.x), z)
            # project to xy plane
            zm_r[2,0] = 0
            # normalize compas
            zm_r = zm_r / sqrt(np.dot(zm_r.T, zm_r))
            # return to body frame
            z = np.dot(self.Rn2b(self.kf.x), zm_r)

        self.kf.update(self.H, self.R, z, self.z_hat)
        self.quaternion_normalization()

        self.x = self.kf.x

    def setF(self, gyro: Union[Tuple, List], dt: float) -> None:
        r"""Set transition matrix :math:`F_k`

        .. math::
            F_k = \frac{1}{2} \Omega(\omega) \Delta t + I

        where

        .. math::
            \Omega(\boldsymbol{\omega}(t)) =
            \begin{bmatrix}
               0 & -\omega_x & -\omega_y & -\omega_z\\
               \omega_x & 0 & \omega_z & -\omega_y\\
               \omega_y & -\omega_z & 0 & \omega_x\\
               \omega_z & \omega_y & -\omega_x & 0
            \end{bmatrix}

        Args:
            gyro: gyroscope values taken from inertial measurement unit
            dt: time differential between gyroscope measurements

        """
        gx = float(gyro[0])
        gy = float(gyro[1])
        gz = float(gyro[2])

        self.F = np.array([
            [0,    -gx,    -gy,    -gz],
            [gx,  0,         gz,    -gy],
            [gy, -gz,     0,        gx],
            [gz,  gy,     -gx,     0]
            ])

        self.F *= 0.5*dt
        self.F += np.identity(4)

    def setH(self, sensor):
        q0 = self.kf.x[0,0]
        q1 = self.kf.x[1,0]
        q2 = self.kf.x[2,0]
        q3 = self.kf.x[3,0]

        if sensor == 'accel':
            # H_{ak} =
            # \begin{bmatrix}
            #   -2q_2 &  2q_3 & -2q_0 & 2q_1 \\
            #   2q_1 &  2q_0 &  2q_3 & 2q_2 \\
            #   2q_0 & -2q_1 & -2q_2 & 2q_3
            # \end{bmatrix}
            self.H = np.array([
              [-2*q2,     2*q3,    -2*q0,    2*q1],
              [ 2*q1,     2*q0,     2*q3,    2*q2],
              [ 2*q0,    -2*q1,    -2*q2,    2*q3],
              ])

        if sensor == 'mag':
            # H_{mk} =
            # \begin{bmatrix}
            #   2q_0 & 2q_1 & -2q_2 & -2q_3 \\
            #   -2q_3 & 2q_2 &  2q_1 & -2q_0 \\
            #   2q_2 & 2q_3 &  2q_0 &  2q_1
            # \end{bmatrix}
            self.H = np.array([
              [ 2*q0,     2*q1,    -2*q2,   -2*q3],
              [-2*q3,     2*q2,     2*q1,   -2*q0],
              [ 2*q2,     2*q3,     2*q0,    2*q1],
              ])

    def setR(self, sensor):
        if sensor == 'accel':
            self.R = self.Ra

        if sensor == 'mag':
            self.R = self.Rm

    def setz_hat(self, sensor):
        if sensor == 'accel':
            # Rotate gravity to body frame
            s = np.array([[0.],[0.],[1.]])

        if sensor == 'mag':
            # Rotate nort to body frame
            s = np.array([[1.],[0.],[0.]])

        # Rotation happen here
        self.z_hat = np.dot(self.Rn2b(self.kf.x), s)

    def quaternion_normalization(self):
        # normalize the quaternion and the covariance matrix
        self.kf.x = self.kf.x / sqrt(np.dot(self.kf.x.T, self.kf.x))

        q0 = self.kf.x[0,0]
        q1 = self.kf.x[1,0]
        q2 = self.kf.x[2,0]
        q3 = self.kf.x[3,0]
        Norm_Jacobian = np.array([
          [ q1**2 + q2**2 + q3**2,-q0*q1, -q0*q2, -q0*q3],
          [-q0*q1,q0**2 + q2**2 + q3**2, -q2*q1, -q3*q1],
          [-q0*q2,-q1*q2, q0**2 + q1**2 + q3**2,-q3*q2],
          [-q0*q3,-q1*q3,-q2*q3,q0**2 + q1**2 + q2**2],
          ])

        Norm_Jacobian *= (q0**2 + q1**2 + q2**2 + q3**2)**(-1.5)
        PNJ = np.dot(self.kf.P, Norm_Jacobian.T)
        self.kf.P = np.dot(Norm_Jacobian, PNJ)

    def Rn2b(self, q):
        q0 = q[0,0]
        q1 = q[1,0]
        q2 = q[2,0]
        q3 = q[3,0]

        R = np.array([
            [-q3**2-q2**2+q1**2+q0**2,2*(q0*q3+q1*q2),2*(q1*q3-q0*q2)],
            [2*(q1*q2-q0*q3),-q3**2+q2**2-q1**2+q0**2,2*(q2*q3+q0*q1)],
            [2*(q1*q3+q0*q2),2*(q2*q3-q0*q1),q3**2-q2**2-q1**2+q0**2]
            ])

        return R

    def Rb2n(self, q):
        q0 = q[0,0]
        q1 = q[1,0]
        q2 = q[2,0]
        q3 = q[3,0]

        R = np.array([
            [-q3**2-q2**2+q1**2+q0**2,2*(q1*q2-q0*q3),2*(q1*q3+q0*q2)],
            [2*(q0*q3+q1*q2),-q3**2+q2**2-q1**2+q0**2,2*(q2*q3-q0*q1)],
            [2*(q1*q3-q0*q2),2*(q2*q3+q0*q1),q3**2-q2**2-q1**2+q0**2]
            ])

        return R

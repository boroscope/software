#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

"""device_gy87

This module is used as abstraction layer of GY-87 IMU to get accelerometer and
gyroscope measurement. This informations it is used by Borescope class to
estimate orientation of camera by means of an Extended Kalman filter.
"""
import logging

from typing import Tuple

from borescope.modules.interface_bus_i2c import BusI2C

class Gy87:
    """Class to use GY-87 Inertial Measurement Unit

    This module has three sensors: MPU6050, HMC5883L and BMP085.

    Note:
        Details on each registers can be found at
        corresponding sensor datasheet and
        `Register Map <https://invensense.tdk.com/wp-content/\
                uploads/2015/02/MPU-6000-Register-Map1.pdf>`__

    Args:
        i2c_port: i2c port where sensors are available

    Attributes:
        gyro_sf: gyroscope scale factor
        accel_sf: accelerometer scale factor
        i2c: instance of BusI2C class

    """

    mpu6050_address = 0x68
    """i2c address of MPU6050

    All i2c bus operations involving MPUU6050 should be done
    pointing to this address
    """

    mpuXXXX_whoami = 0x75
    """i2c address of modules register

    This register is used to verify the identity of the device
    """

    mpu6050_whoami_ans = 0x68
    """default value of the MPU6050 registry
    """

    power_mgmt_1 = 0x6b
    """power mode configuration address

    if clear internal 8MHz oscillator is selected as clock source
    """

    gyro_config = 0x1b
    """gyroscope self-test and full scale range configuration address

    if clear ± 250 °/s full scale range is selected
    """

    gyro_xout_h = 0x43
    """high byte of gyroscope x axis measurement
    """

    gyro_yout_h = 0x45
    """high byte of gyroscope y axis measurement
    """

    gyro_zout_h = 0x47
    """high byte of gyroscope z axis measurement
    """

    accel_config = 0x1c
    """accelerometer self-test and full scale range configuration address

    if clear 2g full scale range is selected
    """

    accel_xout_h = 0x3b
    """high byte of accelerometer x axis measurement
    """

    accel_yout_h = 0x3d
    """high byte of accelerometer x axis measurement
    """

    accel_zout_h = 0x3f
    """high byte of accelerometer x axis measurement
    """

    available_sensors = [
            'gyro',
            'gyro_rad',
            'accel',
            #'magneto',
            #'baro'
            ]

    def __init__(self, i2c_port=1):
        self.i2c = BusI2C(i2c_port)

        self.gyro_sf = 131.0
        self.accel_sf = 16384.0

        self._discovered = False
        self.model = 'gy87'

    def discover(self):
        reg = self.i2c.read_byte(self.mpu6050_address, self.mpuXXXX_whoami)
        if reg == self.mpu6050_whoami_ans:
            logging.info('MPU6050 was found')
            self._discovered = True
        else:
            logging.info('MPU6050 was not found.')

    @property
    def discovered(self):
        return self._discovered

    def setup(self):
        """Initial configurations of sensors

        At start, MPU6050 need power configuration and scale factors settings
        """
        if self.i2c.bus is None:
            return None

        self.i2c.write_byte(Gy87.mpu6050_address, Gy87.power_mgmt_1, 0)

        # +-250degrees/s # SF 131
        self.i2c.write_byte(Gy87.mpu6050_address, Gy87.gyro_config, 0)
        self.gyro_sf = 131.0

        # +-2g # SF 16384
        self.i2c.write_byte(Gy87.mpu6050_address, Gy87.accel_config, 0)
        self.accel_sf = 16384.0

        self.model = 'gy87'

        return self.model

    def get_gyro(self) -> Tuple[float, float, float]:
        """Return gyroscope measurement in degrees per second

        If bus return None, return of this method will be a tuple of zeros

        Returns:
            gx: gyroscope measurement in x axis in degrees per second
            gy: gyroscope measurement in y axis in degrees per second
            gz: gyroscope measurement in z axis in degrees per second

        """
        gx_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, Gy87.gyro_xout_h)
        gy_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, Gy87.gyro_yout_h)
        gz_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, Gy87.gyro_zout_h)

        if None in [gx_raw, gy_raw, gz_raw]:
            # log this event, something wrong happened
            return 0.0, 0.0, 0.0

        gx_d, gy_d, gz_d = (x/self.gyro_sf for x in (gx_raw, gy_raw, gz_raw))

        return gx_d, gy_d, gz_d

    def get_gyro_rad(self) -> Tuple[float, float, float]:
        """Return gyroscope measurement in radians per second

        deegre to rad -> degree * pi/180 = degree * 0.0174532925

        Returns:
            gx: gyroscope measurement in x axis in radians per second
            gy: gyroscope measurement in y axis in radians per second
            gz: gyroscope measurement in z axis in radians per second

        """
        gx_ds, gy_ds, gz_ds = self.get_gyro()

        gx_r, gy_r, gz_r = (x*0.0174532925 for x in (gx_ds, gy_ds, gz_ds))

        return gx_r, gy_r, gz_r

    def get_accel(self) -> Tuple[float, float, float]:
        """Return accelerometer measurement in gravity units

        Returns:
            ax: accelerometer measurement in x axis
            ay: accelerometer measurement in y axis
            az: accelerometer measurement in z axis

        """
        ax_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, 
        			       Gy87.accel_xout_h)
        ay_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, 
         			       Gy87.accel_yout_h)
        az_raw = self.i2c.read_word_2c(Gy87.mpu6050_address, 
        			       Gy87.accel_zout_h)

        if None in [ax_raw, ay_raw, az_raw]:
            # log this event, something wrong happened
            return 0.0, 0.0, 1.0

        ax_g, ay_g, az_g = (x/self.accel_sf for x in (ax_raw, ay_raw, az_raw))

        return ax_g, ay_g, az_g

    def get_accel_and_gyro(self) -> Tuple[float, float, float,
                                          float, float, float]:
        """Return accelerometer and gyroscope measurement

        Returns:
            ax: accelerometer measurement in x axis
            ay: accelerometer measurement in y axis
            az: accelerometer measurement in z axis
            gx: gyroscope measurement in x axis in degrees per second
            gy: gyroscope measurement in y axis in degrees per second
            gz: gyroscope measurement in z axis in degrees per second
        """
        gx_d, gy_d, gz_d = self.get_gyro()
        ax_g, ay_g, az_g = self.get_accel()
        return ax_g, ay_g, az_g, gx_d, gy_d, gz_d


if __name__ == '__main__':

    imu = Gy87()

    while True:
        ax, ay, az = imu.get_accel()
        gx, gy, gz = imu.get_gyro()
        print(f'{ax:8.4f} {ay:8.4f} {az:8.4f} {gx:10.4f} {gy:10.4f} {gz:10.4f}')

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from borescope.modules.driver_led import Led
from borescope.modules.driver_servo import Servo


class Actuators:
    def __init__(self, options):
        self.options = options

        self.led = Led(self.options)

        self.servo = Servo(self.options)

    def discover(self, device='all'):
        if device == 'led' or device == 'all':
            self.led.discover()

        if device == 'servo' or device == 'all':
            self.servo.discover()


if __name__ == '__main__':
    from types import SimpleNamespace
    import platform

    options = SimpleNamespace()
    options.pwm = True
    if platform.machine() == 'aarch64':
        options.running_on_arm = True
    else:
        options.running_on_arm = False

    actuators = Actuators(options)
    actuators.discover('servo')

    actuators.servo.enable()
    actuators.servo.set_angle(actuators.servo.channel1, 0)


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

class RunningAverage:
    def __init__(self, n):
        self.n = n

        self._samples = []
        self._average = 0.

    def add_sample(self, value):
        self._samples.append(value)

        if len(self._samples) <= self.n:
            self._average = np.sum(self._samples) / len(self._samples)
        else:
            self._average -= self._samples.pop(0) / self.n
            self._average += value / self.n

    @property
    def average(self):
        return self._average



#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

conf_values={# 'key':['data','line','previous string']
        'imu':[''],
        'i2c_port':[''],
        'display_title':[''],
        'display_angles':[''],
        'intensity':[''],
        'display_horizon':[''],
        'horizon_thickness':[''],
        'horizon_color':[''],
        'battery_capacity':[''],
        'battery_soc':[''],
        'debug_level':[''],
        }

def save_all(options, file_path):
    conf_values['imu'] = options.imu
    conf_values['i2c_port'] = options.i2c_port
    conf_values['display_title'] = options.display_title
    conf_values['display_angles'] = options.display_angles
    conf_values['intensity'] = options.intensity
    conf_values['pwm'] = options.pwm
    conf_values['display_horizon'] = options.display_horizon
    conf_values['horizon_thickness'] = options.horizon_thickness
    r, g, b = options.horizon_color
    conf_values['horizon_color'] = f'{r}-{g}-{b}'
    conf_values['battery_capacity'] = options.battery_capacity
    conf_values['battery_soc'] = options.battery_soc
    conf_values['debug_level'] = options.debug_level

    with open(file_path, 'r') as file:
        text = file.readlines()

    line_int=0

    for line in text:# Going all over the file to change value.
        for key in conf_values:
            if key in line and not('#' in line):# To prevent changes in comments.
                text[line_int] = (key
                                  + ' = '
                                  + str(conf_values[key]).lower()
                                  + '\n')
        line_int = line_int + 1# Update to next line.

    with open(file_path, 'w') as file:
        file.writelines(text)

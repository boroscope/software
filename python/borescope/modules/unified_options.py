#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import sys
import copy

def merge_options(file_options, stdin_args=None):
    options = copy.deepcopy(file_options)

    options.no_x = False
    if hasattr(file_options, "no_x"):
        options.no_x = file_options.no_x

    if stdin_args is not None:
        if stdin_args.video_dev is not None:
            options.front_camera_path = stdin_args.video_dev

        if stdin_args.no_imu:
            options.imu = False

        if stdin_args.emu_imu:
            options.imu = 'emulated'

        if stdin_args.no_x:
            options.no_x = True

    options.running_on_arm = None

    options.camera_updated = False

    options.changes = {
            'fps logged' : 0.0,
            'display horizon': None,
            'horizon thickness': None,
            'horizon color': None,
            'display title': None,
            'display angles': None,
            'battery soc': None,
            'battery soc value': None,
            'battery capacity': None,
            'battery capacity value': None,
            }

    return options

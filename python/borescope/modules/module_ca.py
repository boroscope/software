#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from borescope.modules.driver_servo import servo

class continuum_arm:
    def __init__(self):
        self.channel_x = 0
        self.channel_y = 1

        self.angle_x = 90
        self.angle_y = 90

        self.angle_x_max = 120
        self.angle_y_max = 120

        self.angle_x_min = 60
        self.angle_y_min = 60

        self.angle_step = 5

        self.servo = servo()

    def set_pwm_device(self, driver):
        self.servo.set_pwm_device(driver)

    def sub_step_x_angle(self):
        self.delta_step_x_angle(-1)

    def add_step_x_angle(self):
        self.delta_step_x_angle(1)

    def sub_step_y_angle(self):
        self.delta_step_y_angle(-1)

    def add_step_y_angle(self):
        self.delta_step_y_angle(1)

    def delta_step_x_angle(self, delta):
        min_, max_ = self.angle_x_min, self.angle_x_max
        new_angle = self.angle_x + delta*self.angle_step
        if self.valid_new_angle(new_angle, min_, max_):
            self.angle_x = new_angle
            self.servo.set_angle(self.channel_x, self.angle_x)

    def delta_step_y_angle(self, delta):
        min_, max_ = self.angle_y_min, self.angle_y_max
        new_angle = self.angle_y + delta*self.angle_step
        if self.valid_new_angle(new_angle, min_, max_):
            self.angle_y = new_angle
            self.servo.set_angle(self.channel_y, self.angle_y)

    def valid_new_angle(self, angle, min_, max_):
        if min_ < angle < max_:
            return True
        else:
            return False



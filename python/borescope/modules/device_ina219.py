#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

"""device_ina219

This module is used as abstraction layer of INA219 sensor to get current and
voltage measurement. This informations it is used by Battery class to
estimate the battery state of charge.
"""

import logging

from math import trunc

if __name__ == '__main__':
    from interface_bus_i2c import BusI2C
else:
    #from interface_bus_i2c import BusI2C
    from borescope.modules.interface_bus_i2c import BusI2C

class Ina219:
    """Class to use INA219 power sensor.

    Note:
        Details on each registers can be found at corresponding sensor
        `Datasheet <https://www.ti.com/lit/ds/symlink/ina219.pdf>`_
    """

    v_shunt_lsb = 0.00001
    """Vshunt measurement, 1 LSB step size."""

    v_bus_lsb = 0.004
    """Vbus measurement, 1 LSB step size."""

    current_lsb = 0
    """Current measurement, 1 LSB step size

    It's calculate in set_calibration method.
    This value depend of max_current attribute.
    """

    power_lsb = 0
    """Power measurement, 1 LSB step size

    It's calculate in set_calibration method.
    This value depend of max_current attribute.
    """

    ovf = -1
    """Math Overflow Flag.

    Is set when the Power or Current calculations are out of range.
    It indicates that current and power data may be meaningless
    """

    cnvr = -1
    """Conversion Ready.

    The INA219 Conversion Ready bit (CNVR) indicates when data from a conversion
    is available in the data output register
    """

    # Registers
    _config = 0x00
    _v_shunt = 0x01
    _v_bus = 0x02
    _power = 0x03
    _current = 0x04
    _calibration = 0x05

    # Configuration Register bits
    config = {'rst': 0x0,     #: Reset bit
              'brng': 0x0,    #: Bus voltage Range = 0x0:16V ,0x1:32V
              'pg': 0x0,      #: Gain = [1,/2,/4,/8] Range = [40,80,160,320]mV
              'badc': 0x3,    #: Bus ADC = 12 bits
              'sadc': 0x3,    #: Shunt ADC = 12 bits
              'mode': 0x7     #: Operating Mode = Shunt and Bus, continuous
            }

    def __init__(self, i2c_port=1, address=0x40, r_shunt=0.1,
                 max_current=1, max_voltage=16):
        """Initialization of sensor parameters and settings.

        Args:
            address (hex): i2c bus address
            r_shunt (float): shunt resistor in ohms.
            max_current (int): max current sensed.
            max_voltage (int): max voltage sensed, 16V or 32V.

        """
        self.i2c = BusI2C(i2c_port)
        self._address = address
        self.r_shunt = r_shunt
        self.max_current = max_current
        self._discovered = False

        # Max bus voltage only can be 16V o 32V
        if max_voltage in [16, 32]:
            self.max_voltage = max_voltage
        else:
            raise Exception("Wrong max bus voltage, only can be 16V o 32V")

        # Max shunt voltage must be less than 0.320mV
        self.max_shunt_voltage = max_current*r_shunt
        if self.max_shunt_voltage > 0.320:
            raise Exception("Max Shunt voltage is greater than 320mV")

    def discover(self):
        reg = self.i2c.read_byte(self._address, self._config)
        if reg is not None:
            logging.info('INA219 was found.')
            self._discovered = True
        else:
            logging.info('INA219 was not found.')

    @property
    def discovered(self):
        return self._discovered

    def setup(self):
        self.config_val = self.set_config()
        self.calibration_val = self.set_calibration()

        self.model = 'ina219'

        return self.model

    def set_config(self):
        """Configure the sensor

        Returns:
            config_reg value if successful, None otherwise.

        """
        c = self.config

        if self.max_voltage == 16:
            c['brng'] = 0
        elif self.max_voltage == 32:
            c['brng'] = 1
        else:
            raise Exception("BRNG only can be 16V o 32V")

        #PGA = [1,/2,/4,/8]  Range = +-[40,80,160,320]mV
        if self.max_shunt_voltage <= 0.04:
            c['pg'] = 0x0
        elif self.max_shunt_voltage <= 0.08:
            c['pg'] = 0x1
        elif self.max_shunt_voltage <= 0.160:
            c['pg'] = 0x2
        elif self.max_shunt_voltage <= 0.320:
            c['pg'] = 0x3
        else:
            raise Exception("PGA only can be [0x0, 0x1, 0x2, 0x3]")

        config_val = ((c['rst'] << 15) | (0x00 << 14) | (c['brng'] << 13)
                      | (c['pg']  << 11) | (c['badc'] << 7) | (c['sadc'] << 3)  
                      | (c['mode'] << 0))

        config_reg = [(config_val & 0xff00) >> 8, config_val & 0x00ff]

        write_ok = self.i2c.write_block(self._address, self._config, config_reg)

        self.config = c

        if write_ok:
            return config_val

        return None

    def set_calibration(self):
        """Calibrate the sensor.

        Returns:
            calibration_reg value if successful, None otherwise.

        """
        self.current_lsb = self.max_current/(2**15)
        self.power_lsb = 20*self.current_lsb

        calibration_val = trunc(0.04096/(self.current_lsb*self.r_shunt))
        calibration_reg = [(calibration_val & 0xff00) >> 8,
                            calibration_val & 0x00ff]

        write_ok = self.i2c.write_block(self._address,
                                        self._calibration,
                                        calibration_reg)

        if write_ok:
            return calibration_val

        return write_ok

    def vbus(self):
        """Read the bus voltage (v_bus), between the pins Vin+ and Gnd.

        Returns:
            v_bus value if successful, None otherwise.

        """
        v_bus_reg = self.i2c.read_block(self._address,
                                        self._v_bus,
                                        2)
        if v_bus_reg is None:
            return None

        v_bus = (v_bus_reg[0] << 8) | v_bus_reg[1]

        self.ovf = v_bus & 0x0001
        self.cnvr = (v_bus & 0x0002) >> 1

        #if self.ovf == 1:
            #print("WARNING - Math Overflow is set up")
        #if self.cnvr == 1:
            #print("Conversion Ready")

        v_bus = (v_bus >> 3) * self.v_bus_lsb

        return v_bus

    def vshunt(self):
        """Read the shunt voltage (v_shunt), between the pins Vin+ and Vin-.

        Returns:
            v_shunt value if successful, None otherwise.

        """
        v_shunt_reg = self.i2c.read_block(self._address,
                                          self._v_shunt,
                                          2)

        if v_shunt_reg is None:
            return None

        v_shunt = (v_shunt_reg[0] << 8) | v_shunt_reg[1]
        v_shunt = self.bin2c_b16(v_shunt)*self.v_shunt_lsb

        return v_shunt

    def current(self):
        """Read the current flowing through the shunt resistor

        Returns:
            current value if successful, None otherwise.

        """
        current_reg = self.i2c.read_block(self._address,
                                          self._current,
                                          2)

        if current_reg is None:
            return None

        current = (current_reg[0] << 8) | current_reg[1]
        current = self.bin2c_b16(current)*self.current_lsb

        return current

    def power(self):
        """Read sensed power value.

        Returns:
            power value if successful, None otherwise.

        """
        power_reg = self.i2c.read_block(self._address,
                                        self._power,
                                        2)

        if power_reg is None:
            return None

        power = (power_reg[0] << 8) | power_reg[1]
        power = power*self.power_lsb

        return power

    def bin2c_b16(self, num):
        """Two's complement to decimal conversion.

        Args:
            num (int): Two's complement number

        Returns:
            int: Decimal number

        """
        sign = num >> 15
        if sign == 1:
            num = (2**16 - num) * (-1)

        return num

if __name__ == '__main__':
    import time

    INA = Ina219(address=0x40, r_shunt=0.1,
                 max_current=1, max_voltage=16)

    DATA = {}

    while True:
        DATA = {"V shunt": INA.vshunt(),
                "V bus": INA.vbus(),
                "Current": INA.current(),
                "Power": INA.power()
                }
        DATA['Current Shunt'] = (DATA['V shunt'] * INA.calibration_val)/4096

        print('-'*20)
        print("V shunt: ", DATA['V shunt'])
        print("V bus: ", DATA['V bus'])
        print("Current: ", DATA['Current'])
        print("Current Shunt: ", DATA['Current Shunt'])
        print("Power: ", DATA['Power'])

        time.sleep(1)

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.
import time
import datetime
import threading
from collections import deque
from borescope.modules.device_ina219 import Ina219
from borescope.modules.mocked_sensors import MockIna

class Battery:
    def __init__(self, options):

        self.options = options
        self.capacity = options.battery_capacity
        self.initial_soc = options.battery_soc

        self.device = None
        self.device_list = [Ina219]

        self._model = 'Not available'
        self.state = 'unknown'
        self.available = False

        self.v_terminal = deque(maxlen=10)
        self.i = deque([0], maxlen=50)
        self.i_prom = 0

        self.soc = self.initial_soc
        self.soc_accum = 0
        self.percentage = 0
        self.time_remaining = str(datetime.timedelta(hours=0))
        self.health = []

        self.max_current = 5    #options.battery_max_current
        self.min_voltage = 7.1  #options.battery_min_voltage
        self.max_voltage = 8.6  #options.battery_max_voltage

        self.sampling_time = 0.2
        self.thread = threading.Thread(target=self.soc_thread, daemon=True)

    def update_parameters(self):
        self.capacity = self.options.battery_capacity
        self.initial_soc = self.options.battery_soc

    def soc_thread(self):
        last_soc_time = time.time()
        last_current = 0

        while True:
            current = self.current()
            voltage = self.voltage()
            if current is None or voltage is None:
                self.state = 'unknown'
            else:
                now = time.time()
                delta_t = now - last_soc_time
                delta_current = (current+last_current) / 2

                self.soc_accum += (delta_current*delta_t) * 1000 / 3600

                last_soc_time = now
                last_current = current

                self.i.append(current)
                self.i_prom = sum(self.i)/len(self.i)

                soc = self.initial_soc + self.soc_accum
                self.soc = round(soc, 5)

                if self.options.changes['battery soc value'] is not None:
                    self.soc = self.options.changes['battery soc value']
                    self.initial_soc = self.soc
                    self.soc_accum = 0.
                    self.options.changes['battery soc value'] = None

                self.options.battery_soc = int(self.soc)

                if self.options.changes['battery capacity value'] is not None:
                    self.options.battery_capacity = self.options.changes[
                                                 'battery capacity value'
                                                  ]
                    self.options.changes['battery capacity value'] = None

                percentage = self.soc/self.capacity
                self.percentage = round(percentage*100, 2)
                if self.percentage > 100:
                    self.percentage = 100

                # if the average of the current is positive it is because it is
                # charging. If not, it is discharging.
                if self.i_prom > 0:
                    self.state = 'charging'
                    cap_to_charge = self.capacity - self.soc
                    time_remaining = cap_to_charge/(self.i_prom*1000)
                else:
                    self.state = 'discharging'
                    time_remaining = self.soc/(-self.i_prom*1000)

                self.time_remaining = str(datetime.timedelta(
                                            hours=time_remaining))

                self.health_status(self.i_prom, voltage)

            time.sleep(self.sampling_time)

    def voltage(self):
        if self.device is None:
            return None

        vbus = self.device.vbus()
        vshunt = self.device.vshunt()

        if vbus is not None and vshunt is not None:
            v_terminal = vbus - vshunt
            return round(v_terminal, 3)
        else:
            self.state = 'unknown'
            return None

    def current(self):
        if self.device is None:
            return None

        current = self.device.current()

        if current is not None:
            return round(current, 3)
        else:
            self.state = 'unknown'
            return None

    def health_status(self, current, voltage):
            msg = []

            if current is not None:
                if abs(current) > self.max_current:
                    msg.append('Battery current is over safe levels')

            if voltage is not None:
                if voltage < self.min_voltage:
                    msg.append('Battery voltage is below safe levels')
                elif voltage > self.max_voltage:
                    msg.append('Battery voltage is over safe levels')
            if msg == []:
                msg = ['Good']

            self.health = msg

    def status(self):
        status =  {'state': self.state,
                   'percentage': self.percentage,
                   'time_remaining': self.time_remaining,
                   'health': self.health}

        return status

    def is_available(self):
        return self.available

    def discover(self):
        self.device = None
        if self.options.battery is True and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover()
                if self.device.discovered:
                    self.available = True
                    break
            else:
                self.device = None
                self.available = False

        if self.options.battery == 'emulated':
            self.device = MockIna()
            self.available = True

        if self.device is not None:
            self._model = self.device.setup()
        else:
            self._model = 'Not available'

        if self.available:
            self.thread.start()

if __name__ == '__main__':
    from types import SimpleNamespace

    options = SimpleNamespace()
    options.battery_soc = 2500
    options.battery_capacity = 5000

    ina = MockIna(options) #Ina219()
    bat = Battery()

    while True:
        print('-'*20)
        print('voltage: ', bat.voltage())
        print('current: ', bat.current())
        print('status: ', bat.status())
        print('i_prom: ', bat.i_prom)
        print('soc_accum: ', bat.soc_accum)
        print('cap remaining: ', bat.soc)
        time.sleep(1)

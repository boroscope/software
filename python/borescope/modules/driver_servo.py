#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

"""driver_servo

This module is used as controller to configure the use of DC motor. The
parameters that configure are duty cicle and started angle.
"""

from borescope.modules.device_pca9685 import Pca9685

class Servo:
    """Class to use MG996

    Note:
        Details and main characteristics can be found in
        `datasheet <https://datasheetspdf.com/pdf/942981/ETC/MG996R/1>`__

    Attributes:
        pwm: device class instance
        available (bool): configuration state variable
    """

    # duty cycle percentage for full range

    min_angle = -90
    """Servo phisical min angle
    """

    max_angle = 90
    """Servo phisical max angle
    """

    def __init__(self, options):
        self.options = options

        self.device = None
        self.available = False

        self.device_list = [
                Pca9685,
                ]

        self.channel1 = 2
        self.channel2 = 3

        self._intensity = 0
        self.disable()

    def discover(self):
        self.device = None
        if self.options.pwm and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover()
                if self.device.discovered:
                    self.available = True
                    break
            else:
                self.device = None

            if self.device is not None:
                self.device.setup()

    def set_angle(self, channel, angle):
        """Set axis and angle for step of servo

        Args:
            channel (int): X o Y axis value
            angle (int): Step value in degrees
        """
        if self.available:
            if angle > Servo.max_angle:
                angle = Servo.max_angle
            elif angle < Servo.min_angle:
                angle = Servo.min_angle

            duty = ((100/(Servo.max_angle - Servo.min_angle))
                    *(angle + Servo.min_angle) 
                    + 100)
            self.device.set_duty(channel, duty)
            return duty
        return None

    def disable(self):
        """Disable LEDs

        """

        if self.available:
            self.device.disable_output()

    def enable(self):
        """Enable LEDs

        """

        if self.available:
            self.device.enable_output()

class servo(Servo):
    """Class to use Servo driver

    Note:

    This module is to keep backward compatibility and will be removed on a future refactor
    """
    pass

if __name__ == '__main__':

    from types import SimpleNamespace
    import time
    options = SimpleNamespace()
    options.pwm = True
    servo = Servo(options)
    servo.discover()
    servo.enable()
    print('available:', servo.available)
    demora = 0.005
    servo_limit_min = -70
    servo_limit_max = 70
    while 1:
        for angle in range(-90,90, 1):
            duty = servo.set_angle(0, angle)
            print(duty, angle)
            time.sleep(demora)

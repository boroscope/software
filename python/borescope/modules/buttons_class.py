#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.
"""buttons_class

This class allows the usage of a keyboard to control the Borescope.

It contains all the neccesary functions to set up GPIO ports and determine which key was pressed in
a button matrix.
"""

from borescope.modules.interface_gpio import GPIO


class Buttons:
    """Class that allows the usage of the physical keyboard

    Contains the initial setup of the GPIO pins in relation to their physical button
    as well as methods to determine which key was pressed

    Args:
        pin_row (list): number of GPIO pins that act as rows
        pin_column (list): number of GPIO pins that act as columns

    Attributes:
        keypad (list): contains the labels of each button present in the keyboard
        column (list): contains each keyboard column GPIO pin number
        row (list): contains each keyboard row GPIO pin number
    """

    def __init__(self, pin_row=[22, 38], pin_column=[24, 26, 36]): # routed rows 22,38 cols 24, 26, 36
        self.gpio = GPIO()

        self.no_connected = [7,8,10,12,13,15,16,18,19,21,29,31,33,35,37]

        self.set_nc_as_inputs()

        self.keypad = [ # left down back home up right
            ["DOWN", "HOME", "UP"],
            ["LEFT", "BACK","RIGHT"],
        ]

        self.column = pin_column
        self.row = pin_row

        self.set_columns_as_inputs()
        self.set_rows_as_inputs()

    def set_nc_as_inputs(self):
        for nc in self.no_connected:
            self.gpio.setup(nc, 'in', pull_up_down='pull_down')

    def set_columns_high(self):
        #set all columns as high
        for i in range(len(self.column)):
            self.gpio.output(self.column[i], 'high')

    def set_columns_as_output(self):
        #set all columns as output
        for i in range(len(self.column)):
            self.gpio.setup(self.column[i], 'out')

    def set_rows_as_inputs(self):
        #set all rows as input (pull down)
        for i in range(len(self.row)):
            self.gpio.setup(self.row[i], 'in', pull_up_down='pull_down')

    def set_columns_as_inputs(self):
        #set all columns as input (pull down)
        for i in range(len(self.column)):
            self.gpio.setup(self.column[i], 'in', pull_up_down='pull_down')

    def get_key(self):
        """Determines which key was pressed on the keypad

        Returns:
            list: returns keypad value according to pressed key indexes
            None: if Raspberry isn't available

        """

        self.set_nc_as_inputs()
        self.set_columns_as_output()
        self.set_columns_high()
        self.set_rows_as_inputs()

        #read to see if any row is connected to a high column
        row_value = -1
        for i in range(len(self.row)):
            temp_read = self.gpio.input(self.row[i])
            #print(f'{self.row[i]=} {temp_read=}')
            if temp_read:
                row_value = i

        if row_value < 0 or row_value > len(self.row):
            self.exit()
            return None

        self.set_columns_as_inputs()

        #set the sensed row to output low
        self.gpio.setup(self.row[row_value], 'out')
        self.gpio.output(self.row[row_value], 'high')

        #read to see if any column is connected to the row on low
        column_value = -1
        for j in range(len(self.column)):
            temp_read = self.gpio.input(self.column[j])
            if temp_read:
                column_value = j

        if column_value < 0 or column_value > len(self.column):
            self.exit()
            return None

        if self.keypad[row_value][column_value] == 'DOWN':
            self.exit()
            return None

        return self.keypad[row_value][column_value]

    #safe exit cleaning up used ports
    def exit(self):
        """Return all used ports to their default input state to prevent shortcircuits

        Returns:

        None

        """
        for i in range(len(self.row)):
            self.gpio.cleanup(self.row[i])
        for j in range(len(self.column)):
            self.gpio.cleanup(self.column[j])


if __name__ == '__main__':
    kp = Buttons()

    KEY = None
    try:
        i = 0
        while True:
            KEY = kp.get_key()
            if KEY is not None:
                i += 1
                print(i, KEY)
    except:
        kp.gpio.cleanup()


class dummy_class:
    def __init__(self):
        self.option = None

    def set_option(self, value):
        """ recibe un entero
        """
        #if type(value) in [str, int, float]:
        #    self.option = int(float(value))
        #else:
        #    self.option = 0
        self.option = value

    def get_option(self):
        """devuelve un entero
        """
        return self.option

    def some_text_process(self, text):
        words = text.split(' ')
        return words


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.
"""bs_core

This module is used for initial configuration of the program, integrating hardware and software
"""
import sys
import time
import platform
import logging

from borescope.modules.service_sensors import Sensors
from borescope.modules.service_actuators import Actuators

from borescope.modules.quaternionEKF import quaternionEKF
from borescope.modules.rotation_tools import get_euler_degrees
from borescope.modules.geometric_tools import ArtificialHorizon


class Borescope:
    """Class to discover and initial orientation of the camera using i2c port

    Args:
        options: initial configuration of hardware

    Attributes:
        i2c: class instance of sensors in i2c port
    """

    def __init__(self, options):
        self.options = options

        self.last = None
        self.ekf = None

        self.running_on_arm = None
        self.horizon = None

        self.scan_arch()

        self.sensors = Sensors(self.options)
        self.sensors.discover('imu')
        self.sensors.discover('battery')

        # self.actuators = Actuators(self.options)
        # self.actuators.discover()

        self.horizon = ArtificialHorizon(self.options)

        if self.sensors.imu.device is not None:
            self.ekf = quaternionEKF()
            self.ekf.euler_angles = (0., 0., 0.)

        self.last = time.time()


    def orientation_estimation(self):
        gx, gy, gz = self.sensors.imu.gyro_rad

        now = time.time()
        dt = now-self.last
        self.last = now

        self.ekf.predict((gx, gy, gz), dt)

        ax, ay, az = self.sensors.imu.accel

        self.ekf.update((ax, ay, az), 'accel')

    def calculate_artificial_horizon(self):
        """calculate the artificial horizon
        """
        self.horizon.calculate(quaternion=self.ekf.x)

    def update_euler_angles(self):
        """get the euler angles in degrees
        """
        self.ekf.euler_angles = get_euler_degrees(self.ekf.x)

    def scan_arch(self):
        """Check if it works on arm architecture
        """

        if platform.machine() in ['armv7l', 'aarch64']:
            self.options.running_on_arm = True
            self.running_on_arm = True
            logging.info('Running on Arm')
        else:
            self.options.running_on_arm = False
            self.running_on_arm = False
            logging.info('Running on Desktop')

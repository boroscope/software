#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from borescope.modules.device_pca9685 import Pca9685


class led:
    """Abstraction layer to use pwm driver

    Attributes:
        pwm: device class instance
        available (bool): to know if driver was set

    """
    def __init__(self, driver=None):
        self.pwm = None
        self.available = False
        self.channel1 = 12
        self.channel2 = 13

    def set_pwm_device(self, driver):
        if driver is None:
            self.pwm = None
            self.available = False
        else:
            self.pwm = driver
            self.available = True

    def set_intensity(self, intensity: int) -> None:
        """Set pwd duty

        Args:
            intensity: Value of new pwm to send to pwm driver
        """
        duty = intensity
        if self.available:
            self.pwm.set_duty(self.channel1, duty)
            self.pwm.set_duty(self.channel2, duty)

class Led:
    def __init__(self, options):
        self.options = options

        self.device = None
        self.available = False

        self.device_list = [
                Pca9685,
                ]

        self.channel1 = 12
        self.channel2 = 13

        self._intensity = 0
        self.disable()

    def discover(self):
        self.device = None
        if self.options.pwm and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover()
                if self.device.discovered:
                    self.available = True
                    break
            else:
                self.device = None

            if self.device is not None:
                self.device.setup()

                self.device.set_min_off_time(0)
                self.device.set_max_off_time(4095)

    @property
    def intensity(self):
        return self._intensity

    @intensity.setter
    def intensity(self, intensity):
        self._intensity = intensity
        duty = intensity
        if self.device is not None:
            self.device.set_duty(self.channel1, duty)
            self.device.set_duty(self.channel2, duty)

    def disable(self):
        """Disable LEDs

        """

        if self.available:
            self.device.disable_output()

    def enable(self):
        """Enable LEDs

        """

        if self.available:
            self.device.enable_output()

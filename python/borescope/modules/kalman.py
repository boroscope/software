#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

class Kalman:
    def __init__(self, x0, P0):
        self.x = np.array(x0, dtype=np.float32).reshape((-1,1))
        n = len(self.x)
        self.P = np.array(P0, dtype=np.float32)

    def predict(self, F, Q):
        n = len(self.x)
        F = np.array(F, dtype=np.float32)
        Q = np.array(Q, dtype=np.float32)

        x = self.x
        P = self.P

        # x_{k} = F_{k} * x_{k-1}
        x_priori = np.dot(F, x)

        # P_{k}^{-} = F_{k} * P_{k-1} * F_{k}^{T} + Q_{k-1}
        P_priori = np.dot(F, np.dot(P, F.T)) + Q

        self.x = x_priori
        self.P = P_priori

    def update(self, H, R, z, z_hat):
        z = np.array(z, dtype=np.float32).reshape((-1,1))
        z_hat = np.array(z_hat, dtype=np.float32).reshape((-1,1))
        H = np.array(H, dtype=np.float32)
        R = np.array(R, dtype=np.float32)

        # S = H_{k} * P_{k}^{-} * H_{k}^{T} + R_{k}
        PH = np.dot(self.P, H.T)
        HPH = np.dot(H, PH)
        S = HPH + R

        # K_{k} =  P_{k}^{-} * H_{k}^{T} * S^{-1}
        S_1 = np.linalg.pinv(S)
        K = np.dot(PH, S_1)

        # x_{k} = x_{k-1} + K_{k} * (z - \hat{z})
        y = z - z_hat
        x_e = np.dot(K, y)

        # just for quaternions in accel mode
        # x_e[3,0] = 0
        # just for quaternions in mag mode
        # x_e[1,0] = 0
        # x_e[2,0] = 0

        self.x += x_e

        # P_{k2 = P_{k}^{-} - K_{k} * H_{k} * P_{k}^{-}
        # or
        # P_{k} = P_{k}^{-} - K_{k} * S_{k} * K_{k}^{T}
        SK = np.dot(S, K.T)
        self.P = self.P - np.dot(K, SK)



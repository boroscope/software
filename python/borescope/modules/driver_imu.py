#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from mock import MagicMock

from borescope.modules.device_gy87 import Gy87
from borescope.modules.mocked_sensors import MockImu


class InertialMeasurementUnit:
    def __init__(self, options):
        self.options = options

        self.device = None

        self.device_list = [
                Gy87,
                ]

        self._model = 'Not available'

    def discover(self):
        self.device = None
        if self.options.imu and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover()
                if self.device.discovered:
                    break
            else:
                self.device = None

        if self.options.imu == 'emulated':
            self.device = MockImu()

        if self.device is not None:
            self._model = self.device.setup()
        else:
            self._model = 'Not available'


    @property
    def gyro_rad(self):
        if 'gyro_rad' in self.device.available_sensors:
            return self.device.get_gyro_rad()
        else:
            return (None,)

    @property
    def gyro(self):
        if 'gyro' in self.device.available_sensors:
            return self.device.get_gyro()
        else:
            return (None,)

    @property
    def accel(self):
        if 'accel' in self.device.available_sensors:
            return self.device.get_accel()
        else:
            return (None,)

    @property
    def magneto(self):
        if 'magneto' in self.device.available_sensors:
            return self.device.get_magneto()
        else:
            return (None,)

    @property
    def baro(self):
        if 'baro' in self.device.available_sensors:
            return self.device.get_baro()
        else:
            return (None,)

    def model(self):
        return self._model

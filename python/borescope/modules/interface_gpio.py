#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import logging


class GPIO:
    def __init__(self):
        try:
            from RPi import GPIO as io
            io.cleanup()

            self._io = io


        except RuntimeError:
            logging.info('GPIO is not active. Running on desktop')
            self._io = None

        if self._io is not None:
            # setup
            self._io.setmode(self._io.BOARD)
            #self._io.setwarnings(False)
            print('GPIO is active')

    def setup(self, pin, mode, pull_up_down=None):
        if self._io is not None:
            if mode == 'out':
                mode = self._io.OUT
            if mode == 'in':
                mode = self._io.IN

            if pull_up_down == 'pull_down':
                pull_up_down = self._io.PUD_DOWN
            if pull_up_down == 'pull_up':
                pull_up_down = self._io.PUD_UP

            if pull_up_down is None:
                self._io.setup(pin, mode)
            else:
                self._io.setup(pin, mode, pull_up_down=pull_up_down)

    def pin_on(self, pin):
        if self._io is not None:
            self._io.output(pin, self._io.HIGH)

    def pin_off(self, pin):
        if self._io is not None:
            self._io.output(pin, self._io.LOW)

    def output(self, pin, level):
        if self._io is not None:
            if level == 'high':
                self._io.output(pin, self._io.HIGH)
            if level == 'low':
                self._io.output(pin, self._io.LOW)

    def input(self, pin):
        if self._io is not None:
            return self._io.input(pin)

        return None

    def cleanup(self, pin=None):
        if self._io is not None:
            if pin is None:
                self._io.cleanup()
            else:
                self._io.cleanup(pin)




#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time
import logging
import qtawesome as qta
from PyQt5.QtGui import QImage
from PyQt5.QtCore import QThread, pyqtSignal

from borescope.modules.math_tools import RunningAverage

class borescope_thread(QThread):
    changePixmap = pyqtSignal(QImage)
    changeAngles = pyqtSignal(tuple)
    changeLookingMsg = pyqtSignal(int)

    changeFPS = pyqtSignal(float)

    def __init__(self, parent=None, options=None, core=None):
        super().__init__()
        self.options = options
        self.bs_core = core
        self.imageWidth = self.bs_core.options.screen_width
        self.imageHeight = self.bs_core.options.screen_height
        self.working = False

        self.delay = 0.02
        self.fps = RunningAverage(100) # samples

        self.high_fps = 30
        self.normal_fps = 25
        self.low_fps = 20

        self.fps_status = 0 # 0:critical, 1:low, 2:normal, 3:high

    def run(self):
        self.working = True

        while self.working:
            start_time = time.time()
            time.sleep(self.delay)

            if self.bs_core.sensors.camera.is_active:
                self.bs_core.sensors.camera.set_frame()

            if self.bs_core.sensors.imu.device is not None:
                self.bs_core.orientation_estimation()
                self.bs_core.calculate_artificial_horizon()

                horizon_not_visible = self.bs_core.horizon.relative_tilt
                if horizon_not_visible:
                    self.changeLookingMsg.emit(horizon_not_visible)
                if self.options.display_angles:
                    self.bs_core.update_euler_angles()
                    self.changeAngles.emit(self.bs_core.ekf.euler_angles)

            if self.bs_core.sensors.camera.is_active:
                rgbImage = self.bs_core.sensors.camera.get_rgb_frame()
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h,
                                           bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(self.imageWidth,
                                             self.imageHeight)
                self.changePixmap.emit(p)

            camera_fps = 1.0 / (time.time() - start_time)
            self.changeFPS.emit(camera_fps)
            self.fps.add_sample(camera_fps)

            fps = self.fps.average
            if fps < self.normal_fps:
                fps_status = 1
            else:
                fps_status = 2

            if fps < self.low_fps:
                fps_status = 0

            if fps >= self.high_fps:
                fps_status = 3

            if self.fps_status is not fps_status:
                self.fps_status = fps_status
                if fps_status is 0:
                    logging.info(f'FPS is critical.')
                if fps_status is 1:
                    logging.info(f'FPS is low')
                if fps_status is 2:
                    logging.info(f'FPS back to normal')
                if fps_status is 3:
                    logging.info(f'FPS is high. High is good.')

                logging.info(f'The current FPS is: {round(fps, 2)}')


    def stop(self):
        self.working = False

        if self.bs_core.sensors.camera.is_active:
            self.bs_core.sensors.camera.release_capturing_device()

        self.exit(0)

    def changeWinSize(self,size):
        self.imageWidth = size.width()
        self.imageHeight = size.height()

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time

from PyQt5.QtCore import QThread, pyqtSignal

from borescope.modules.buttons_class import Buttons

class KBThread(QThread):
    pressed_button = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__()

        self.kp = Buttons()
        self.working = False
        self.delay = 0.01

        self.key_was_released = True


    def run(self):
        self.working = True
        key_press = False
        while self.working:
            time.sleep(self.delay)

            key = self.kp.get_key()
            if key is not None and self.key_was_released:
                key_press = True
                self.key_was_released = False

            if key_press:
                self.pressed_button.emit(key)
                key_press = False

            if key is None and not self.key_was_released:
                key_press = False
                self.key_was_released = True


    def stop(self):
        self.working = False
        self.exit(0)


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time
import logging
from PyQt5.QtGui import QImage
from PyQt5.QtCore import QThread, pyqtSignal

class CameraThread(QThread):
    changePixmap = pyqtSignal(dict)

    def __init__(self, parent=None, options=None, core=None):
        super().__init__()
        self.options = options
        self.bs_core = core
        self.imageWidth = self.bs_core.options.screen_width
        self.imageHeight = self.bs_core.options.screen_height
        self.working = False

        self.camera = None

        self.delay = 0.1

        self.signal_dict = {'image':None, 'id':None}

    def run(self):
        self.working = True

        while self.working:
            time.sleep(self.delay)

            if self.camera is not None:
                if self.camera.is_active:
                    self.camera.set_frame()

                    if self.camera.camera_id == 'right':
                        self.camera.flip_frame()

                    rgbImage = self.camera.get_rgb_frame()
                    h, w, ch = rgbImage.shape
                    bytesPerLine = ch * w
                    convertToQtFormat = QImage(rgbImage.data, w, h,
                                               bytesPerLine, QImage.Format_RGB888)
                    p = convertToQtFormat.scaled(self.imageWidth,
                                                 self.imageHeight)



                    self.signal_dict['id'] = self.camera.camera_id
                    self.signal_dict['image'] = p
                    self.changePixmap.emit(self.signal_dict)


    def stop(self):
        self.working = False

        if self.camera is not None:
            if self.camera.is_active:
                self.camera.release_capturing_device()

        self.exit(0)

    def changeWinSize(self, size):
        self.imageWidth = size.width()
        self.imageHeight = size.height()


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QColor

import time

from borescope.gui.ui_multicamera import MultiCameraWindow
from borescope.gui.ui_system import systemWindow

from borescope.gui.ui_tools import Color, Background
from borescope.gui.thread_keyboard import KBThread

class startWindow(QMainWindow):
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.cameraWindow = None
        self.systemWindow = None

        self.th_kb = KBThread(self)

        self.options.camera_updated = True

        self.setUI(options)
        self.setStyle()
        self.setLayout()

        self.th_kb.pressed_button.connect(self.pressed_button)

        self.setWindowFlag(Qt.FramelessWindowHint)

    def setUI(self, options):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)
        self.setFixedWidth(self.options.screen_width)
        self.setFixedHeight(self.options.screen_height)

        self.label = QLabel(f'v{self.options.app_version}')

        self.buttonSystem = QPushButton('System')
        self.buttonSystem.clicked.connect(self.showSystemInfo)

        self.buttonCamera = QPushButton('Camera')
        self.buttonCamera.clicked.connect(self.startCamera)

    def setStyle(self):
        labelStyleSheet = """
            font-size: 60px; color: grey
        """
        sysButtonStyleSheet = """
            background-color: red;
            font-size: 30px;
            padding: 6px;
            border-radius: 10px;
        """
        camButtonStyleSheet = """
            background-color: green;
            font-size: 30px;
            padding: 6px;
            border-radius: 10px;
        """

        self.label.setStyleSheet(labelStyleSheet)
        self.label.setAlignment(Qt.AlignCenter)

        self.buttonSystem.setStyleSheet(sysButtonStyleSheet)

        self.buttonCamera.setStyleSheet(camButtonStyleSheet)

    def setLayout(self):
        layoutV = QVBoxLayout()
        layoutH_botones = QHBoxLayout()
        layoutH_label = QHBoxLayout()
        layoutH_logos = QHBoxLayout()

        layoutH_botones.addWidget(self.buttonSystem)
        layoutH_botones.addWidget(QWidget())
        layoutH_botones.addWidget(self.buttonCamera)

        layoutH_label.addWidget(QWidget())
        layoutH_label.addWidget(QWidget())
        layoutH_label.addWidget(self.label)

        bgi = 'python/borescope/gui/images/topos.png'
        layoutH_logos.addWidget(Background(image=bgi))
        bgi = 'python/borescope/gui/images/utn.png'
        layoutH_logos.addWidget(Background(image=bgi))
        bgi = 'python/borescope/gui/images/ciii.png'
        layoutH_logos.addWidget(Background(image=bgi))

        layoutV.addLayout(layoutH_logos)
        layoutV.addWidget(QWidget())
        layoutV.addLayout(layoutH_label)
        layoutV.addLayout(layoutH_botones)
        layoutV.setContentsMargins(0, 0, 0, 0)

        bgi = 'python/borescope/gui/images/borescope.png'
        widget = Background(parent=self, image=bgi)
        widget.setLayout(layoutV)
        widget.setContentsMargins(0, 0, 0, 0)

        self.setCentralWidget(widget)

    def showEvent(self, event):
        if self.options.running_on_arm:
            self.showMaximized()
        else:
            self.show()

        self.th_kb.start()

    def startCamera(self):
        self.th_kb.stop()

        self.bs_core.sensors.discover('camera')
        self.bs_core.sensors.battery.update_parameters()

        self.cameraWindow = MultiCameraWindow(self.options, self.bs_core)

        # if (self.bs_core.running_on_arm and
                # self.bs_core.actuators.led.available):
            # self.bs_core.actuators.led.intensity = self.options.intensity

        self.cameraWindow.lost_focus.connect(self.lost_focus_method)

        if self.options.running_on_arm:
            self.cameraWindow.showMaximized() #Here is cameraWindow.showMaximized()
        else:
            self.cameraWindow.show()


    def showSystemInfo(self):
        self.th_kb.stop()

        if self.systemWindow is None:
            self.systemWindow = systemWindow(self.options, self.bs_core)
        self.systemWindow.lost_focus.connect(self.lost_focus_method)

        if self.options.running_on_arm:
            self.systemWindow.showMaximized() #Here is .showMaximized()
        else:
            self.systemWindow.show()

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            pass

        if key == 'HOME': #back
            self.showSystemInfo()
            pass

        if key == 'UP':
            pass

        if key == 'DOWN':
            pass

        if key == 'LEFT':
            pass

        if key == 'RIGHT':
            self.startCamera()

    @pyqtSlot(bool)
    def lost_focus_method(self, child_lost_focus):
        self.th_kb.start()


if __name__ == "__main__":
    from types import SimpleNamespace
    options = SimpleNamespace()
    options.app_name = __name__
    options.app_version = 0.99
    options.running_on_arm = True
    options.screen_width = 800
    options.screen_height = 480
    options.display_title = True
    options.i2c_port = 1
    options.imu= False
    options.display_angles = True
    options.camerapath = "/dev/video0"

    app = QApplication([])
    start_window = startWindow(options)
    start_window.show()
    app.exit(app.exec_())

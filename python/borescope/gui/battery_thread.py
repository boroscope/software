#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time

from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread, pyqtSignal
import qtawesome as qta

class battery_thread(QThread):
    changeIconBattery = pyqtSignal(QIcon, str, str)
    changeDCState = pyqtSignal(str, str)
    healthAlert = pyqtSignal(list)

    def __init__(self, parent=None, options=None, core=None):
        super().__init__()
        self.options = options
        self.bs_core = core
        self.working = False
        self.delay = 2

    def run(self):
        self.working = True

        while self.working:

            icon_bat, level, time_rem = self.battery_icon()
            voltage, current = self.dc_state()
            alert = self.health_alert()

            self.changeIconBattery.emit(icon_bat, level, time_rem)
            self.changeDCState.emit(voltage, current)
            self.healthAlert.emit(alert)

            time.sleep(self.delay)

    def stop(self):
        self.working = False
        self.exit(0)

    def round_level(self, value):

        if value >= 0 and value < 15:
            level, color  = '-10', 'red'
            return level, color

        if value >= 15 and value < 25:
            level, color  = '-20', 'red'
            return level, color

        if value >= 25 and value < 35:
            level, color  = '-30', 'yellow'
            return level, color

        if value >= 35 and value < 45:
            level, color  = '-40', 'yellow'
            return level, color

        if value >= 45 and value < 55:
            level, color  = '-50', 'yellow'
            return level, color

        if value >= 55 and value < 65:
            level, color  = '-60', 'yellow'
            return level, color
        if value >= 65 and value < 75:
            level, color  = '-70', 'green'
            return level, color

        if value >= 75 and value < 85:
            level, color  = '-80', 'green'
            return level, color

        if value >= 85 and value < 95:
            level, color  = '-90', 'green'
            return level, color

        if value >= 95:
            level, color  = '', 'green'
            return level, color

        return '-unknown', 'black'

    def battery_icon(self):
        if self.bs_core.sensors.battery.is_available():
            status = self.bs_core.sensors.battery.status()
            state = status.get('state')
            level = status.get('percentage')
            time_rem = status.get('time_remaining', '--:--:--')
            health = status.get('health')

            if health != ['Good']:
                #state = 'alert'
                pass

            mdi = 'mdi.battery'

            if state == 'charging':
                mdi = mdi + "-charging"
                mdi_level, color = self.round_level(level)
                mdi = mdi + mdi_level
            elif state == 'discharging':
                if level < 10:
                    mdi, color = mdi + '-alert', 'red'
                else:
                    mdi_level, color = self.round_level(level)
                    mdi = mdi + mdi_level
            elif state == 'alert':
                mdi = mdi + '-alert'
                color = 'red'
            else:
                mdi, color = mdi + '-unknown', 'red'

            icon_bat = qta.icon(mdi, color=color)
            level = str(int(level))

        else:
            mdi_battery, color = 'mdi.battery-off-outline', 'black'
            icon_bat = qta.icon(mdi_battery, color=color)
            level = '--'
            time_rem = '--:--:--'
        
        return icon_bat, level, time_rem

    def dc_state(self):
        if self.bs_core.sensors.battery.is_available():
            status = self.bs_core.sensors.battery.status()
            state = status.get('state')

            current = self.bs_core.sensors.battery.current()
            if current is None or state == 'unknown':
                current = '-.--'
            else:
                current = str(current)

            voltage = self.bs_core.sensors.battery.voltage()
            if voltage is None or state == 'unknown':
                voltage = '-.--'
            else:
                voltage = str(voltage)
        else:
            current = '-.--'
            voltage = '-.--'

        return voltage, current

    def health_alert(self):
        if self.bs_core.sensors.battery.is_available():
            status = self.bs_core.sensors.battery.status()
            health = status.get('health')
            
            return health
        
        return []

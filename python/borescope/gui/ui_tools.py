#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPalette, QColor, QPainter, QPixmap
from PyQt5.QtCore import Qt


class Color(QWidget):
    def __init__(self, color, *args, **kwargs):
        super(Color, self).__init__(*args, **kwargs)
        self.setAutoFillBackground(True)

        palette = self.palette()
        palette.setColor(QPalette.Window, QColor(color))
        self.setPalette(palette)

class Background(QWidget):
    def __init__(self, parent=None, image=None):
        super().__init__(parent)
        self.background_image = QPixmap(image)

    # def paintEvent(self, event):
        # # Dibujar la imagen de fondo ajustada al tamaño del widget
        # painter = QPainter(self)
        # if not self.background_image.isNull():
            # painter.drawPixmap(self.rect(), self.background_image)

    def paintEvent(self, event):
        # Dibujar la imagen de fondo manteniendo la relación de aspecto
        painter = QPainter(self)
        if not self.background_image.isNull():
            scaled_image = self.background_image.scaled(
                self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation
            )
            # Centrar la imagen escalada en el widget
            x_offset = (self.width() - scaled_image.width()) // 2
            y_offset = (self.height() - scaled_image.height()) // 2
            painter.drawPixmap(x_offset, y_offset, scaled_image)


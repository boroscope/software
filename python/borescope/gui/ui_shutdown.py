#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import os

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt, QSize
import qtawesome as qta

from PyQt5.QtCore import pyqtSlot, pyqtSignal

from borescope.gui.thread_keyboard import KBThread

class shutdown(QWidget):
    lost_focus = pyqtSignal(bool)
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.th_kb = KBThread(self)
        self.th_kb.pressed_button.connect(self.pressed_button)

        self.info = None

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        self.setGeometry(self.left, self.top, self.width, self.height)

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.setWindowFlag(Qt.FramelessWindowHint)

    def setUI(self):
        self.setStyleSheet("background-color: black;")

        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.buttonMenu = QPushButton(self, text='Back')
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.buttonRestart = QPushButton(self)
        self.buttonRestart.clicked.connect(self.restart)
        self.labelRestart = QLabel('Restart now')

        self.buttonSuspend = QPushButton(self)
        self.buttonSuspend.clicked.connect(self.suspend)
        self.labelSuspend = QLabel('Suspend in 10 seconds')

        self.buttonShutdown = QPushButton(self)
        self.buttonShutdown.clicked.connect(self.shutdown)
        self.labelShutdown = QLabel('Shutdown now')

    def setStyle(self):
        buttonMenuStyleSheet = """
            background-color: red;
            font-size: 20px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
        """

        buttonActionStyleSheet = """
            background-color: rgba(0,0,0,0);
        """

        labelStyleSheet = """
            color: grey;
            font-size: 26px;
        """

        self.buttonMenu.setStyleSheet(buttonMenuStyleSheet)

        restart_icon = qta.icon('mdi.restart', color='grey')
        self.buttonRestart.setIcon(restart_icon)
        self.buttonRestart.setIconSize(QSize(60, 60))
        self.buttonRestart.setStyleSheet(buttonActionStyleSheet)
        self.buttonRestart.setFlat(True)

        self.labelRestart.setStyleSheet(labelStyleSheet)

        suspend_icon = qta.icon('mdi.power-sleep', color='grey')
        self.buttonSuspend.setIcon(suspend_icon)
        self.buttonSuspend.setIconSize(QSize(60, 60))
        self.buttonSuspend.setStyleSheet(buttonActionStyleSheet)
        self.buttonSuspend.setFlat(True)

        self.labelSuspend.setStyleSheet(labelStyleSheet)

        power_icon = qta.icon('mdi.power', color='grey')
        self.buttonShutdown.setIcon(power_icon)
        self.buttonShutdown.setIconSize(QSize(60, 60))
        self.buttonShutdown.setStyleSheet(buttonActionStyleSheet)
        self.buttonShutdown.setFlat(True)

        self.labelShutdown.setStyleSheet(labelStyleSheet)

    def setWindowLayout(self):
        layoutVLines = QVBoxLayout()
        layoutV = QVBoxLayout()
        layoutH = QHBoxLayout()

        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        layoutVLines.insertSpacing(1, space.get('top'))

        layoutHButtonAndText = QHBoxLayout()
        layoutHButtonAndText.addWidget(self.buttonRestart, 0, Qt.AlignLeft)
        layoutHButtonAndText.addWidget(self.labelRestart, 0, Qt.AlignLeft)
        layoutHButtonAndText.addStretch(1)
        layoutVLines.addLayout(layoutHButtonAndText)

        layoutHButtonAndText = QHBoxLayout()
        layoutHButtonAndText.addWidget(self.buttonSuspend, 0, Qt.AlignLeft)
        layoutHButtonAndText.addWidget(self.labelSuspend, 0, Qt.AlignLeft)
        layoutHButtonAndText.addStretch(1)
        layoutVLines.addLayout(layoutHButtonAndText)

        layoutHButtonAndText = QHBoxLayout()
        layoutHButtonAndText.addWidget(self.buttonShutdown, 0, Qt.AlignLeft)
        layoutHButtonAndText.addWidget(self.labelShutdown, 0, Qt.AlignLeft)
        layoutHButtonAndText.addStretch(1)
        layoutVLines.addLayout(layoutHButtonAndText)

        layoutVLines.insertSpacing(-1, space.get('bottom'))

        layoutH.insertSpacing(1, space.get('left'))
        layoutH.addLayout(layoutVLines)
        layoutH.insertSpacing(-1, space.get('right'))

        layoutV.addLayout(layoutH)
        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignRight | Qt.AlignBottom)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.hide()

        self.th_kb.stop()
        self.lost_focus.emit(True)

        self.close()

    def restart(self):
        print('reset pressed')

    def suspend(self):
        print('suspend pressed')

    def shutdown(self):
        print("Power off pressed. Exiting...")
        #os.system("systemctl poweroff")

    def showEvent(self, event):
        if self.options.running_on_arm:
            self.showMaximized()
        else:
            self.show()

        self.th_kb.start()

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            self.backToMenu()

        if key == 'HOME': #back
            pass

        if key == 'UP':
            pass

        if key == 'DOWN':
            pass

        if key == 'LEFT':
            pass

        if key == 'RIGHT':
            pass

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import platform
import subprocess

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QStackedWidget, QListWidget, QListWidgetItem
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtGui import QFont, QPainter, QPixmap
from PyQt5.QtCore import Qt, QSize, pyqtSignal, pyqtSlot

from borescope.gui.ui_tools import Color
from borescope.gui.thread_keyboard import KBThread

class SystemInfoWindow(QWidget):
    lost_focus = pyqtSignal(bool)
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core
        self.hw_cpu = None

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        self.setGeometry(self.left, self.top, self.width, self.height)

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.th_kb = KBThread(self)
        self.th_kb.pressed_button.connect(self.pressed_button)

        self.process_OS_release()

        self.info_pages = [
                {'name': 'Sensors',
                 'info_lines':
                    [
                        {
                        'name': 'Inertial Unit',
                        'from': self.sensors_IMU_status,
                        },
                        {
                        'name': 'Front Camera Device',
                        'from': self.sensors_front_camera_device,
                        },
                        {
                        'name': 'Front Camera FPS',
                        'from': self.sensors_front_camera_FPS,
                        },
                        #{
                        #'name': 'Left Camera Device',
                        #'from': self.sensors_left_camera_device,
                        #},
                        #{
                        #'name': 'Left Camera FPS',
                        #'from': self.sensors_left_camera_FPS,
                        #},
                        #{
                        #'name': 'Right Camera Device',
                        #'from': self.sensors_right_camera_device,
                        #},
                        #{
                        #'name': 'Right Camera FPS',
                        #'from': self.sensors_right_camera_FPS,
                        #},
                    ],
                },
                {'name': 'Hardware',
                 'info_lines':
                    [
                        {
                        'name': 'Processor',
                        'from': self.hardware_CPU,
                        },
                    ],
                },
                {'name': 'System',
                 'info_lines':
                    [
                        {
                        'name': 'Borescope',
                        'from': self.system_BS_version,
                        },
                        {
                        'name': 'OS',
                        'from': self.system_OS_release,
                        },
                        {
                        'name': 'release',
                        'from': self.system_OS_version,
                        },
                        {
                        'name': 'Kernel version',
                        'from': self.system_OS_kernel,
                        },
                    ],
                },
                ]

        self.set_UI()
        self.set_style()
        self.set_window_layout()

        self.setWindowFlag(Qt.FramelessWindowHint)

        image = 'python/borescope/gui/images/fondo-borescope.png'
        self.background_image = QPixmap(image)

    def set_UI(self):

        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.button_menu = QPushButton(self, text='Back')
        self.button_menu.clicked.connect(self.back_to_menu)

        self.list_info = QListWidget(self)
        self.stacked_info = QStackedWidget(self)


        for i in range(len(self.info_pages)):
            item = QListWidgetItem()
            item.setText(self.info_pages[i].get('name'))
            item.setSizeHint(QSize(100, 50))
            item.setTextAlignment(Qt.AlignCenter)
            self.list_info.addItem(item)

        for i in range(len(self.info_pages)):
            page = QWidget()
            layout= QVBoxLayout()

            style_sheet = """
                font-size: 15px;
                color: white;
                background: black;
                margin: 0px;
            """

            for line in self.info_pages[i].get('info_lines'):
                layout_line = QHBoxLayout()
                label_left = QLabel()
                label_right = QLabel()
                label_left.setStyleSheet(style_sheet)
                label_left.setText(f"{line.get('name')}:")
                label_right.setStyleSheet(style_sheet)
                label_right.setFont(QFont('FreeMono', 15))
                text = line.get('from')()
                if not isinstance(text, str):
                    text = 'N/D'
                label_right.setText(f"{text:<20s}")
                layout_line.addWidget(label_left, 0, Qt.AlignRight)
                layout_line.addWidget(label_right)
                layout.addLayout(layout_line)

            layout.setAlignment(Qt.AlignTop)
            page.setLayout(layout)

            self.stacked_info.addWidget(page)

        self.list_info.currentRowChanged.connect(self.stacked_info.setCurrentIndex)
        self.list_info.setCurrentRow(0)

    def set_style(self):
        # backgroundStyleSheet = """
            # background-color: black;
        # """

        buttonMenuStyleSheet = """
            background-color: red;
            font-size: 20px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
        """

        widgetStyleSheet = """
            QListWidget {
                min-width: 120px;
                max-width: 120px;
                font-size: 20px;
                color: white;
                background: transparent;
            }
            QListWidget::item {
                background: rgb(30,30,30);
                border-top-left-radius: 8px;
                border-bottom-left-radius: 8px;
                margin: 1px;
            }
            QListWidget::item:selected {
                border-right-width: 2px;
                border-right-color: red;
                border-right-style: solid;
                background: rgb(70,70,70);
                }
            """
        stackedInfoStyleSheet = """
            background: black;
            margin: 0px;
            border-top-right-radius: 20px;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        """

        # self.setStyleSheet(backgroundStyleSheet)
        self.button_menu.setStyleSheet(buttonMenuStyleSheet)
        self.list_info.setStyleSheet(widgetStyleSheet)
        self.stacked_info.setStyleSheet(stackedInfoStyleSheet)

        self.list_info.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.list_info.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def show_event(self, event):
        if self.options.running_on_arm:
            self.showMaximized()
        else:
            self.show()

        self.th_kb.start()

    def set_window_layout(self):

        layout_list = QHBoxLayout()
        layout_list.setSpacing(0)
        layout_list.addWidget(self.list_info)
        layout_list.addWidget(self.stacked_info)

        layout = QVBoxLayout()
        layout.addLayout(layout_list)
        layout.addWidget(self.button_menu, 0, Qt.AlignRight | Qt.AlignBottom)

        self.setLayout(layout)

    def back_to_menu(self):
        self.hide()
        self.th_kb.stop()
        self.lost_focus.emit(True)
        self.close()

    def hardware_CPU(self):
        cmmd = 'cat /proc/cpuinfo | grep "model name\|Hardware\|Serial"| uniq'
        output = subprocess.check_output(cmmd, shell=True).strip()

        output = output.decode('utf-8').split('\n')[0].split('\t: ')
        long_name = output[1]
        self.hw_cpu = long_name.split('@')[0]

        return self.hw_cpu

    def process_OS_release(self):
        command = 'cat /etc/os-release'
        output = subprocess.check_output(command, shell=True).strip()

        self.os_release_lines = output.decode('utf-8').split('\n')

    def sensors_IMU_status(self):
        if self.bs_core.sensors.imu.device.model is None:
            return 'Not available'

        return self.bs_core.sensors.imu.device.model

    def sensors_front_camera_device(self):
        if self.bs_core.sensors.front_camera.device is None:
            return 'Not available'
        
        return self.bs_core.sensors.front_camera.device[15:-13]

    def sensors_front_camera_FPS(self):
        return f'{self.bs_core.sensors.front_camera.fps:.2f}'

    #def sensors_left_camera_device(self):
    #    if self.bs_core.sensors.left_camera.device is None:
    #        return 'Not available'
        
    #    return self.bs_core.sensors.left_camera.device[15:-13]

    #def sensors_left_camera_FPS(self):
    #    return f'{self.bs_core.sensors.left_camera.fps:.2f}'

    #def sensors_right_camera_device(self):
    #   if self.bs_core.sensors.right_camera.device is None:
    #        return 'Not available'

    #    return self.bs_core.sensors.right_camera.device[15:-13]

    #def sensors_right_camera_FPS(self):
    #    return f'{self.bs_core.sensors.right_camera.fps:.2f}'

    def system_OS_release(self):
        os_release_name = ''
        for line in self.os_release_lines:
            l = line.split('=')
            if l[0] == 'NAME':
                os_release_name = l[1].split('"')[1]

        return os_release_name

    def system_OS_version(self):
        os_release_version = ''
        for line in self.os_release_lines:
            l = line.split('=')
            if l[0] == 'VERSION':
                os_release_version = l[1].split('"')[1]

        return os_release_version

    def system_OS_kernel(self):
        return platform.release().split('-')[0]

    def system_BS_version(self):
        return self.options.app_version

    def template(self):
        return ' '

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            self.backToMenu()

        if key == 'HOME': #back
            pass

        if key == 'UP':
            pass

        if key == 'DOWN':
            pass

        if key == 'LEFT':
            pass

        if key == 'RIGHT':
            pass

    def paintEvent(self, event):
        # Dibujar la imagen de fondo manteniendo la relación de aspecto
        painter = QPainter(self)
        if not self.background_image.isNull():
            scaled_image = self.background_image.scaled(
                self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation
            )
            # Centrar la imagen escalada en el widget
            x_offset = (self.width - scaled_image.width()) // 2
            y_offset = (self.height - scaled_image.height()) // 2
            painter.drawPixmap(x_offset, y_offset, scaled_image)

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QPainter, QPixmap
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal

from borescope.gui.ui_info import SystemInfoWindow
from borescope.gui.ui_settings import SystemSettingsWindow
from borescope.gui.ui_shutdown import shutdown
from borescope.gui.thread_keyboard import KBThread
from borescope.gui.ui_tools import Background

class systemWindow(QWidget):
    lost_focus = pyqtSignal(bool)
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.info = None
        self.shutdown = None
        self.SystemSettingsWindow = None

        self.th_kb = KBThread(self)

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        self.setGeometry(self.left, self.top, self.width, self.height)

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.th_kb.pressed_button.connect(self.pressed_button)

        self.setWindowFlag(Qt.FramelessWindowHint)

        image = 'python/borescope/gui/images/fondo-borescope.png'
        self.background_image = QPixmap(image)


    def setUI(self):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.buttonInfo = QPushButton(self, text='System Info')
        self.buttonInfo.clicked.connect(self.showInfo)

        self.buttonSettings = QPushButton(self, text='Settings')
        self.buttonSettings.clicked.connect(self.systemSettings)

        self.buttonShutdown = QPushButton(self, text='Shutdown')
        self.buttonShutdown.clicked.connect(self.systemShutdown)

    def setStyle(self):
        defaultGreyButton = """
                background-color: grey;
                font-size: 30px;
                padding: 6px;
                border-radius: 10px;
            """

        menuButtonStyleSheet = """
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
            """

        self.buttonInfo.setStyleSheet(defaultGreyButton)

        self.buttonSettings.setStyleSheet(defaultGreyButton)

        self.buttonShutdown.setStyleSheet(defaultGreyButton)

        self.buttonMenu.setStyleSheet(menuButtonStyleSheet)

    def setWindowLayout(self):
        layoutVButtons = QVBoxLayout()
        layoutV = QVBoxLayout()
        layoutH = QHBoxLayout()

        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        layoutVButtons.insertSpacing(1, space.get('top'))
        layoutVButtons.addWidget(self.buttonInfo)
        layoutVButtons.addWidget(self.buttonSettings)
        layoutVButtons.addWidget(self.buttonShutdown)
        layoutVButtons.insertSpacing(-1, space.get('bottom'))

        layoutH.insertSpacing(1, space.get('left'))
        layoutH.addLayout(layoutVButtons)
        layoutH.insertSpacing(-1, space.get('right'))

        layoutV.addLayout(layoutH)
        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignRight | Qt.AlignBottom)

        self.setLayout(layoutV)



    def backToMenu(self):
        self.hide()
        self.th_kb.stop()
        self.lost_focus.emit(True)
        self.close()

    def showInfo(self):
        self.th_kb.stop()

        self.info = SystemInfoWindow(self.options, self.bs_core)
        self.info.lost_focus.connect(self.lost_focus_method)

        if self.options.running_on_arm:
            self.info.show() #Here is .showMaximized()
        else:
            self.info.show()

    def systemSettings(self):
        self.th_kb.stop()

        self.SystemSettingsWindow = SystemSettingsWindow(self.options, self.bs_core)
        self.SystemSettingsWindow.lost_focus.connect(self.lost_focus_method)

        if self.options.running_on_arm:
            self.SystemSettingsWindow.show() #Here is .showMaximized()
        else:
            self.SystemSettingsWindow.show()

    def systemShutdown(self):
        self.th_kb.stop()

        self.shutdown = shutdown(self.options, self.bs_core)
        self.shutdown.lost_focus.connect(self.lost_focus_method)

        if self.options.running_on_arm:
            self.shutdown.showMaximized()
        else:
            self.shutdown.show()

    def showEvent(self, event):
        if self.options.running_on_arm:
            self.showMaximized()
        else:
            self.show()

        self.th_kb.start()

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            self.backToMenu()

        if key == 'HOME': #back
            pass

        if key == 'UP':
            self.showInfo()

        if key == 'DOWN':
            self.systemShutdown()

        if key == 'LEFT':
            self.systemSettings()

        if key == 'RIGHT':
            pass

    @pyqtSlot(bool)
    def lost_focus_method(self, child_lost_focus):
        self.th_kb.key_was_released = False
        self.th_kb.start()

    def paintEvent(self, event):
        # Dibujar la imagen de fondo manteniendo la relación de aspecto
        painter = QPainter(self)
        if not self.background_image.isNull():
            scaled_image = self.background_image.scaled(
                self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation
            )
            # Centrar la imagen escalada en el widget
            x_offset = (self.width - scaled_image.width()) // 2
            y_offset = (self.height - scaled_image.height()) // 2
            painter.drawPixmap(x_offset, y_offset, scaled_image)

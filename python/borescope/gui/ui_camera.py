#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import logging

from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QMessageBox
from PyQt5.QtGui import QImage, QPixmap, QFont, QIcon
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal, QPoint, QSize, QLine
import qtawesome as qta

from borescope.gui.core_thread import borescope_thread
from borescope.gui.thread_keyboard import KBThread

class CameraWindow(QWidget):
    lost_focus = pyqtSignal(bool)
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.fps = 0.

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        # self.setGeometry(self.left, self.top, self.width, self.height)

        r, g, b = self.options.horizon_color
        self.horizon_color = QColor(r, g, b)

        self.position_x = 0
        self.position_y = 0

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.setUI()

        self.bsth = borescope_thread(self, self.options, self.bs_core)
        self.battery_alert_emited = False

        self.setStyle()
        self.setLayout()

        if self.options.display_angles:
            self.labelAngles.setHidden(False)
        else:
            self.labelAngles.setHidden(True)

        self.bs_core.actuators.led.enable()
        self.bs_core.actuators.servo.enable()

        self.setWindowFlag(Qt.FramelessWindowHint)

    def setUI(self):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.frontCamera = QLabel(self)
        self.leftCamera = QLabel(self)
        self.rightCamera = QLabel(self)

        self.buttonMenu = QPushButton('Menu', self)
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.buttonCAControl = QPushButton('CA', self)
        self.buttonCAControl.clicked.connect(self.toggleCA)

        self.buttonCAControlUp = QPushButton('U', self)
        self.buttonCAControlUp.clicked.connect(self.buttonCAControlUpPushed)
        self.buttonCAControlUp.hide()

        self.buttonCAControlDown = QPushButton('D', self)
        self.buttonCAControlDown.clicked.connect(self.buttonCAControlDownPushed)
        self.buttonCAControlDown.hide()

        self.buttonCAControlLeft = QPushButton('L', self)
        self.buttonCAControlLeft.clicked.connect(self.buttonCAControlLeftPushed)
        self.buttonCAControlLeft.hide()

        self.buttonCAControlRight = QPushButton('R', self)
        self.buttonCAControlRight.clicked.connect(self.buttonCAControlRightPushed)
        self.buttonCAControlRight.hide()

        self.buttonLed = QPushButton('', self)
        self.buttonLed.clicked.connect(self.toggleLedsSlider)

        self.sliderLed = QSlider(Qt.Vertical, self)
        self.sliderLed.hide()
        self.sliderLed.valueChanged.connect(self.ledIntensityUpdate)

        self.labelAngles = QLabel(self)

        self.labelLookingUp= QLabel(self)
        self.labelLookingDown= QLabel(self)

        self.labelBattery = QPushButton(self)
        self.labelAlertBattery = QLabel(self)
        self.labelAlertBattery.setText('')

        if self.options.debug_level >= 1:
            self.labelDCCurrent = QLabel(self)
            self.labelDCCurrent.setText('DC Current: -.--A')
            self.labelDCVoltage = QLabel(self)
            self.labelDCVoltage.setText('DC Voltage: -.--V')

        self.labelTitle = QLabel(self)

    def setStyle(self):
        self.frontCamera.resize(640, 480)
        self.leftCamera.resize(640, 480)
        self.rightCamera.resize(640, 480)

        buttonMenuStyleSheet = """
            background-color: red;
            font-size: 20px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
            """

        buttonCAControlStyleSheet = """
            background-color: green;
            font-size: 30px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
            """

        buttonCAControlDirectionStyleSheet = """
            color: rgba(0, 0, 0, 50);
            border-style: outset;
            border-width: 2px;
            border-color: grey;
            font-size: 30px;
            padding-left: 25px; padding-right: 25px;
            padding-top: 5px; padding-bottom: 5px;
            border-radius: 6px;
            """

        self.buttonMenu.setStyleSheet(buttonMenuStyleSheet)
        self.buttonCAControl.setStyleSheet(buttonCAControlStyleSheet)
        self.buttonCAControlUp.setStyleSheet(buttonCAControlDirectionStyleSheet)
        self.buttonCAControlDown.setStyleSheet(buttonCAControlDirectionStyleSheet)     
        self.buttonCAControlLeft.setStyleSheet(buttonCAControlDirectionStyleSheet)
        self.buttonCAControlRight.setStyleSheet(buttonCAControlDirectionStyleSheet)

        led_icon = qta.icon('mdi.white-balance-sunny',  color='white')
        self.buttonLed.setIcon(led_icon)
        self.buttonLed.setIconSize(QSize(30, 30))
        self.buttonLed.setStyleSheet('background-color: rgba(0,0,0,0);')
        self.buttonLed.setFlat(True)

        self.sliderLed.resize(16,84)
        self.sliderLed.setMinimum(0)
        self.sliderLed.setMaximum(100)
        self.sliderLed.setSingleStep(10)
        self.sliderLed.setValue(self.options.intensity)
        self.ledIntensityUpdate()

        angle = 0.0
        self.labelAngles.setFont(QFont('FreeMono', 12))
        angles = f'r:{angle:7.2f}\np:{angle:7.2f}\ny:{angle:7.2f}'
        self.labelAngles.setText(angles)

        labelAnglesStyleSheet = """
            border: 1px;
            color: green;
            font-weight: bold;
            """

        self.labelAngles.setStyleSheet(labelAnglesStyleSheet)

        icon_battery = qta.icon('mdi.battery-unknown',  color='black')
        self.labelBattery.setIcon(icon_battery)
        self.labelBattery.setText('--%')
        self.labelBattery.setIconSize(QSize(30, 30))
        self.labelBattery.setStyleSheet("""
                background-color: rgba(203,203,203,0.8);
                """)
        self.labelBattery.setFlat(True)
        self.labelBattery.setFont(QFont('SansSerif', 10))

        labelAlertBatteryStyleSheet = """
            background-color: red;
            color: white;
            font-weight: bold;
            font-size: 10px;
            """

        self.labelAlertBattery.setStyleSheet(labelAlertBatteryStyleSheet)

        if self.options.debug_level >= 1:
            labelDCStyleSheet = """
                color: white;
                font-weight: bold;
                font-size: 10px;
                """

            self.labelDCCurrent.setStyleSheet(labelDCStyleSheet)
            self.labelDCVoltage.setStyleSheet(labelDCStyleSheet)


        self.labelTitle.setStyleSheet('color: white')

        if self.options.display_angles and self.options.imu is not False:
            self.labelAngles.setHidden(False)
        else:
            self.labelAngles.setHidden(True)

    def setLayout(self):
        self.width = self.size().width()
        self.height = self.size().height()
        self.bsth.changeWinSize(self.size())

        #Layout Image
        self.image.resize(self.width, self.height)

        #Layout buttonMenu
        y = self.size().height() - self.buttonMenu.size().height()
        p = QPoint(5, y)
        self.buttonMenu.move(p)

        #Layout buttonCAControl
        y = self.size().height() - self.buttonCAControl.size().height()
        x = self.width//2 - self.buttonCAControl.size().width()//2

        p = QPoint(x, y - 3)
        self.buttonCAControl.move(p)

        #Layout buttonCAControlUp
        y = self.size().height()//4 - self.buttonCAControlUp.size().height()
        x = self.width//2 - self.buttonCAControlUp.size().width()//2

        p = QPoint(x + 10, y)
        self.buttonCAControlUp.move(p)

        #Layout buttonCAControlDown
        y = self.size().height()*3//4 - self.buttonCAControlDown.size().height()
        x = self.width//2 - self.buttonCAControlDown.size().width()//2

        p = QPoint(x + 10, y)
        self.buttonCAControlDown.move(p)

        #Layout buttonCAControlLeft
        y = self.size().height()//2 - self.buttonCAControlLeft.size().height()
        x = self.width//4 - self.buttonCAControlLeft.size().width()//2

        p = QPoint(x + 10, y)
        self.buttonCAControlLeft.move(p)

        #Layout buttonCAControlRight
        y = self.size().height()//2 - self.buttonCAControlRight.size().height()
        x = self.width*3//4 - self.buttonCAControlRight.size().width()//2

        p = QPoint(x + 10, y)
        self.buttonCAControlRight.move(p)

        #Layout buttonLed
        x = self.size().width() - self.buttonLed.size().width()
        p = QPoint(x, 0)
        self.buttonLed.move(p)

        #Layout sliderLed
        x += self.buttonLed.size().width()//2 - self.sliderLed.size().width()//2
        p = QPoint(x, self.buttonLed.size().height())
        self.sliderLed.move(p)

        #Layout labelAngles
        x = self.size().width() - self.labelAngles.size().width()
        y = self.size().height() - self.labelAngles.size().height()
        p = QPoint(x - 10, y - 10)
        self.labelAngles.move(p)

        #Layout labelBattery
        p = QPoint(5, 0)
        self.labelBattery.move(p)

        #Layout labelTitle
        if self.options.display_title:
            title = f'{self.options.app_name} v{self.options.app_version}'
        else:
            title = ''

        self.labelTitle.setText(title)

        x = self.size().width()//2 - self.labelTitle.size().width()//2
        p = QPoint(x, 7)
        self.labelTitle.move(p)

        x, y = 16, 40
        p = QPoint(x, y)
        self.labelAlertBattery.move(p)
        if self.options.debug_level == 0:
            self.labelAlertBattery.hide()

        if self.options.debug_level >= 1:
            p = QPoint(x, y+15)
            self.labelDCCurrent.move(p)
            p = QPoint(x, y+30)
            self.labelDCVoltage.move(p)

        r, g, b = self.options.horizon_color
        self.horizon_color = QColor(r, g, b)

    def resizeEvent(self, p):
        self.setLayout()

    def showEvent(self, event):
        self.setStyle()
        self.setLayout()
        self.startCapture()
        #self.show()

    def mousePressEvent(self, event):
        x_clk = event.pos().x()
        y_clk = event.pos().y()

        #Behavior of angles labels zone
        x_z1 = self.labelAngles.geometry().x()
        y_z1 = self.labelAngles.geometry().y()
        if x_clk > x_z1 and y_clk > y_z1:
            self.toggleAnglesLabel()

    @pyqtSlot(QImage)
    def setImage(self, image):
        self.image.setPixmap(QPixmap.fromImage(image))
        if self.options.display_horizon:
            painter = QPainter(self.image.pixmap())
            painter.setRenderHint(QPainter.Antialiasing, True)
            painter.setRenderHint(QPainter.HighQualityAntialiasing, True)
            painter.setRenderHint(QPainter.SmoothPixmapTransform, True)
            painter.setPen(QPen(self.horizon_color, self.options.horizon_thickness))
            x1, y1 = self.bs_core.horizon.points[0]
            x2, y2 = self.bs_core.horizon.points[1]
            painter.drawLine(x1,y1,x2,y2)

    @pyqtSlot(QIcon, str, str)
    def setIconBattery(self, icon, level, time_rem):
        self.labelBattery.setIcon(icon)

        [th, tm, ts] = time_rem.split(':')
        if th not in ['0', '--']:
            time_rem = '{}hs {}min'.format(th, tm)
        else:
            time_rem = '{}min'.format(tm)

        self.labelBattery.setText(level + '%' + '\n{}'.format(time_rem))
        self.labelBattery.resize(self.labelBattery.sizeHint())

    @pyqtSlot(tuple)
    def setAngles(self, euler_angles):
        roll, pitch, yaw = euler_angles
        angles = f'r:{roll:7.2f}\np:{pitch:7.2f}\ny:{yaw:7.2f}'
        self.labelAngles.setText(angles)

    @pyqtSlot(int)
    def setLookingMsg(self, angles):
        pass

    @pyqtSlot(float)
    def setFPS(self, fps):
        self.fps = fps

    @pyqtSlot(str, str)
    def setDCState(self, voltage, current):
        if self.options.debug_level >= 1:
            self.labelDCCurrent.setText(f'DC Current: {current}A')
            self.labelDCCurrent.adjustSize()

            self.labelDCVoltage.setText(f'DC Voltage: {voltage}V')
            self.labelDCVoltage.adjustSize()

    @pyqtSlot(list)
    def healthAlert(self, alert):
        
        if alert == ['Good'] or alert == []:    
            self.battery_alert_emited = False
            self.labelAlertBattery.setText('')
            self.labelAlertBattery.adjustSize()
            self.labelAlertBattery.hide()
        else:
            if not self.battery_alert_emited:
                self.battery_alert_emited = True

                self.labelAlertBattery.setText('\n'.join(alert))
                self.labelAlertBattery.adjustSize()
                #self.labelAlertBattery.show()

                text = '\n'.join(alert)

                msgBox = QMessageBox(self)
                msgBox.setIcon(QMessageBox.Critical)
                msgBox.setText(text)
                msgBox.setWindowTitle('Battery Status Alert')
                msgBox.setStandardButtons(QMessageBox.Ok)

                retval = msgBox.exec_()

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            if self.buttonCAControlUp.isHidden():
                self.startCA()
            elif self.sliderLed.isHidden():
                self.toggleLedsSlider()

        if key == 'HOME': #back
            if not self.buttonCAControlUp.isHidden():
                self.stopCA()
            elif self.sliderLed.isVisible():
                self.toggleLedsSlider()

        if key == 'UP':
            if not self.buttonCAControlUp.isHidden():
                self.buttonCAControlUpPushed()
            elif self.sliderLed.isVisible():
                self.led_update(5)

        if key == 'DOWN':
            if not self.buttonCAControlUp.isHidden():
                self.buttonCAControlDownPushed()
            elif self.sliderLed.isVisible():
                self.led_update(-5)

        if key == 'LEFT':
            if not self.buttonCAControlUp.isHidden():
                self.buttonCAControlLeftPushed()
            elif self.buttonCAControlUp.isHidden() and self.sliderLed.isHidden():
                self.backToMenu()

        if key == 'RIGHT':
            if not self.buttonCAControlUp.isHidden():
                self.buttonCAControlRightPushed()
            elif self.sliderLed.isHidden():
                self.toggleLedsSlider()

    def led_update(self, delta):
        intensity = self.sliderLed.value()
        intensity += delta
        self.sliderLed.setValue(intensity)

    def startCapture(self):
        self.bsth.changePixmap.connect(self.setImage)
        self.bsth.changeAngles.connect(self.setAngles)
        self.bsth.changeLookingMsg.connect(self.setLookingMsg)
        self.bsth.changeFPS.connect(self.setFPS)

        self.bsth.start()

    def backToMenu(self):
        if self.bs_core.running_on_arm and self.bs_core.actuators.led.available:
            self.bs_core.actuators.led.intensity = 0

        self.hide()
        self.lost_focus.emit(True)

        self.bs_core.actuators.servo.disable()
        self.bsth.working = False
        self.ledFinalValue()

        self.close()

    def startCA(self):
        #print('home')
        if self.buttonCAControlUp.isHidden():
            self.buttonCAControlUp.show()
            self.buttonCAControlDown.show()
            self.buttonCAControlLeft.show()
            self.buttonCAControlRight.show()

    def stopCA(self):
        #print('back')
        if not self.buttonCAControlUp.isHidden():
            self.buttonCAControlUp.hide()
            self.buttonCAControlDown.hide()
            self.buttonCAControlLeft.hide()
            self.buttonCAControlRight.hide()

    def toggleCA(self):
        if self.buttonCAControlUp.isHidden():
            self.buttonCAControlUp.show()
            self.buttonCAControlDown.show()
            self.buttonCAControlLeft.show()
            self.buttonCAControlRight.show()
        else:
            self.buttonCAControlUp.hide()
            self.buttonCAControlDown.hide()
            self.buttonCAControlLeft.hide()
            self.buttonCAControlRight.hide()

    def buttonCAControlUpPushed(self):
        if (self.bs_core.running_on_arm and
                self.bs_core.actuators.servo.available):
            #print('up')

            self.position_y -= 1
            if self.position_y < -90:
                self.position_y = -90

            self.bs_core.actuators.servo.set_angle(
              self.bs_core.actuators.servo.channel2, self.position_y)

    def buttonCAControlDownPushed(self):
        if (self.bs_core.running_on_arm and
                self.bs_core.actuators.servo.available):

            self.position_y += 1
            if self.position_y > 90:
                self.position_y = 90

            self.bs_core.actuators.servo.set_angle(
              self.bs_core.actuators.servo.channel2, self.position_y)

    def buttonCAControlLeftPushed(self):
        if (self.bs_core.running_on_arm and
                self.bs_core.actuators.servo.available):

            self.position_x += 1
            if self.position_x > 90:
                self.position_x = 90

            self.bs_core.actuators.servo.set_angle(
              self.bs_core.actuators.servo.channel1, self.position_x)

    def buttonCAControlRightPushed(self):
        if (self.bs_core.running_on_arm and
                self.bs_core.actuators.servo.available):

            self.position_x -= 1
            if self.position_x < -90:
                self.position_x = -90

            self.bs_core.actuators.servo.set_angle(
              self.bs_core.actuators.servo.channel1, self.position_x)

    def toggleLedsSlider(self):
        if self.sliderLed.isVisible():
            self.sliderLed.hide()
            intensity = self.sliderLed.value()
            logging.info(f'LED intensity changed to {intensity}')
        else:
            self.sliderLed.show()

    def toggleAnglesLabel(self):
        if self.labelAngles.isVisible():
            self.labelAngles.hide()
        else:
            self.labelAngles.show()

    def ledIntensityUpdate(self):
        intensity = self.sliderLed.value()
        self.options.intensity = intensity
        if self.bs_core.running_on_arm and self.bs_core.actuators.led.available:
            self.bs_core.actuators.led.intensity = intensity

    def closeEvent(self, event):
        self.bsth.stop()
        self.ledFinalValue()
        event.accept()

    def ledFinalValue(self):
        intensity = self.sliderLed.value()
        logging.info(f'LED intensity changed to {intensity}.')
        logging.info('But window was closed so LED was turned OFF')

if __name__ == "__main__":
    from types import SimpleNamespace
    options = SimpleNamespace()
    options.app_name = __name__
    options.app_version = 0.99
    options.running_on_arm = True
    options.screen_width = 800
    options.screen_height = 480
    options.display_title = True
    options.i2c_port = 1
    options.imu = False
    options.display_angles = True
    options.camerapath = "/dev/video0"

    app = QApplication([])
    start_window = CameraWindow(options)
    start_window.show()
    app.exit(app.exec_())

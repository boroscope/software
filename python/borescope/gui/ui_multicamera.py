#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import logging

from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QMessageBox
from PyQt5.QtGui import QImage, QPixmap, QFont, QIcon
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal, QPoint, QSize, QLine
import qtawesome as qta

import time

from borescope.gui.camera_thread import CameraThread
from borescope.gui.thread_keyboard import KBThread

class Image(QLabel):
    clicked = pyqtSignal()

    def __init__(self, *args):
        super().__init__(*args)

    def mouseReleaseEvent(self, event):
        self.clicked.emit()

class MultiCameraWindow(QWidget):
    lost_focus = pyqtSignal(bool)

    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.th_kb = KBThread(self)

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        # self.setGeometry(self.left, self.top, self.width, self.height)

        self.position_x = 0
        self.position_y = 0

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.setUI()

        # self.bsth = borescope_thread(self, self.options, self.bs_core)
        self.front_camera_th = CameraThread(self, self.options, self.bs_core)
        self.front_camera_th.camera = self.bs_core.sensors.front_camera
        # self.left_camera_th = CameraThread(self, self.options, self.bs_core)
        # self.left_camera_th.camera = self.bs_core.sensors.left_camera
        # self.right_camera_th = CameraThread(self, self.options, self.bs_core)
        # self.right_camera_th.camera = self.bs_core.sensors.right_camera

        self.th_kb.pressed_button.connect(self.pressed_button)

        try:
            from RPi import GPIO
            # GPIO.setmode(GPIO.BOARD)
            GPIO.setup(32, GPIO.OUT)
            GPIO.setup(33, GPIO.OUT)
            GPIO.setup(40, GPIO.OUT)

            self.pwmL1 = GPIO.PWM(32, 1000)
            self.pwmL2 = GPIO.PWM(33, 1000)
            GPIO.output(40, GPIO.LOW)

            print('pwm al 50%')

        except RuntimeError:
            print('GPIO is not active. Running on desktop')
            logging.info('GPIO is not active. Running on desktop')
            self._io = None

        self.set_style()
        self.set_layout()

        # self.bs_core.actuators.led.enable()

        self.setWindowFlag(Qt.FramelessWindowHint)

        self.fps = 0.0


    def setUI(self):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.front_camera = Image(self)
        #self.left_camera = Image(self)
        #self.right_camera = Image(self)

        #self.left_camera.clicked.connect(self.set_left_in_focus)
        #self.right_camera.clicked.connect(self.set_right_in_focus)

        self.buttonMenu = QPushButton('Menu', self)
        self.buttonMenu.clicked.connect(self.backToMenu)

        #self.buttonLeftBackToLeft = QPushButton('Back', self)
        #self.buttonLeftBackToLeft.hide()
        #self.buttonLeftBackToLeft.clicked.connect(self.set_front_in_focus)

        #self.buttonRightBackToRight = QPushButton('Back', self)
        #self.buttonRightBackToRight.hide()
        #self.buttonRightBackToRight.clicked.connect(self.set_front_in_focus)

        self.buttonLed = QPushButton('', self)
        self.buttonLed.clicked.connect(self.toggleLedsSlider)

        self.sliderLed = QSlider(Qt.Vertical, self)
        self.sliderLed.hide()
        self.sliderLed.valueChanged.connect(self.ledIntensityUpdate)

        self.labelTitle = QLabel(self)

    def set_style(self):
        self.front_camera.resize(800, 480)
        # self.left_camera.resize(640, 480)
        # self.right_camera.resize(640, 480)

        buttonBackToFrontStyleSheet = """
            background-color: orange;
            font-size: 20px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
            """

        buttonMenuStyleSheet = """
            background-color: red;
            font-size: 20px;
            padding-left: 5px; padding-right: 5px;
            padding-top: 1px; padding-bottom: 1px;
            border-radius: 6px;
            """

        self.buttonMenu.setStyleSheet(buttonMenuStyleSheet)
        #self.buttonLeftBackToLeft.setStyleSheet(buttonBackToFrontStyleSheet)
        #self.buttonRightBackToRight.setStyleSheet(buttonBackToFrontStyleSheet)

        led_icon = qta.icon('mdi.white-balance-sunny',  color='white')
        self.buttonLed.setIcon(led_icon)
        self.buttonLed.setIconSize(QSize(30, 30))
        self.buttonLed.setStyleSheet('background-color: rgba(0,0,0,0);')
        self.buttonLed.setFlat(True)

        self.sliderLed.resize(16,84)
        self.sliderLed.setMinimum(0)
        self.sliderLed.setMaximum(100)
        self.sliderLed.setSingleStep(10)
        self.sliderLed.setValue(self.options.intensity)
        self.ledIntensityUpdate()

        self.labelTitle.setStyleSheet('color: white')

    def set_layout(self):
        self.small_camera_width = 500
        self.small_camera_height = 360
        self.small_camera_x = 450
        self.small_camera_y = 60
        self.width = self.size().width()
        self.height = self.size().height()
        # self.front_camera_th.changeWinSize(self.size())

        self.set_front_in_focus()

        #Layout buttonMenu
        y = self.size().height() - self.buttonMenu.size().height()
        p = QPoint(5, y-5)
        self.buttonMenu.move(p)

        #Layout buttonLeftBackToLeft
        #y = self.size().height() // 2 - self.buttonLeftBackToLeft.size().height()
        #p = QPoint(5, y)
        #self.buttonLeftBackToLeft.move(p)

        #Layout buttonRightBackToRight
        #y = self.size().height() // 2 - self.buttonRightBackToRight.size().height()
        #x = self.size().width() - self.buttonRightBackToRight.size().width()
        #p = QPoint(x+40, y)
        #self.buttonRightBackToRight.move(p)

        #Layout buttonLed
        x = self.size().width() - self.buttonLed.size().width()
        p = QPoint(x, 0)
        self.buttonLed.move(p)

        #Layout sliderLed
        x += self.buttonLed.size().width()//2 - self.sliderLed.size().width()//2
        p = QPoint(x, self.buttonLed.size().height())
        self.sliderLed.move(p)

        #Layout labelTitle
        if self.options.display_title:
            title = f'{self.options.app_name} v{self.options.app_version}'
        else:
            title = ''

        self.labelTitle.setText(title)

        x = self.size().width()//2 - self.labelTitle.size().width()//2
        p = QPoint(x, 7)
        self.labelTitle.move(p)

    def set_front_in_focus(self):
        self.front_camera.resize(self.width, self.height)
        self.front_camera_th.camera.set_camera_size(self.width, self.height)
        self.front_camera_th.changeWinSize(self.front_camera.size())

        self.front_camera_th.camera.set_cap_frame_width(640)
        self.front_camera_th.camera.set_cap_frame_height(480)
        self.front_camera_th.camera.set_cap_fps(10)
        self.front_camera_th.delay = 0.1

        #self.left_camera.resize(self.small_camera_width,
        #                        self.small_camera_height)
        #self.left_camera_th.camera.set_camera_size(self.small_camera_width,
        #                                           self.small_camera_height)
        #self.left_camera_th.changeWinSize(self.left_camera.size())

        #p = QPoint(5-self.small_camera_x, self.small_camera_y)
        #self.left_camera.move(p)

        #self.left_camera_th.camera.set_cap_frame_width(320)
        #self.left_camera_th.camera.set_cap_frame_height(240)
        #self.left_camera_th.camera.set_cap_fps(10)

        #self.right_camera.resize(self.small_camera_width,
        #                         self.small_camera_height)
        #self.right_camera_th.camera.set_camera_size(self.small_camera_width,
        #                                            self.small_camera_height)
        #self.right_camera_th.changeWinSize(self.right_camera.size())

        #x = self.size().width() - self.right_camera.size().width()
        #p = QPoint(x-5+self.small_camera_x, self.small_camera_y)
        #self.right_camera.move(p)

        #self.right_camera_th.camera.set_cap_frame_width(320)
        #self.right_camera_th.camera.set_cap_frame_height(240)
        #self.right_camera_th.camera.set_cap_fps(10)

        #self.buttonLeftBackToLeft.hide()
        #self.buttonRightBackToRight.hide()

    #def set_right_in_focus(self):
    #    self.front_camera_th.camera.set_cap_frame_width(320)
    #    self.front_camera_th.camera.set_cap_frame_height(240)
    #    self.front_camera_th.camera.set_cap_fps(10)
    #    self.front_camera_th.delay = 1

    #    camera_widht = self.width - 20
    #    camera_height = self.height - 20
    #    self.right_camera.resize(camera_widht, camera_height)
    #    self.right_camera_th.camera.set_camera_size(camera_widht, camera_height)
    #    self.right_camera_th.changeWinSize(self.right_camera.size())

    #    self.right_camera_th.camera.set_cap_frame_width(640)
    #    self.right_camera_th.camera.set_cap_frame_height(480)
    #    self.right_camera_th.camera.set_cap_fps(10)

    #    p = QPoint(10, 10)
    #    self.right_camera.move(p)

    #    self.left_camera_th.camera.set_cap_frame_width(160)
    #    self.left_camera_th.camera.set_cap_frame_height(120)
    #    self.left_camera_th.camera.set_cap_fps(10)

    #    self.buttonRightBackToRight.show()

    #def set_left_in_focus(self):
    #    self.front_camera_th.camera.set_cap_frame_width(320)
    #    self.front_camera_th.camera.set_cap_frame_height(240)
    #    self.front_camera_th.camera.set_cap_fps(10)
    #    self.front_camera_th.delay = 0.1

    #    camera_widht = self.width - 20
    #    camera_height = self.height - 20
    #    self.left_camera.resize(camera_widht, camera_height)
    #    self.left_camera_th.camera.set_camera_size(camera_widht, camera_height)
    #    self.left_camera_th.changeWinSize(self.left_camera.size())

    #    self.left_camera_th.camera.set_cap_frame_width(640)
    #    self.left_camera_th.camera.set_cap_frame_height(480)
    #    self.left_camera_th.camera.set_cap_fps(10)

    #    p = QPoint(10, 10)
    #    self.left_camera.move(p)

    #    self.right_camera_th.camera.set_cap_frame_width(160)
    #    self.right_camera_th.camera.set_cap_frame_height(120)
    #    self.right_camera_th.camera.set_cap_fps(10)

    #    self.buttonLeftBackToLeft.show()

    def resizeEvent(self, p):
        self.set_layout()

    def showEvent(self, event):
        self.set_style()
        self.set_layout()
        self.startCapture()

        self.th_kb.start()

        #self.show()

    @pyqtSlot(dict)
    def setImage(self, signal_dict):
        if signal_dict['id'] == 'front':
            self.front_camera.setPixmap(QPixmap.fromImage(signal_dict['image']))

        #if signal_dict['id'] == 'left':
            #self.left_camera.setPixmap(QPixmap.fromImage(signal_dict['image']))

        #if signal_dict['id'] == 'right':
            #self.right_camera.setPixmap(QPixmap.fromImage(signal_dict['image']))

    def led_update(self, delta):
        intensity = self.sliderLed.value()
        intensity += delta
        self.sliderLed.setValue(intensity)

    def startCapture(self):
        self.front_camera_th.changePixmap.connect(self.setImage)
        #self.left_camera_th.changePixmap.connect(self.setImage)
        #self.right_camera_th.changePixmap.connect(self.setImage)

        self.front_camera_th.start()
        #self.left_camera_th.start()
        #self.right_camera_th.start()

    def backToMenu(self):
        self.th_kb.stop()

        # if self.bs_core.running_on_arm and self.bs_core.actuators.led.available:
            # self.bs_core.actuators.led.intensity = 0


        self.hide()

        self.front_camera_th.working = False
        #self.left_camera_th.working = False
        #self.right_camera_th.working = False

        self.ledFinalValue()

        self.lost_focus.emit(True)
        self.close()

    def toggleLedsSlider(self):
        if self.sliderLed.isVisible():
            self.sliderLed.hide()
            intensity = self.sliderLed.value()
            logging.info(f'LED intensity changed to {intensity}')
        else:
            self.sliderLed.show()

    def ledIntensityUpdate(self):
        intensity = self.sliderLed.value()
        self.options.intensity = intensity
        try:
            self.pwmL1.start(intensity)
            self.pwmL2.start(intensity)
            # print(f'intensity={intensity}')
        except:
            pass

        # if self.bs_core.running_on_arm and self.bs_core.actuators.led.available:
            # self.bs_core.actuators.led.intensity = intensity

    def closeEvent(self, event):
        self.front_camera_th.stop()
        #self.left_camera_th.stop()
        #self.right_camera_th.stop()
        self.ledFinalValue()

        try:
            self.pwmL1.stop()
            self.pwmL2.stop()
        except:
            pass

        event.accept()

    def ledFinalValue(self):
        intensity = self.sliderLed.value()
        logging.info(f'LED intensity changed to {intensity}.')
        logging.info('But window was closed so LED was turned OFF')

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK':
            self.backToMenu()

        if key == 'HOME':
            self.set_front_in_focus()

        if key == 'UP':
            pass

        if key == 'DOWN':
            pass

        #if key == 'LEFT':
            #self.set_left_in_focus()

        #if key == 'RIGHT':
            #self.set_right_in_focus()


if __name__ == "__main__":
    from types import SimpleNamespace
    options = SimpleNamespace()
    options.app_name = __name__
    options.app_version = 0.99
    options.running_on_arm = True
    options.screen_width = 800
    options.screen_height = 480
    options.display_title = True
    options.i2c_port = 1
    options.imu = False
    options.display_angles = True
    options.camerapath = "/dev/video0"

    app = QApplication([])
    start_window = CameraWindow(options)
    start_window.show()
    app.exit(app.exec_())

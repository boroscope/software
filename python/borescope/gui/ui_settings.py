#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import platform, subprocess
import logging

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QStackedWidget, QListWidget, QListWidgetItem
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel, QComboBox, QSlider
from PyQt5.QtWidgets import QSpinBox, QAbstractSpinBox, QFrame
from PyQt5.QtGui import QPainter, QPen, QColor, QFont, QPalette
from PyQt5.QtGui import QPainter, QPixmap
from PyQt5.QtCore import Qt, QSize, QPropertyAnimation, QParallelAnimationGroup
from PyQt5.QtCore import pyqtSlot, pyqtSignal

from qtwidgets import AnimatedToggle

from borescope.gui.ui_tools import Color
from borescope.gui.thread_keyboard import KBThread

class SystemSettingsWindow(QWidget):
    lost_focus = pyqtSignal(bool)
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.bs_core = core

        self.th_kb = KBThread(self)
        self.th_kb.pressed_button.connect(self.pressed_button)

        self.left = 0
        self.top = 0
        self.width = self.options.screen_width
        self.height = self.options.screen_height
        self.setGeometry(self.left, self.top, self.width, self.height)

        if not self.options.running_on_arm:
            self.setFixedWidth(self.width)
            self.setFixedHeight(self.height)

        self.setting_pages = [
                {'name': 'Sensors',
                 'settings_lines':
                    [
                        {
                        'name': 'Inertial Unit',
                        'from': ['true', 'false', 'emulated'],
                        'change_option': self.change_IMU_option,
                        'get_current_index': self.get_IMU_current_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Power Management',
                        'from': ['true', 'false', 'emulated'],
                        'change_option': self.change_power_option,
                        'get_current_index': self.get_power_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Camera',
                        'from': self.available_cameras,
                        'change_option': self.change_camera_option,
                        'get_current_index': self.get_camera_option,
                        'widget': 'cameraComboBox'
                        },
                    ],
                },
                {'name': 'Drivers',
                 'settings_lines':
                    [
                        {
                        'name': 'PWM',
                        'from': ['true', 'false'],
                        'change_option': self.change_pwm_option,
                        'get_current_index': self.get_pwm_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'LED',
                        'from': '',
                        'change_option': '',
                        'get_current_index': self.get_led_intensity,
                        'widget': 'sliderLED'
                        },
                        {
                        'name': 'LED Test',
                        'from': '',
                        'change_option': '',
                        'get_current_index': '',
                        'widget': 'toggle'
                        },
                    ],
                },
                {'name': 'App',
                 'settings_lines':
                    [
                        {
                        'name': 'Display Title',
                        'from': ['true', 'false'],
                        'change_option': self.change_title_option,
                        'get_current_index': self.get_title_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Display Angles',
                        'from': ['true', 'false'],
                        'change_option': self.change_angles_option,
                        'get_current_index': self.get_angles_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Display Horizon',
                        'from': ['true', 'false'],
                        'change_option': self.change_horizon_option,
                        'get_current_index': self.get_horizon_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Horizon Thickness',
                        'from': [str(x+1) for x in range(5)],
                        'change_option': self.change_horizon_thickness_option,
                        'get_current_index': self.get_horizon_thickness_option,
                        'widget': 'comboBox'
                        },
                        {
                        'name': 'Horizon Color',
                        'from': '',
                        'change_option': '',
                        'get_current_index': '',
                        'widget': 'horizonColorBlock'
                        },
                        {
                        'name': 'Battery Capacity',
                        'from': '',
                        'change_option': self.change_battery_capacity,
                        'get_current_index': self.get_battery_capacity_option,
                        'widget': 'spinBatteryCapacity'
                        },
                        {
                        'name': 'Battery SoC',
                        'from': '',
                        'change_option': self.change_battery_soc,
                        'get_current_index': self.get_battery_soc_option,
                        'widget': 'spinBatterySoC'
                        },
                    ],
                },
                ]

        self.led_test_on = False

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.setWindowFlag(Qt.FramelessWindowHint)

        image = 'python/borescope/gui/images/fondo-borescope.png'
        self.background_image = QPixmap(image)

    def setUI(self):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.buttonMenu = QPushButton(self, text='Back')
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.listSettings = QListWidget(self)
        self.stackedSettings = QStackedWidget(self)

        labelStyleSheet = """
            font-size: 15px;
            color: white;
            background: black;
            margin: 0px;
        """

        comboStyleSheet = """
            font-size: 15px;
            padding-left: 7px;
            color: white;
            background: black;
            selection-background-color: grey;
            border: 1px solid grey;
            border-radius: 3px;
        """

        spinStyleSheet = """
            QSpinBox {
                font-size: 17px;
                color: white;
                border: 1px solid grey;
            }
            QSpinBox::up-button {
                subcontrol-origin: margin;
                subcontrol-position: center right;
                background-color: black;
                background-image: url(/usr/share/icons/gnome/16x16/actions/media-playback-start.png);
                right: 1px;
                height: 16px;
                width: 16px;
            }
            QSpinBox::down-button {
                subcontrol-origin: margin;
                subcontrol-position: center left;
                background-color: black;
                background-image: url(/usr/share/icons/gnome/16x16/actions/media-playback-start-rtl.png);
                left: 1px;
                height: 16px;
                width: 16px;
            }
        """

        for i in range(len(self.setting_pages)):
            item = QListWidgetItem()
            item.setText(self.setting_pages[i].get('name'))
            item.setSizeHint(QSize(100, 50))
            item.setTextAlignment(Qt.AlignCenter)
            self.listSettings.addItem(item)

        for i in range(len(self.setting_pages)):
            page = QWidget()
            layout= QVBoxLayout()

            for line in self.setting_pages[i].get('settings_lines'):
                layoutLine = QHBoxLayout()
                labelLeft = QLabel()
                labelLeft.setStyleSheet(labelStyleSheet)
                labelLeft.setText(f"{line.get('name')}:")
                layoutLine.addWidget(labelLeft, 0, Qt.AlignRight|Qt.AlignTop)

                if line.get('widget') is 'comboBox':
                    comboRight = QComboBox()
                    comboRight.setStyleSheet(comboStyleSheet)
                    comboRight.addItems(line.get('from'))
                    comboRight.activated[str].connect(line.get('change_option'))
                    current_index = line.get('get_current_index')()
                    comboRight.setCurrentIndex(current_index)
                    layoutLine.addWidget(comboRight)

                if line.get('widget') is 'sliderLED':
                    self.sliderLed = QSlider(Qt.Horizontal, self)
                    self.sliderLed.valueChanged.connect(self.ledIntensityUpdate)
                    self.sliderLed.setMinimum(0)
                    self.sliderLed.setMaximum(100)
                    self.sliderLed.setSingleStep(10)
                    self.sliderLed.setValue(self.options.intensity)
                    layoutLine.addWidget(self.sliderLed)

                if line.get('widget') is 'toggle':
                    layoutLEDLine = QHBoxLayout()
                    self.labelLEDStatus = QLabel()
                    self.toggleTestLED = AnimatedToggle()
                    self.toggleTestLED.toggled.connect(self.test_led)
                    self.toggleTestLED.setChecked(False)
                    self.labelLEDStatus.setStyleSheet(labelStyleSheet)
                    self.labelLEDStatus.setText('Off')
                    layoutLEDLine.addWidget(self.toggleTestLED)
                    layoutLEDLine.addWidget(self.labelLEDStatus)
                    layoutLine.addLayout(layoutLEDLine)

                if line.get('widget') is 'cameraComboBox':
                    comboRight = QComboBox()
                    comboRight.setStyleSheet(comboStyleSheet)
                    comboRight.addItems(line.get('from')()) #from have a method
                    comboRight.activated[str].connect(line.get('change_option'))
                    current_index = line.get('get_current_index')()
                    comboRight.setCurrentIndex(current_index)
                    layoutLine.addWidget(comboRight)

                if line.get('widget') is 'horizonColorBlock':
                    layoutColorBlock = QVBoxLayout()

                    layoutColorLine = QHBoxLayout()
                    labelRColor = QLabel()
                    labelRColor.setText('Red:')
                    labelRColor.setStyleSheet(labelStyleSheet)
                    self.spinRColor = QSpinBox()
                    self.spinRColor.setStyleSheet(spinStyleSheet)
                    self.spinRColor.setMinimum(0)
                    self.spinRColor.setMaximum(255)
                    horizon_color_R = self.options.horizon_color[0]
                    self.spinRColor.setValue(horizon_color_R)
                    self.spinRColor.setAlignment(Qt.AlignHCenter)
                    self.spinRColor.valueChanged.connect(self.change_horizon_color)
                    layoutColorLine.addWidget(labelRColor, 0, Qt.AlignRight)
                    layoutColorLine.addWidget(self.spinRColor)
                    layoutColorBlock.addLayout(layoutColorLine)

                    layoutColorLine = QHBoxLayout()
                    labelGColor = QLabel()
                    labelGColor.setText('Green:')
                    labelGColor.setStyleSheet(labelStyleSheet)
                    self.spinGColor = QSpinBox()
                    self.spinGColor.setStyleSheet(spinStyleSheet)
                    self.spinGColor.setMinimum(0)
                    self.spinGColor.setMaximum(255)
                    horizon_color_G = self.options.horizon_color[1]
                    self.spinGColor.setValue(horizon_color_G)
                    self.spinGColor.setAlignment(Qt.AlignHCenter)
                    self.spinGColor.valueChanged.connect(self.change_horizon_color)
                    layoutColorLine.addWidget(labelGColor, 0, Qt.AlignRight)
                    layoutColorLine.addWidget(self.spinGColor)
                    layoutColorBlock.addLayout(layoutColorLine)

                    layoutColorLine = QHBoxLayout()
                    labelBColor = QLabel()
                    labelBColor.setText('Blue:')
                    labelBColor.setStyleSheet(labelStyleSheet)
                    self.spinBColor = QSpinBox()
                    self.spinBColor.setStyleSheet(spinStyleSheet)
                    self.spinBColor.setMinimum(0)
                    self.spinBColor.setMaximum(255)
                    horizon_color_B = self.options.horizon_color[2]
                    self.spinBColor.setValue(horizon_color_B)
                    self.spinBColor.setAlignment(Qt.AlignHCenter)
                    self.spinBColor.valueChanged.connect(self.change_horizon_color)
                    layoutColorLine.addWidget(labelBColor, 0, Qt.AlignRight)
                    layoutColorLine.addWidget(self.spinBColor)
                    layoutColorBlock.addLayout(layoutColorLine)

                    self.horizon = QFrame()
                    color = f'rgb({horizon_color_R},{horizon_color_G},{horizon_color_B})'
                    self.horizon.setStyleSheet(f'QFrame {{background: {color}}}')
                    thickness = self.options.horizon_thickness
                    self.horizon.setMaximumHeight(thickness)
                    self.horizon.setMinimumHeight(thickness)

                    layoutColorBlock.addWidget(self.horizon)

                    layoutLine.addLayout(layoutColorBlock)

                if self.options.debug_level >= 1:
                    if line.get('widget') is 'spinBatteryCapacity':
                        self.spinBatteryCapacity = QSpinBox()
                        self.spinBatteryCapacity.setStyleSheet(spinStyleSheet)
                        self.spinBatteryCapacity.setMinimum(0)
                        self.spinBatteryCapacity.setSingleStep(10)
                        self.spinBatteryCapacity.setMaximum(9999)
                        self.spinBatteryCapacity.valueChanged.connect(
                                self.change_battery_capacity)
                        self.spinBatteryCapacity.setValue(
                                int(self.options.battery_capacity))
                        layoutLine.addWidget(self.spinBatteryCapacity)

                if self.options.debug_level >= 1:
                    if line.get('widget') is 'spinBatterySoC':
                        self.spinBatterySoC = QSpinBox()
                        self.spinBatterySoC.setStyleSheet(spinStyleSheet)
                        self.spinBatterySoC.setMinimum(0)
                        self.spinBatterySoC.setSingleStep(10)
                        self.spinBatterySoC.setMaximum(9999)
                        self.spinBatterySoC.valueChanged.connect(
                                self.change_battery_soc)
                        self.spinBatterySoC.setValue(
                                int(self.options.battery_soc))
                        layoutLine.addWidget(self.spinBatterySoC)

                layout.addLayout(layoutLine)


            layout.setAlignment(Qt.AlignTop)
            page.setLayout(layout)

            self.stackedSettings.addWidget(page)

        self.listSettings.currentRowChanged.connect(self.stackedSettings.setCurrentIndex)
        self.listSettings.setCurrentRow(0)

    def setStyle(self):
        self.setStyleSheet("""
                background-color: black;
                """)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        self.listSettings.setStyleSheet("""
                QListWidget {
                    min-width: 120px;
                    max-width: 120px;
                    font-size: 20px;
                    color: white;
                    background: transparent;
                }
                QListWidget::item {
                    background: rgb(30,30,30);
                    border-top-left-radius: 8px;
                    border-bottom-left-radius: 8px;
                    margin: 1px;
                }
                QListWidget::item:selected {
                    border-right-width: 2px;
                    border-right-color: red;
                    border-right-style: solid;
                    background: rgb(70,70,70);
                }
                """)

        self.stackedSettings.setStyleSheet("""
                background: black;
                margin: 0px;
                border-top-right-radius: 15px;
                border-bottom-left-radius: 15px;
                border-bottom-right-radius: 15px;
                """)

        self.listSettings.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.listSettings.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def setWindowLayout(self):

        layoutList = QHBoxLayout()
        layoutList.setSpacing(0)

        layoutList.addWidget(self.listSettings)
        layoutList.addWidget(self.stackedSettings)

        layout = QVBoxLayout()
        layout.addLayout(layoutList)
        layout.addWidget(self.buttonMenu, 0, Qt.AlignRight | Qt.AlignBottom)

        self.setLayout(layout)

    def showEvent(self, event):
        if self.options.running_on_arm:
            self.showMaximized()
        else:
            self.show()

        self.th_kb.start()

    def backToMenu(self):
        for key, value in self.options.changes.items():
            if type(value) is float:
                continue

            if value is not None:
                logging.info(value)

        self.hide()

        self.th_kb.stop()
        self.lost_focus.emit(True)

        self.close()

    def change_IMU_option(self, text):
        if text == 'emulated':
            self.options.imu = 'emulated'
        elif text == 'true':
            self.options.imu = True
        else:
            self.options.imu = False

    def change_power_option(self, text):
        if text == 'emulated':
            self.options.battery = 'emulated'
        elif text == 'true':
            self.options.battery = True
        else:
            self.options.battery = False

    def change_pwm_option(self, text):
        if text == 'true':
            self.options.pwm = True
        else:
            self.options.pwm = False


    def change_title_option(self, text):
        if text == 'true':
            self.options.display_title = True
            msg = 'Display Title option set ON'
        else:
            self.options.display_title = False
            msg = 'Display Title option set OFF'

        self.options.changes['display title'] = msg

    def change_angles_option(self, text):
        if text == 'true':
            self.options.display_angles = True
            msg = 'Display Angles option set ON'
        else:
            self.options.display_angles = False
            msg = 'Display Angles option set OFF'

        self.options.changes['display angles'] = msg

    def get_IMU_current_option(self):
        if self.options.imu is True :
            return 0
        elif self.options.imu is False:
            return 1
        else:
            return 2

    def get_power_option(self):
        if self.options.battery is True:
            return 0
        elif self.options.battery is False:
            return 1
        else:
            return 2

    def get_pwm_option(self):
        if self.options.pwm is True:
            return 0
        else:
            return 1

    def get_title_option(self):
        if self.options.display_title is True:
            return 0
        else:
            return 1

    def get_angles_option(self):
        if self.options.display_angles is True:
            return 0
        else:
            return 1

    def ledIntensityUpdate(self):
        intensity = self.sliderLed.value()
        self.options.intensity = intensity
        # if self.bs_core.running_on_arm and self.bs_core.actuators.led.available:
            # if self.led_test_on:
                # self.bs_core.actuators.led.intensity = intensity
            # else:
                # self.bs_core.actuators.led.intensity = 0

    def get_led_intensity(self):
        return self.options.intensity

    def test_led(self, state):
        if state is True:
            self.led_test_on = True
            self.labelLEDStatus.setText('On')
        else:
            self.led_test_on = False
            self.labelLEDStatus.setText('Off')

        self.ledIntensityUpdate()

    def available_cameras(self):
        command = 'ls /dev/video*'
        output = subprocess.run(command, shell=True, capture_output=True)
        available_cameras = ['No camera']

        if output.stderr:
            self.os_dev_video = []

        if output.stdout:
            out = output.stdout.strip()
            self.os_dev_video = out.decode('utf-8').split('\n')

        return available_cameras + self.os_dev_video

    def change_camera_option(self, text):
        if text == 'No camera':
            self.options.front_camera_path = False
        else:
            self.options.front_camera_path = text

        self.options.camera_updated = True

    def get_camera_option(self):
        if self.options.front_camera_path is False:
            return 0
        else:
            cameras_list = self.os_dev_video
            if self.options.front_camera_path in cameras_list:
                return cameras_list.index(self.options.front_camera_path)+1
            else:
                return 0

    def change_horizon_option(self, text):
        if text == 'true':
            self.options.display_horizon = True
        else:
            self.options.display_horizon = False

        msg = f'Display horizon changed to {self.options.display_horizon}'
        self.options.changes['display horizon'] = msg

    def change_horizon_thickness_option(self, text):
        old_thickness = self.options.horizon_thickness

        thickness = int(text)

        min_height_animation = QPropertyAnimation(self.horizon, b"minimumHeight")
        min_height_animation.setDuration(10)
        min_height_animation.setStartValue(old_thickness)
        min_height_animation.setEndValue(thickness)

        max_height_animation = QPropertyAnimation(self.horizon, b"maximumHeight")
        max_height_animation.setDuration(10)
        max_height_animation.setStartValue(old_thickness)
        max_height_animation.setEndValue(thickness)

        self.animation = QParallelAnimationGroup()
        self.animation.addAnimation(min_height_animation)
        self.animation.addAnimation(max_height_animation)
        self.animation.alwaysRunToEnd = True
        self.animation.start()

        self.options.horizon_thickness = thickness

        msg = f'Horizon thickness changed to {thickness}'
        self.options.changes['horizon thickness'] = msg

    def get_horizon_option(self):
        if self.options.display_horizon is True:
            return 0
        else:
            return 1

    def get_horizon_thickness_option(self):
        return self.options.horizon_thickness - 1

    def change_horizon_color(self, value):
        r = self.spinRColor.value()
        g = self.spinGColor.value()
        b = self.spinBColor.value()

        color = f'rgb({r},{g},{b})'
        self.horizon.setStyleSheet(f'QFrame {{background: {color}}}')

        self.options.horizon_color = [r, g, b]

        msg = f'Horizon color changed to {color}'
        self.options.changes['horizon color'] = msg

    def change_battery_capacity(self, text):
        self.options.battery_capacity = int(text)

        msg = f'Battery Capacity changed to {self.options.battery_capacity}'
        self.options.changes['battery capacity'] = msg
        self.options.changes['battery capacity value'] = int(text)

    def get_battery_capacity_option(self):
        return self.options.battery_capacity

    def change_battery_soc(self, text):
        self.options.battery_soc = int(text)

        msg = f'Battery SoC changed to {self.options.battery_soc}'
        self.options.changes['battery soc'] = msg
        self.options.changes['battery soc value'] = int(text)

    def get_battery_soc_option(self):
        return self.options.battery_soc

    @pyqtSlot(str)
    def pressed_button(self, key):
        if key == 'BACK': #home
            self.backToMenu()

        if key == 'HOME': #back
            pass

        if key == 'UP':
            pass

        if key == 'DOWN':
            pass

        if key == 'LEFT':
            pass

        if key == 'RIGHT':
            pass

    def paintEvent(self, event):
        # Dibujar la imagen de fondo manteniendo la relación de aspecto
        painter = QPainter(self)
        if not self.background_image.isNull():
            scaled_image = self.background_image.scaled(
                self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation
            )
            # Centrar la imagen escalada en el widget
            x_offset = (self.width - scaled_image.width()) // 2
            y_offset = (self.height - scaled_image.height()) // 2
            painter.drawPixmap(x_offset, y_offset, scaled_image)

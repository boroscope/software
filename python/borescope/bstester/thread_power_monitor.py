import time

from PyQt5.QtCore import QThread, pyqtSignal


class ThreadPowerMonitor(QThread):
    change_counter = pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.working = False
        self.delay = 0.5

    def run(self):
        self.working = True
        counter = 0
        while self.working:
            time.sleep(self.delay)
            if counter == 1000:
                counter = 0

            self.change_counter.emit(counter)
            counter +=1


    def stop(self):
        self.working = False
        self.exit(0)


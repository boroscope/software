#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time

from PyQt5.QtGui import QImage
from PyQt5.QtCore import QThread, pyqtSignal

from borescope.modules.service_sensors import Sensors

class IMUThread(QThread):
    change_gyro = pyqtSignal(tuple)
    change_accel = pyqtSignal(tuple)
    change_magneto = pyqtSignal(tuple)
    change_baro = pyqtSignal(tuple)

    def __init__(self, parent=None, options=None):
        super().__init__()
        self.options = options

        self.sensors = Sensors(self.options, device='imu')
        self.sensors.discover('imu')

        self.working = False
        self.delay = 0.01

    def run(self):
        self.working = True
        i = 0
        while self.working:
            time.sleep(self.delay)

            gyro = self.sensors.imu.gyro_rad
            accel = self.sensors.imu.accel
            magneto = self.sensors.imu.magneto
            baro = self.sensors.imu.baro

            self.change_gyro.emit(gyro)
            self.change_accel.emit(accel)
            self.change_magneto.emit(magneto)
            self.change_baro.emit(baro)

    def stop(self):
        self.working = False
        self.exit(0)

from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt, pyqtSlot

from borescope.bstester.thread_keyboard import ThreadKeyboard

class TestKeyboardWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()
        self.thread_keyboard = ThreadKeyboard()

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.thread_keyboard.change_kb.connect(self.set_keyboard)
        self.thread_keyboard.start()

    def setUI(self):
        self.title = 'Borescope Tester - Keyboard'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.labelTitle = QLabel(self.title)
        self.label_counter = QLabel()

        self.label_ok = QLabel('Ok')
        self.label_back = QLabel('Back')

        self.label_up = QLabel('Up')

        self.label_down = QLabel('Down')

        self.label_left = QLabel('Left')

        self.label_right = QLabel('Right')

        self.buttonMenu = QLabel('Menu')

    def setStyle(self):
        self.setStyleSheet("""
                background-color: black;
                """)
        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 28px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)
        self.label_options_off = """
                background-color: black;
                border: 1px solid grey;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                font-size: 20px;
                color: grey;
                """
        self.label_options_on = """
                background-color: grey;
                border: 1px solid grey;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                font-size: 20px;
                color: grey;
                """
        self.label_title_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 30px;
                color: grey
                """
        self.label_counter_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 30px;
                color: grey
                """

        self.labelTitle.setStyleSheet(self.label_title_style)
        self.label_counter.setStyleSheet(self.label_counter_style)
        self.label_up.setStyleSheet(self.label_options_off)
        self.label_down.setStyleSheet(self.label_options_off)
        self.label_left.setStyleSheet(self.label_options_off)
        self.label_right.setStyleSheet(self.label_options_off)
        self.label_ok.setStyleSheet(self.label_options_off)
        self.label_back.setStyleSheet(self.label_options_off)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()
        gridLayout = QGridLayout()
        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        self.label_counter.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.label_counter)

        self.label_left.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_left, 0, 0)

        self.label_up.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_up, 0, 1)

        self.label_right.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_right, 0, 2)

        self.label_ok.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_ok, 1, 0)

        self.label_down.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_down, 1, 1)

        self.label_back.setAlignment(Qt.AlignHCenter)
        gridLayout.addWidget(self.label_back, 1, 2)
        layoutV.addLayout(gridLayout)

        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.hide()

    def closeEvent(self, event):
        self.thread_keyboard.stop()
        event.accept()

    def off_layout(self, button): 
        if button == 'BACK':
            time.sleep(100)
            self.label_back.SetStylesheet(self.label_options_off) 
        if button == 'OK':
            time.sleep(100)
            self.label_ok.SetStylesheet(self.label_options_off)
        if button == 'UP':
            time.sleep(100)
            self.label_up.SetStylesheet(self.label_options_off)
        if button == 'DOWN':
            time.sleep(100)
            self.label_down.SetStylesheet(self.label_options_off)
        if button == 'RIGHT':
            time.sleep(100)
            self.label_right.SetStylesheet(self.label_options_off)
        if button == 'LEFT':
            time.sleep(100)
            self.label_left.SetStylesheet(self.label_options_off)

    @pyqtSlot(str)
    def set_keyboard(self, key):
        print(button)
        button_discover = f'counter strike'
        self.label_counter.setText(button_discover)
        if button == 'BACK':
            self.label_back.SetStylesheet(self.label_options_on)
            self.off_layout(button)
        if button == 'OK':
            self.label_ok.SetStylesheet(self.label_options_on)
            self.off_layout(button)
        if button == 'UP':
            self.label_up.SetStylesheet(self.label_options_on)
            self.off_layout(button)
        if button == 'DOWN':
            self.label_down.SetStylesheet(self.label_options_on)
            self.off_layout(button)
        if button == 'RIGHT':
            self.label_right.SetStylesheet(self.label_options_on)
            self.off_layout(button)
        if button == 'LEFT':
            self.label_left.SetStylesheet(self.label_options_on)
            self.off_layout(button)


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import platform
from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt, pyqtSlot

from borescope.gui.ui_tools import Color
from borescope.bstester.thread_imu import IMUThread

class TestIMUWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()
        
        if platform.machine() in ['armv7l', 'aarch64']:
            self.options.running_on_arm = True
            self.running_on_arm = True
            self.options.imu = True
        else:
            self.options.running_on_arm = False
            self.running_on_arm = False
            self.options.imu = 'emulated'
              
        self.thread_imu = IMUThread(self, self.options)

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.thread_imu.change_gyro.connect(self.set_gyro)
        self.thread_imu.change_accel.connect(self.set_accel)
        self.thread_imu.change_magneto.connect(self.set_magneto)
        self.thread_imu.change_baro.connect(self.set_baro)
        self.thread_imu.start()

    def setUI(self):
        self.title = 'Boresope Tester - IMU'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.labelTitle = QLabel(self.title)

        self.label_gx = QLabel()
        self.label_gy = QLabel()
        self.label_gz = QLabel()

        self.label_ax = QLabel()
        self.label_ay = QLabel()
        self.label_az = QLabel()

        self.label_mgx = QLabel()
        self.label_mgy = QLabel()
        self.label_mgz = QLabel()

        self.label_baro = QLabel()

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)

    def setStyle(self):
        
        windowStyleSheet = """
                background-color: black;
                """
        self.setStyleSheet(windowStyleSheet)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        label_title_style = """
                background-color: black;
                border: 1px solid black;
                font-size:30px;
                color: grey
                """

        label_gyro_style = """
                background-color: black;
                border: 1px solid black;
                font-size:20px;
                color: white
                """

        self.labelTitle.setStyleSheet(label_title_style)

        self.label_gx.setStyleSheet(label_gyro_style)
        self.label_gy.setStyleSheet(label_gyro_style)
        self.label_gz.setStyleSheet(label_gyro_style)
        
        self.label_ax.setStyleSheet(label_gyro_style)
        self.label_ay.setStyleSheet(label_gyro_style)
        self.label_az.setStyleSheet(label_gyro_style)

        self.label_mgx.setStyleSheet(label_gyro_style)
        self.label_mgy.setStyleSheet(label_gyro_style)
        self.label_mgz.setStyleSheet(label_gyro_style)

        self.label_baro.setStyleSheet(label_gyro_style)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()

        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        layoutH = QHBoxLayout()
        layoutH.addWidget(Color('black'))
        layoutH.addWidget(self.label_gx)
        layoutH.addWidget(self.label_gy)
        layoutH.addWidget(self.label_gz)
        layoutH.addWidget(Color('black'))
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(Color('black'))
        layoutH.addWidget(self.label_ax)
        layoutH.addWidget(self.label_ay)
        layoutH.addWidget(self.label_az)
        layoutH.addWidget(Color('black'))
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(Color('black'))
        layoutH.addWidget(self.label_mgx)
        layoutH.addWidget(self.label_mgy)
        layoutH.addWidget(self.label_mgz)
        layoutH.addWidget(Color('black'))
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        self.label_baro.setAlignment(Qt.AlignHCenter)
        layoutH.addWidget(self.label_baro)
        layoutV.addLayout(layoutH)

        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.close()

    def closeEvent(self, event):
        self.thread_imu.stop()
        event.accept()

    @pyqtSlot(tuple)
    def set_gyro(self, gyro):
        if gyro[0] is not None:
            gx, gy, gz = gyro
            gx_text = f'gx:{gx:8.3f}'
            gy_text = f'gy:{gy:8.3f}'
            gz_text = f'gz:{gz:8.3f}'
            self.label_gx.setText(gx_text)
            self.label_gy.setText(gy_text)
            self.label_gz.setText(gz_text)
        else:
            gx_text = 'gx: N/D'
            gy_text = 'gy: N/D'
            gz_text = 'gz: N/D'
            self.label_gx.setText(gx_text)
            self.label_gy.setText(gy_text)
            self.label_gz.setText(gz_text)

    @pyqtSlot(tuple)
    def set_accel(self, accel):
        if accel[0] is not None:
            ax, ay, az = accel
            ax_text = f'ax:{ax:8.3f}'
            ay_text = f'ay:{ay:8.3f}'
            az_text = f'az:{az:8.3f}'
            self.label_ax.setText(ax_text)
            self.label_ay.setText(ay_text)
            self.label_az.setText(az_text)
        else:
            ax_text = 'ax: N/D'
            ay_text = 'ay: N/D'
            az_text = 'az: N/D'
            self.label_ax.setText(ax_text)
            self.label_ay.setText(ay_text)
            self.label_az.setText(az_text)

    @pyqtSlot(tuple)
    def set_magneto(self, magneto):
        if magneto[0] is not None:
            mgx, mgy, mgz = magneto
            mgx_text = f'mx:{mgx:8.3f}'
            mgy_text = f'my:{mgy:8.3f}'
            mgz_text = f'mz:{mgz:8.3f}'
            self.label_mgx.setText(mgx_text)
            self.label_mgy.setText(mgy_text)
            self.label_mgz.setText(mgz_text)
        else:
            mgx_text = 'mx: N/D'
            mgy_text = 'my: N/D'
            mgz_text = 'mz: N/D'
            self.label_mgx.setText(mgx_text)
            self.label_mgy.setText(mgy_text)
            self.label_mgz.setText(mgz_text)

    @pyqtSlot(tuple)
    def set_baro(self, baro):
        if baro[0] is not None:
            baro_text = f'baro:{baro:5}'
            self.label_baro.setText(baro_text)
        else:
            baro_text = 'baro: N/D'
            self.label_baro.setText(baro_text)

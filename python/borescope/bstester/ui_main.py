#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt

from borescope.gui.ui_tools import Color
from borescope.bstester.ui_test_imu import TestIMUWindow
from borescope.bstester.ui_test_servo import TestServoWindow
from borescope.bstester.ui_test_power_monitor import TestPowerMonitorWindow
from borescope.bstester.ui_test_led import TestLEDWindow
from borescope.bstester.ui_test_camera import TestCameraWindow

class startWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setUI()
        self.setStyle()
        self.setLayout()

    def setUI(self):
        title = 'Borescope Tester'
        self.setWindowTitle(title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.labelTitle = QLabel(title)

        self.buttonIMU = QPushButton('IMU')
        self.buttonIMU.clicked.connect(self.startIMUTest)

        self.buttonBattery = QPushButton('Power Monitor')
        #self.buttonBattery.clicked.connect(self.startBatteryTest)

        self.buttonLEDs = QPushButton('LEDs')
        self.buttonLEDs.clicked.connect(self.startLEDTest)

        self.buttonServos = QPushButton('Servos')
        #self.buttonServos.clicked.connect(self.startServosTest)

        self.buttonCamera = QPushButton('Camera')
        #self.buttonCamera.clicked.connect(self.startCameraTest)

        self.buttonKeyboard = QPushButton('Keyboard')
        self.buttonKeyboard.clicked.connect(self.startKeyboardTest)

        self.buttonExit = QPushButton(self, text='Exit Tester')
        self.buttonExit.clicked.connect(self.exitApp)

        self.buttonKeyboard = QPushButton('Keyboard')
        self.buttonKeyboard.clicked.connect(self.startKeyboardTest)

    def setStyle(self):
        label_style = """
                background-color: black;
                border: 1px solid black;
                font-size:30px;
                color:grey
                """

        button_style = """
                background-color: grey;
                font-size: 30px;
                padding: 6px;
                border-radius: 10px;
                """

        self.buttonExit.setStyleSheet("""
                background-color: red;
                font-size: 25px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        self.labelTitle.setStyleSheet(label_style)

        self.buttonIMU.setStyleSheet(button_style)
        self.buttonBattery.setStyleSheet(button_style)
        self.buttonLEDs.setStyleSheet(button_style)
        self.buttonServos.setStyleSheet(button_style)
        self.buttonCamera.setStyleSheet(button_style)
        self.buttonKeyboard.setStyleSheet(button_style)

    def setLayout(self):
        layoutV = QVBoxLayout()

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)
        layoutV.addWidget(Color('black'))


        layoutH = QHBoxLayout()
        layoutH.addWidget(self.buttonIMU)
        layoutH.addWidget(self.buttonCamera)
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.buttonLEDs)
        layoutH.addWidget(self.buttonServos)
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.buttonBattery)
        layoutH.addWidget(self.buttonKeyboard)
        layoutV.addLayout(layoutH)

        layoutV.addWidget(Color('black'))
        layoutV.addWidget(self.buttonExit, 0, Qt.AlignHCenter)

        widget = Color('black')
        widget.setLayout(layoutV)
        self.setCentralWidget(widget)

    def startIMUTest(self):
        self.test_imu_windows = TestIMUWindow()
        self.test_imu_windows.show()

    def startBatteryTest(self):
        self.test_battery_windows = TestPowerMonitorWindow()
        self.test_battery_windows.show()

    def startServosTest(self):
        self.test_servos_windows = TestServoWindow()
        self.test_servos_windows.show()

    def startLEDTest(self):
        self.test_led_windows = TestLEDWindow()
        self.test_led_windows.show()

    def exitApp(self):
        self.test_imu_windows = None
        self.test_battery_windows = None
        self.test_led_windows = None
        self.close()

    def startCameraTest(self):
        self.test_camera_windows = TestCameraWindow()
        self.test_camera_windows.show()

    def startKeyboardTest(self):
        pass

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import platform
from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider
from PyQt5.QtCore import Qt

from borescope.modules.service_actuators import Actuators

class TestLEDWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()

        self.load_options()

        self.actuators = Actuators(self.options)
        self.actuators.discover('led')

        self.actuators.led.enable()

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

    def __del__(self):
        self.actuators.led.disable()

    def load_options(self):
        self.options.pwm = True

        if platform.machine() == 'aarch64':
            self.options.running_on_arm = True
        else:
            self.options.running_on_arm = False

    def setUI(self):
        self.title = 'Boresope Tester - LED'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.labelTitle = QLabel(self.title)
        self.labelIntensity1 = QLabel(f'{0:3}') 
        self.labelIntensity1.setFixedWidth(45)
        self.labelIntensity2 = QLabel(f'{0:3}') 
        self.labelIntensity2.setFixedWidth(45)
        self.labelLED1 = QLabel('LED 1')
        self.labelLED2 = QLabel('LED 2')

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)
        self.buttonLink = QPushButton('Link LEDs')
        self.buttonLink.setCheckable(True)
        
        self.sliderLED1 = QSlider(Qt.Horizontal)
        self.sliderLED1.valueChanged.connect(self.led1IntensityUpdate)
        self.sliderLED1.setMinimum(0)
        self.sliderLED1.setMaximum(100)
        self.sliderLED1.setSingleStep(10)
        self.sliderLED1.setValue(0)
        
        self.sliderLED2 = QSlider(Qt.Horizontal)
        self.sliderLED2.valueChanged.connect(self.led2IntensityUpdate)
        self.sliderLED2.setMinimum(0)
        self.sliderLED2.setMaximum(100)
        self.sliderLED2.setSingleStep(10)
        self.sliderLED2.setValue(0)

    def setStyle(self):
        self.setStyleSheet("""
                background-color: black;
                """)

        button_menu_style = """
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """
        
        button_link_style = """
                QPushButton {
                    background-color: grey;
                    font-size: 20px;
                    padding-left: 5px; padding-right: 5px;
                    padding-top: 1px; padding-bottom: 1px;
                    border-radius: 6px;
                }
                QPushButton:checked {
                    background-color: cyan;
                    font-size: 20px;
                    padding-left: 5px; padding-right: 5px;
                    padding-top: 1px; padding-bottom: 1px;
                    border-radius: 6px;
                }
                """

        slider_led_style = """
                background-color: none;
                border-radius: 10px;
                """

        label_title_style = """
                background-color: black;
                border: 1px solid black;
                font-size:30px;
                color: grey;
                """
        
        label_intensity_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 20px;
                color: white;
                """

        self.labelTitle.setStyleSheet(label_title_style)

        self.labelIntensity1.setStyleSheet(label_intensity_style)
        self.labelIntensity2.setStyleSheet(label_intensity_style)

        self.labelLED1.setStyleSheet(label_intensity_style)
        self.labelLED2.setStyleSheet(label_intensity_style)

        self.sliderLED1.setStyleSheet(slider_led_style)
        self.sliderLED2.setStyleSheet(slider_led_style)

        self.buttonMenu.setStyleSheet(button_menu_style)
        self.buttonLink.setStyleSheet(button_link_style)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        layoutV.addWidget(self.labelLED1)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.sliderLED1)
        layoutH.addWidget(self.labelIntensity1)
        layoutV.addLayout(layoutH)

        layoutV.addWidget(self.labelLED2)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.sliderLED2)
        layoutH.addWidget(self.labelIntensity2)
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)
        layoutH.addWidget(self.buttonLink, 0, Qt.AlignRight | Qt.AlignBottom)
        layoutV.addLayout(layoutH)

        self.setLayout(layoutV)

    def led1IntensityUpdate(self):
        intensity = self.sliderLED1.value()
        self.labelIntensity1.setText(f'{intensity:3}')

        if self.buttonLink.isChecked():
            self.sliderLED2.setValue(self.sliderLED1.value())
            self.actuators.led.intensity = intensity
            print(f'Linked LEDs: {intensity}')
        else:
            print(f'LED 1: {intensity}')
            self.actuators.led.intensity = intensity

    def led2IntensityUpdate(self):
        intensity = self.sliderLED2.value()
        self.labelIntensity2.setText(f'{intensity:3}')

        if self.buttonLink.isChecked():
            self.sliderLED1.setValue(self.sliderLED2.value())
            self.actuators.led.intensity = intensity
        else:
            print(f'LED 2: {intensity}')
            self.actuators.led.intensity = intensity

    def backToMenu(self):
        self.close()

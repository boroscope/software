from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtCore import Qt, pyqtSlot

from borescope.bstester.thread_power_monitor import ThreadPowerMonitor

class TestPowerMonitorWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()
        self.thread_power_monitor = ThreadPowerMonitor()

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.thread_power_monitor.change_counter.connect(self.set_counter)
        self.thread_power_monitor.start()

    def setUI(self):
        self.title = 'Borescope Tester - Power Monitor'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)
        self.title = 'Borescope Tester \n Power Monitor'

        self.labelTitle = QLabel(self.title)
        self.label_counter = QLabel()

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)

    def setStyle(self):
        self.setStyleSheet("""
                background-color: black;
                """)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        label_title_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 30px;
                color: grey
                """
        label_counter_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 30px;
                color: grey
                """
        self.labelTitle.setStyleSheet(label_title_style)
        self.label_counter.setStyleSheet(label_counter_style)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()

        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        self.label_counter.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.label_counter)

        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.hide()


    def closeEvent(self, event):
        self.thread_power_monitor.stop()
        event.accept()

    @pyqtSlot(int)
    def set_counter(self, counter):
        i = counter
        count_text = f'contador: {i}'
        self.label_counter.setText(count_text)

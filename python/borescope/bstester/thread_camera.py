#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time

from PyQt5.QtGui import QImage
from PyQt5.QtCore import QThread, pyqtSignal

from borescope.modules.service_sensors import Sensors
from borescope.modules.math_tools import RunningAverage

class CameraThread(QThread):
    changePixmap = pyqtSignal(QImage)
    changeFPS = pyqtSignal(float)

    def __init__(self, parent=None, options=None):
        super().__init__()
        self.options = options

        self.imageWidth = 480
        self.imageHeight = 320
        self.working = False
        self.delay = 0.02
        self.fps = RunningAverage(1000)

        self.sensors = Sensors(self.options)
        self.sensors.discover('camera')

    def run(self):
        self.working = True
        while self.working:
            start_time = time.time()
            time.sleep(self.delay)
            if self.sensors.camera.is_active:
                self.sensors.camera.set_frame()
            if self.sensors.camera.is_active:
                rgbImage = self.sensors.camera.get_rgb_frame()
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h,
                                            bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(self.imageWidth,
                                            self.imageHeight)
                self.changePixmap.emit(p)

            camera_fps = 1.0 / (time.time() - start_time)
            self.changeFPS.emit(camera_fps)
            self.fps.add_sample(camera_fps)

    def stop(self):
        self.working = False
        if self.sensors.camera.is_active:
            self.sensors.camera.release_capturing_device()
            self.exit(0)

    def changeWinSize(self,size):
        self.imageWidth = size.width()
        self.imageHeight = size.height()

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.
"""config_stdin_parser

This module is used to parse options given to borescope-tester application
through command line.
"""

import argparse

class StdinParser:
    """Class to catch command line options

    Options:
        * ``--no-X``: Use without Xserver

    Args:
        args: List of strings containing command line arguments

    Attributes:
        raw_args: List of strings containing command line arguments without
            proccesing
        parsed_args: Namespace object containing attributes named like parsed
            options with its respective values

    """
    def __init__(self, args):
        self.raw_args = args
        self.parsed_args = None

        self.setup()

    def setup(self):
        """This method is used to configurate ArgumentParser class from
        argparse module

        options will be saved in a Namespace object:
        * ``--no-X`` will be store in no_x attribute as True or False

        """
        name = 'borescope-tester'
        description = 'Tester App for Borescope App'
        self.parser = argparse.ArgumentParser(
                prog=name,
                description=description)

        self.parser.add_argument('--no-X',
                            action='store',
                            dest='no_x',
                            default=None,
                            help='Use without Xserver. NO_X should be one of \
                                    the following: \
                                    imu|led|servo|pm|kb')

        self.parsed_args = self.parser.parse_args(self.raw_args)

    def get_args(self):
        """Method to return parser arguments

        Returns:
            parsed_args: Namespace with parsed command line options
        """
        return self.parsed_args

import time

from PyQt5.QtCore import QThread, pyqtSignal

from borescope.modules.buttons_class import Buttons

class ThreadKeyboard(QThread):
    pressed_button = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.keyboard = Buttons()
        self.working = False
        self.delay = 0.01
        self.key_was_released = True

    def run(self):
        self.working = True
        sef.key_press = False
        while self.working:
            time.sleep(self.delay)

            key = self.keyboard.get_key()
            if keyboard is not None and self.key_was_released:
                self.key_press = True
                self.key_was_released =False
            if key_press:
                self.pressed_button.emit(key)
                self.key_press = False
            if key is None and not self.key_was_released:
                self.key_press = False
                self.key_was_released = True

    def stop(self):
        self.working = False
        self.exit(0)


#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import platform

from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtWidgets import QSlider
from PyQt5.QtCore import Qt

from borescope.modules.service_actuators import Actuators

class TestServoWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()
        self.options.pwm = True

        self.load_options()

        self.actuators = Actuators(self.options)
        self.actuators.discover('servo')

        self.actuators.servo.enable()

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

    def setUI(self):
        self.title = 'Boresope Tester - Servos'
        self.title_servo1 = 'Servo 1'
        self.title_servo2 = 'Servo 2'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.labelTitle = QLabel(self.title)

        self.labelServo1 = QLabel(self.title_servo1)
        self.labelServo2 = QLabel(self.title_servo2)

        self.slider_servo1 = QSlider(Qt.Horizontal)
        self.slider_servo1.setRange(-90, 90)
        self.slider_servo1.valueChanged.connect(self.value_changed_servo1)

        self.slider_servo2 = QSlider(Qt.Horizontal)
        self.slider_servo2.setRange(-90, 90)
        self.slider_servo2.setSingleStep(5)
        self.slider_servo2.valueChanged.connect(self.value_changed_servo2)

        self.labelPositionServo1 = QLabel(f'{0:3}')
        self.labelPositionServo1.setFixedWidth(45)

        self.labelPositionServo2 = QLabel(f'{0:3}')
        self.labelPositionServo2.setFixedWidth(45)

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)

    def setStyle(self):
        self.setStyleSheet("""
                background-color: black;
                """)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        label_title_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 30px;
                color: grey
                """
        label_title_slider_style = """
            QSlider::groove:horizontal {
                border: 1px solid grey;
                height: 10px;
                background: solid black;
                margin: 2px 0;
            }
            QSlider::handle:horizontal {
                background: solid grey;
                border: 1px solid grey;
                width: 18px;
                margin: -2px 0;
                border-radius: 3px;
        }
        """

        label_position_style = """
                background-color: black;
                border: 1px solid black;
                font-size: 20px;
                color: white;
                """

        self.labelTitle.setStyleSheet(label_title_style)

        self.labelServo1.setStyleSheet(label_title_style)
        self.labelServo2.setStyleSheet(label_title_style)

        self.slider_servo1.setStyleSheet(label_title_slider_style)
        self.slider_servo2.setStyleSheet(label_title_slider_style)

        self.labelPositionServo1.setStyleSheet(label_position_style)
        self.labelPositionServo2.setStyleSheet(label_position_style)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()

        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        layoutV.addWidget(self.labelServo1)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.slider_servo1)
        layoutH.addWidget(self.labelPositionServo1)
        layoutV.addLayout(layoutH)

        layoutV.addWidget(self.labelServo2)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.slider_servo2)
        layoutH.addWidget(self.labelPositionServo2)
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)
        layoutV.addLayout(layoutH)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.actuators.servo.disable()
        self.close()

    def value_changed_servo1(self):
        position = self.slider_servo1.value()
        self.labelPositionServo1.setText(f'{position:3}')
        self.actuators.servo.set_angle(self.actuators.servo.channel1, position)
        print(f'Servo 1 angle: {position}')

    def value_changed_servo2(self):
        position = self.slider_servo2.value()
        self.labelPositionServo2.setText(f'{position:3}')
        self.actuators.servo.set_angle(self.actuators.servo.channel2, position)
        print(f'Servo 2 angle: {position}')

    def load_options(self):
        self.options.pwm = True

        if platform.machine() == 'aarch64':
            self.options.running_on_arm = True
        else:
            self.options.running_on_arm = False

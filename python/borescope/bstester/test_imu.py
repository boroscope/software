#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import time
import curses
import platform

from borescope.modules.service_sensors import Sensors

def run():
	from types import SimpleNamespace
	options = SimpleNamespace()
	stdscr = curses.initscr()
	if platform.machine() in ['armv7l', 'aarch64']:
		options.running_on_arm = True
		running_on_arm = True
		options.imu = True
	else:
		options.running_on_arm = False
		running_on_arm = False
		options.imu = 'emulated'
        
	sensors = Sensors(options, device='imu')
	sensors.discover('imu')
	
	while True:
            	time.sleep(0.01)

            	gyro = sensors.imu.gyro_rad
            	accel = sensors.imu.accel
            	#magneto = sensors.imu.magneto
            	#baro = sensors.imu.baro

            	stdscr.clear()
            	
            	stdscr.addstr(0, 0,f'gx={gyro[0]}||gy={gyro[1]}||gz={gyro[2]}')
            	stdscr.addstr(1, 0,f'ax={accel[0]}||ay={accel[1]}||az={accel[2]}')
            	#print(f'mx={magneto[0]}/my={magneto[1]}/mz={magneto[2]}')
            	#print(f'baro={baro}')
            	
            	stdscr.refresh()
        
	curses.endwin()


if __name__=='__main__':       
	run()

#! /usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
from types import SimpleNamespace

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel, QComboBox
from PyQt5.QtCore import Qt, pyqtSlot, QPoint
from PyQt5.QtGui import QImage, QPixmap, QFont, QIcon

from borescope.bstester.thread_camera import CameraThread
from borescope.modules.service_sensors import Sensors

class TestCameraWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.options = SimpleNamespace()
        self.options.camera = '/dev/video0'
        self.options.screen_height = 480
        self.options.screen_width = 800
        self.options.channels = 3

        self.thread_camera = CameraThread(self, self.options)

        self.setUI()
        self.setStyle()
        self.setWindowLayout()

        self.thread_camera.changePixmap.connect(QImage)
        self.thread_camera.start()

        self.fps = 0.


    def setUI(self):
        self.title = 'Borescope Tester - Camera'
        self.setWindowTitle(self.title)
        self.setFixedWidth(800)
        self.setFixedHeight(480)

        self.image = QLabel(self)
        self.labelTitle = QLabel(self.title)

        self.labelFPS = QLabel(self)
        self.labelFPS.setText(f'{29.99:7.2f}')

        self.buttonMenu = QPushButton(self, text='Menu')
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.comboCamera = QComboBox(self)
        self.comboCamera.setGeometry(200, 150, 200, 30)
        self.comboCamera.addItems(self.available_cameras())
        self.comboCamera.activated[str].connect(self.change_camera_option)
        current_index = self.get_camera_option()
        self.comboCamera.setCurrentIndex(current_index)


    def setStyle(self):
        self.width = self.size().width()
        self.height = self.size().height()
        self.image.resize(self.width, self.height)

        self.setStyleSheet("""
                background-color: black;
                """)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        label_title_style = """
                background-color: rgba(0,0,0,0);
                border: 0px solid black;
                font-size:30px;
                color: grey
                """

        label_FPS = """
                background-color: rgba(0,0,0,0);
                border: 0px solid black;
                font-size: 24px;
                color: green;
                font-weight: bold
                """

        comboStyleSheet = """
            font-size: 15px;
            padding-left: 7px;
            color: white;
            background: black;
            selection-background-color: grey;
            border: 1px solid grey;
            border-radius: 3px;
        """

        self.labelTitle.setStyleSheet(label_title_style)
        self.labelFPS.setStyleSheet(label_FPS)
        self.comboCamera.setStyleSheet(comboStyleSheet)

    def setWindowLayout(self):
        layoutV = QVBoxLayout()

        space = {'top': 40,
                 'bottom': 40,
                 'right': 80,
                 'left': 80}

        self.width = self.size().width()
        self.height = self.size().height()
        self.thread_camera.changeWinSize(self.size())

        #Layout Image
        self.image.resize(self.width, self.height)


        self.labelTitle.setAlignment(Qt.AlignHCenter)
        layoutV.addWidget(self.labelTitle)

        layoutV.addWidget(self.buttonMenu, 0, Qt.AlignLeft | Qt.AlignBottom)

        # FPS position
        x = self.size().width() - self.labelFPS.size().width()
        y = self.size().height() - self.labelFPS.size().height()
        p = QPoint(x - 20, y - 10)
        self.labelFPS.move(p)

        # ComboBox position
        x = self.size().width()//2 - self.comboCamera.size().width()//2
        y = self.size().height() - self.comboCamera.size().height()
        p = QPoint(x - 50, y - 10)
        self.comboCamera.move(p)

        self.setLayout(layoutV)

    def backToMenu(self):
        self.hide()

    def closeEvent(self, event):
        self.thread_camera.stop()
        event.accept()

    @pyqtSlot(QImage)
    def setImage(self, image):
        self.image.setPixmap(QPixmap.fromImage(image))

    @pyqtSlot(float)
    def setFPS(self, fps):
        self.fps = fps
        self.labelFPS.setText(f'{fps:7.2f}')

    def showEvent(self, event):
        self.setStyle()
        #self.setWindowLayout()
        self.startCapture()
        #self.show()

    def startCapture(self):
        self.thread_camera.changePixmap.connect(self.setImage)
        self.thread_camera.changeFPS.connect(self.setFPS)
        self.thread_camera.start()

    def get_camera_option(self):
        if self.options.camera is False:
            return 0
        else:
            cameras_list = self.os_dev_video
            if self.options.camera in cameras_list:
                return cameras_list.index(self.options.camera)+1
            else:
                return 0

    def change_camera_option(self, text):
        if text == 'No camera':
            self.options.camera = False
        else:
            self.options.camera = text

    def available_cameras(self):
        command = 'ls /dev/video*'
        output = subprocess.run(command, shell=True, capture_output=True)
        available_cameras = ['No camera']

        if output.stderr:
            self.os_dev_video = []

        if output.stdout:
            out = output.stdout.strip()
            self.os_dev_video = out.decode('utf-8').split('\n')

        return available_cameras + self.os_dev_video


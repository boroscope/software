# !/usr/bin/env python3

#   Borescope is a system to work with inspection cameras in disaster scenarios
#   Copyright (C) 2021 Borescope Team (https://gitlab.com/boroscope/software)
#
#   This file is part of Borescope.
#
#   Borescope is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Borescope is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with borescope.  If not, see <https://www.gnu.org/licenses/>.

import PyQt5.QtWidgets as pyQt

from borescope.modules.bs_core import Borescope
from borescope.modules import save

from borescope.gui.ui_main import startWindow

from borescope import __name__ as app_name
from borescope import __version__ as app_version

def main(options):

    options.app_name = app_name
    options.app_version = app_version

    core = Borescope(options)

    app = pyQt.QApplication([])
    app_win = startWindow(options, core)
    if core.running_on_arm:
      app_win.showMaximized()
    else:
      app_win.show()
    app.exit(app.exec_())

    if options.save_enabled:
        save.save_all(options, options.file)

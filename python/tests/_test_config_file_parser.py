from unittest import TestCase
from unittest.mock import patch

import os
import configparser
from types import SimpleNamespace

from borescope.modules.config_file_parser import ConfigFileParser

import borescope as bs

class CustomConfigFileParser(ConfigFileParser):
    def __init__(self, file):
        self.options = SimpleNamespace()

class CustomConfigParser(configparser.ConfigParser):
    def read(self, filenames, *args, **kwargs):
        self.read_string('''
        [options]
        camera = false
        i2c_port = 1
        imu = emulated
        battery = true
        pwm = true
        display_title = true
        display_angles = true
        intensity = 100
        screen_height = 320
        screen_width = 480
        channels = 3
        display_horizon = true
        horizon_thickness = 1
        horizon_color = 0-255-0
        ''')

class TestMyClass(TestCase):
    def setUp(self):
        pass

    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages',
        '/home/user/.config/borescope'
        ])
    @patch('os.path.exists', side_effect=[True])
    @patch('configparser.ConfigParser', side_effect=CustomConfigParser)
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_instance_None_as_file_config_home_exist(self, mock_dirname,
                                                     mock_exists,
                                                     mock_configparser):

        self.cfp = ConfigFileParser(None)

        self.assertEqual(self.cfp.options.file, '/home/user/.config/borescope/borescope.conf')
        self.assertEqual(self.cfp.options.config_path, '/home/user/.config/borescope')
        self.assertEqual(self.cfp.options.save_enabled, True)

    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages',
        '/usr/local/lib/python3.9/dist-packages/borescope',
        ])
    @patch('os.path.exists', side_effect=[False, True])
    @patch('configparser.ConfigParser', side_effect=CustomConfigParser)
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_instance_None_as_file_config_home_dont_exist(self, mock_dirname,
                                                     mock_exists,
                                                     mock_configparser):

        self.cfp = ConfigFileParser(None)

        self.assertEqual(self.cfp.options.file,
                '/usr/local/lib/python3.9/dist-packages/borescope/borescope.conf')
        self.assertEqual(self.cfp.options.config_path,
                '/usr/local/lib/python3.9/dist-packages/borescope')
        self.assertEqual(self.cfp.options.save_enabled, False)


    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages',
        '/path'
        ])
    @patch('os.path.exists', side_effect=[True])
    @patch('configparser.ConfigParser', side_effect=CustomConfigParser)
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_instance_file_exists(self,
                                  mock_dirname,
                                  mock_exists,
                                  mock_configparser):

        self.cfp = ConfigFileParser('/path/borescope.conf')

        self.assertEqual(self.cfp.options.file, '/path/borescope.conf')
        self.assertEqual(self.cfp.options.config_path, '/path')
        self.assertEqual(self.cfp.options.save_enabled, True)


    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages',
        '/home/user/.config/borescope'
        ])
    @patch('os.path.exists', side_effect=[False, True])
    @patch('configparser.ConfigParser', side_effect=CustomConfigParser)
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_instance_file_dont_exists_home_config_exist(self,
                                  mock_dirname,
                                  mock_exists,
                                  mock_configparser):

        self.cfp = ConfigFileParser('/path/borescope.conf')

        self.assertEqual(self.cfp.options.file,
                '/home/user/.config/borescope/borescope.conf')
        self.assertEqual(self.cfp.options.config_path,
                '/home/user/.config/borescope')
        self.assertEqual(self.cfp.options.save_enabled, True)

    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages',
        '/usr/local/lib/python3.9/dist-packages/borescope',
        ])
    @patch('os.path.exists', side_effect=[False, False, True])
    @patch('configparser.ConfigParser', side_effect=CustomConfigParser)
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_instance_file_dont_exists_home_config_dont_exist(self,
                                  mock_dirname,
                                  mock_exists,
                                  mock_configparser):

        self.cfp = ConfigFileParser('/path/borescope.conf')

        self.assertEqual(self.cfp.options.file,
                '/usr/local/lib/python3.9/dist-packages/borescope/borescope.conf')
        self.assertEqual(self.cfp.options.config_path,
                '/usr/local/lib/python3.9/dist-packages/borescope')
        self.assertEqual(self.cfp.options.save_enabled, False)


    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages'
        ])
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_add_file_to_priority_list_None_file(self, mock_dirname):
        file = None
        self.cfp = CustomConfigFileParser(file)
        config_files = self.cfp.add_file_to_priority_list(file)

        mocked_config_files = [
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python3.9/dist-packages/borescope/borescope.conf',
                ]

        self.assertEqual(config_files, mocked_config_files)


    @patch('os.path.dirname', side_effect=[
        '/usr/local/lib/python3.9/dist-packages/borescope',
        '/usr/local/lib/python3.9/dist-packages'
        ])
    @patch.dict(os.environ, {'HOME': '/home/user'})
    def test_add_file_to_priority_list_some_file(self, mock_dirname):
        file = '/path/borescope.conf'
        self.cfp = CustomConfigFileParser(file)
        config_files = self.cfp.add_file_to_priority_list(file)

        mocked_config_files = [
                '/path/borescope.conf',
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python3.9/dist-packages/borescope/borescope.conf',
                ]

        self.assertEqual(config_files, mocked_config_files)

    def test_set_option_imu_emulated_text_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_imu('emulated')

        self.assertEqual(self.cfp.options.imu, 'emulated')

    def test_set_option_imu_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_imu('foo')

        self.assertEqual(self.cfp.options.imu, False)

    def test_set_option_imu_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_imu(3)

        self.assertEqual(self.cfp.options.imu, False)

    def test_set_option_imu_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_imu('true')

        self.assertEqual(self.cfp.options.imu, True)

    def test_set_option_imu_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_imu('TRue')

        self.assertEqual(self.cfp.options.imu, True)


    def test_set_option_battery_emulated_text_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_battery('emulated')

        self.assertEqual(self.cfp.options.battery, 'emulated')

    def test_set_option_battery_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_battery('foo')

        self.assertEqual(self.cfp.options.battery, False)

    def test_set_option_battery_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_battery(3)

        self.assertEqual(self.cfp.options.battery, False)

    def test_set_option_battery_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_battery('true')

        self.assertEqual(self.cfp.options.battery, True)

    def test_set_option_battery_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_battery('TRue')

        self.assertEqual(self.cfp.options.battery, True)


    def test_set_option_pwm_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_pwm('foo')

        self.assertEqual(self.cfp.options.pwm, False)

    def test_set_option_pwm_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_pwm(3)

        self.assertEqual(self.cfp.options.pwm, False)

    def test_set_option_pwm_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_pwm('true')

        self.assertEqual(self.cfp.options.pwm, True)

    def test_set_option_pwm_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_pwm('TRue')

        self.assertEqual(self.cfp.options.pwm, True)


    def test_set_option_camera_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_camera('foo')

        self.assertEqual(self.cfp.options.camerapath, 'foo')

    def test_set_option_camera_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_camera(0)

        self.assertEqual(self.cfp.options.camerapath, '/dev/video0')

    def test_set_option_camera_false_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_camera('false')

        self.assertEqual(self.cfp.options.camerapath, False)

    def test_set_option_camera_false_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_camera('FAlse')

        self.assertEqual(self.cfp.options.camerapath, False)


    def test_set_option_i2c_port_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_i2c_port('foo')

        self.assertEqual(self.cfp.options.i2c_port, 1)

    def test_set_option_i2c_port_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_i2c_port(1)

        self.assertEqual(self.cfp.options.i2c_port, 1)

    def test_set_option_i2c_port_text_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_i2c_port('1')

        self.assertEqual(self.cfp.options.i2c_port, 1)

    def test_set_option_display_title_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_title('foo')

        self.assertEqual(self.cfp.options.display_title, False)

    def test_set_option_display_title_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_title(3)

        self.assertEqual(self.cfp.options.display_title, False)

    def test_set_option_display_title_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_title('true')

        self.assertEqual(self.cfp.options.display_title, True)

    def test_set_option_display_title_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_title('TRue')

        self.assertEqual(self.cfp.options.display_title, True)


    def test_set_option_display_angles_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_angles('foo')

        self.assertEqual(self.cfp.options.display_angles, False)

    def test_set_option_display_angles_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_angles(3)

        self.assertEqual(self.cfp.options.display_angles, False)

    def test_set_option_display_angles_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_angles('true')

        self.assertEqual(self.cfp.options.display_angles, True)

    def test_set_option_display_angles_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_angles('TRue')

        self.assertEqual(self.cfp.options.display_angles, True)


    def test_set_option_intensity_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity('foo')

        self.assertEqual(self.cfp.options.intensity, 0)

    def test_set_option_intensity_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity(1)

        self.assertEqual(self.cfp.options.intensity, 1)

    def test_set_option_intensity_text_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity('1')

        self.assertEqual(self.cfp.options.intensity, 1)

    def test_set_option_intensity_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity(-1)

        self.assertEqual(self.cfp.options.intensity, 0)

    def test_set_option_intensity_text_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity('-1')

        self.assertEqual(self.cfp.options.intensity, 0)

    def test_set_option_intensity_int_wrong_up(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity(101)

        self.assertEqual(self.cfp.options.intensity, 100)

    def test_set_option_intensity_text_int_wrong_up(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_intensity('101')

        self.assertEqual(self.cfp.options.intensity, 100)


    def test_set_option_screen_height_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_height('foo')

        self.assertEqual(self.cfp.options.screen_height, 320)

    def test_set_option_screen_height_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_height(320)

        self.assertEqual(self.cfp.options.screen_height, 320)

    def test_set_option_screen_height_text_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_height('320')

        self.assertEqual(self.cfp.options.screen_height, 320)

    def test_set_option_screen_height_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_height(-1)

        self.assertEqual(self.cfp.options.screen_height, 320)

    def test_set_option_screen_height_text_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_height('-1')

        self.assertEqual(self.cfp.options.screen_height, 320)


    def test_set_option_screen_width_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_width('foo')

        self.assertEqual(self.cfp.options.screen_width, 480)

    def test_set_option_screen_width_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_width(480)

        self.assertEqual(self.cfp.options.screen_width, 480)

    def test_set_option_screen_width_text_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_width('480')

        self.assertEqual(self.cfp.options.screen_width, 480)

    def test_set_option_screen_width_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_width(-1)

        self.assertEqual(self.cfp.options.screen_width, 480)

    def test_set_option_screen_width_text_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_screen_width('-1')

        self.assertEqual(self.cfp.options.screen_width, 480)


    def test_set_option_channels_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_channels('foo')

        self.assertEqual(self.cfp.options.channels, 3)

    def test_set_option_channels_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_channels(3)

        self.assertEqual(self.cfp.options.channels, 3)

    def test_set_option_channels_text_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_channels('3')

        self.assertEqual(self.cfp.options.channels, 3)

    def test_set_option_channels_int_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_channels(2)

        self.assertEqual(self.cfp.options.channels, 3)

    def test_set_option_channels_text_int_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_channels('2')

        self.assertEqual(self.cfp.options.channels, 3)

    def test_set_option_horizon_thickness_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness('foo')

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness(1)

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_float(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness(1.9)

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_text_int_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness('1')

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness(-1)

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_text_int_wrong_low(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness('-1')

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_int_wrong_up(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness(6)

        self.assertEqual(self.cfp.options.horizon_thickness, 5)

    def test_set_option_horizon_thickness_text_int_wrong_up(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness('6')

        self.assertEqual(self.cfp.options.horizon_thickness, 5)


    def test_set_option_horizon_thickness_int_wrong_down(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness(0)

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_horizon_thickness_text_int_wrong_down(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_thickness('0')

        self.assertEqual(self.cfp.options.horizon_thickness, 1)

    def test_set_option_display_horizon_wrong_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_horizon('foo')

        self.assertEqual(self.cfp.options.display_horizon, False)

    def test_set_option_display_horizon_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_horizon(3)

        self.assertEqual(self.cfp.options.display_horizon, False)

    def test_set_option_display_horizon_true_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_horizon('true')

        self.assertEqual(self.cfp.options.display_horizon, True)

    def test_set_option_display_horizon_true_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_display_horizon('TRue')

        self.assertEqual(self.cfp.options.display_horizon, True)

    def test_set_option_horizon_color_text_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('100-100-100')

        self.assertEqual(self.cfp.options.horizon_color, [100,100,100])

    def test_set_option_horizon_color_text_missing_hyphens1_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('100 100 100')

        self.assertEqual(self.cfp.options.horizon_color, [100,100,100])

    def test_set_option_horizon_color_text_missing_hyphens2_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('100-100 100')

        self.assertEqual(self.cfp.options.horizon_color, [100,100,100])

    def test_set_option_horizon_color_text_missing_hyphens3_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('100 100-100')

        self.assertEqual(self.cfp.options.horizon_color, [100,100,100])

    def test_set_option_horizon_color_float_text_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('100-100.9-100')

        self.assertEqual(self.cfp.options.horizon_color, [100,100,100])

    def test_set_option_horizon_color_text_wrong(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('foo')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_mixed_text_wrong1(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-255-foo')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_mixed_text_wrong2(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-foo-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_mixed_text_wrong3(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('foo-255-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_int(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color(3)

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_color_list_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color([255,0,0])

        self.assertEqual(self.cfp.options.horizon_color, [255,0,0])

    def test_set_option_horizon_color_color_list_float_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color([200.9,0,0])

        self.assertEqual(self.cfp.options.horizon_color, [200,0,0])

    def test_set_option_horizon_color_color_tuple_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color((0,0,255))

        self.assertEqual(self.cfp.options.horizon_color, [0,0,255])

    def test_set_option_horizon_color_color_tuple_float_ok(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color((0,0,200.9))

        self.assertEqual(self.cfp.options.horizon_color, [0,0,200])

    def test_set_option_horizon_color_many_values(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color([0,255,0,0,0])

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_many_values_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-255-0-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_few_values(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color([0,255])

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])


    def test_set_option_horizon_color_few_values_text(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-255')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_extra_hyphen1(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('-0-255-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_extra_hyphen2(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-255-0-')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_extra_hyphen3(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('-0-255-0-')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_extra_hyphen4(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('-0-255--0-')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_mixed_hyphen_space(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('255- 255-255')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_red_out_of_range(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('256-255-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_green_out_of_range(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-256-0')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_set_option_horizon_color_blue_out_of_range(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.set_option_horizon_color('0-255-256')

        self.assertEqual(self.cfp.options.horizon_color, [0,255,0])

    def test_get_options(self):
        file = None
        self.cfp = CustomConfigFileParser(file)

        self.cfp.options.test_option1 = 'test1'
        self.cfp.options.test_option2 = 'test2'

        opt = self.cfp.get_options()

        tester_opt = SimpleNamespace(test_option1='test1', test_option2='test2')

        self.assertEqual(opt, tester_opt)


    @patch('os.path.exists', side_effect=[True])
    def test_search_file_path_exist(self, mock_exists):
        file = '/path/borescope.conf'
        self.cfp = CustomConfigFileParser(file)

        files = [
                '/path/borescope.conf',
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python/dist-packages/borescope/borescope.conf',
                ]

        file, save_enabled = self.cfp.search_file(files)

        self.assertEqual(save_enabled, True)
        self.assertEqual(file, '/path/borescope.conf')

    @patch('os.path.exists', side_effect=[False, True])
    def test_search_file_path_dont_exist_home_exist(self, mock_exists):
        file = '/path/borescope.conf'
        self.cfp = CustomConfigFileParser(file)

        files = [
                '/path/borescope.conf',
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python/dist-packages/borescope/borescope.conf',
                ]

        file, save_enabled = self.cfp.search_file(files)

        self.assertEqual(save_enabled, True)
        self.assertEqual(file, '/home/user/.config/borescope/borescope.conf')

    @patch('os.path.exists', side_effect=[False, False, True])
    def test_search_file_path_dont_exist_home_dont_exist(self, mock_exists):
        file = '/path/borescope.conf'
        self.cfp = CustomConfigFileParser(file)

        files = [
                '/path/borescope.conf',
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python/dist-packages/borescope/borescope.conf',
                ]

        file, save_enabled = self.cfp.search_file(files)

        self.assertEqual(save_enabled, False)
        self.assertEqual(file, '/usr/local/lib/python/dist-packages/borescope/borescope.conf')


    @patch('os.path.exists', side_effect=[True])
    def test_search_file_None_as_file_home_exist(self, mock_exists):
        file = None
        self.cfp = CustomConfigFileParser(file)

        files = [
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python/dist-packages/borescope/borescope.conf',
                ]

        file, save_enabled = self.cfp.search_file(files)

        self.assertEqual(save_enabled, True)
        self.assertEqual(file, '/home/user/.config/borescope/borescope.conf')

    @patch('os.path.exists', side_effect=[False, True])
    def test_search_file_None_as_file_home_dont_exist(self, mock_exists):
        file = '/path/borescope.conf'
        self.cfp = CustomConfigFileParser(file)

        files = [
                '/home/user/.config/borescope/borescope.conf',
                '/usr/local/lib/python/dist-packages/borescope/borescope.conf',
                ]

        file, save_enabled = self.cfp.search_file(files)

        self.assertEqual(save_enabled, False)
        self.assertEqual(file, '/usr/local/lib/python/dist-packages/borescope/borescope.conf')

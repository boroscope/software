from unittest import TestCase
from mock import MagicMock, PropertyMock, patch
from borescope.modules.driver_servo import servo


class TestServo(TestCase):

    CasesTrue = {
        "int": 100,
        "channel": 1,
        "String": "hola mundo",
        "Array": [1, 2, 3, 4],
        "True": True,
    }
    CasesFalse = {
        "False": False,
        "None": None
    }

    @classmethod
    def setUpClass(cls):
        cls.set_driver_mock = MagicMock(return_value=True)
        cls.sv = servo(driver=cls.set_driver_mock)

    def test_set_pwm_device_True(self):
        for x in self.CasesTrue:
            self.sv.set_pwm_device(self.CasesTrue[x])
            self.assertEqual(self.sv.pwm, self.CasesTrue[x])
            self.assertEqual(self.sv.available, True)

    def test_set_pwm_device_False(self):
        self.sv.set_pwm_device(self.CasesFalse["None"])
        self.assertEqual(self.sv.available, False)

    def test_set_angle_True(self):
        self.sv.set_pwm_device(self.set_driver_mock)
        duty = self.sv.set_angle(self.CasesTrue["channel"], self.CasesTrue["int"])
        self.assertNotEqual(None, duty)

    def test_set_angle_False(self):
        duty = self.sv.set_angle(self.CasesTrue["channel"], self.CasesTrue["int"])
        self.assertEqual(None, duty)


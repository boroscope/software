from mock import MagicMock, PropertyMock
from unittest import TestCase
from unittest.mock import patch
# from borescope.modules.bus_i2c import BusI2C
from borescope.modules.device_pca9685 import Pca9685
import math

class CustomPca9685(Pca9685):
    def __init__(self, i2c_port=1):
        self.i2c = MagicMock()
        self._output_enable_pin = 7
        self._pwm_frequency = 50
        self.model = 'PCA9685'
        pass


class TestPCA9685(TestCase):
    methods = {
        'read_byte_data': {
            'address': 0x40,
            'reg': 0x00,
            'data': 0x02,
        },
        # PCA9685 base address
        0x40: {
            0x00: 0, # PCA9685 mode1 address
            0x09: 0, # BASE ADDRESS HIGH
            0xFE: 0, # PCA9685 preescaler address
            # LED_ON_REGISTERS
            0x06: 0, # LED0_ON for LED0
            0x0A: 0, # LED1_ON for LED1
            0x0E: 0, # LED2_ON for LED2
            0x12: 0, # LED3_ON for LED3
            0x16: 0, # LED4_ON for LED4
            0x1A: 0, # LED5_ON for LED5
            0x1E: 0, # LED6_ON for LED6
            0x22: 0, # LED7_ON for LED7
            0x26: 0, # LED8_ON for LED8
            0x2A: 0, # LED9_ON for LED9
            0x2E: 0, # LED10_ON for LED10
            0x32: 0, # LED11_ON for LED11
            0x36: 0, # LED12_ON for LED12
            0x3A: 0, # LED13_ON for LED13
            0x3E: 0, # LED14_ON for LED14
            0x42: 0, # LED15_ON for LED15
            # LED_OFF_REGISTERS
            0x08: 0, # LED0_OFF for LED0
            0x0C: 0, # LED1_OFF for LED1
            0x10: 0, # LED2_OFF for LED2
            0x14: 0, # LED3_OFF for LED3
            0x18: 0, # LED4_OFF for LED4
            0x1C: 0, # LED5_OFF for LED5
            0x20: 0, # LED6_OFF for LED6
            0x24: 0, # LED7_OFF for LED7
            0x28: 0, # LED8_OFF for LED8
            0x2C: 0, # LED9_OFF for LED9
            0x30: 0, # LED10_OFF for LED10
            0x34: 0, # LED11_OFF for LED11
            0x38: 0, # LED12_OFF for LED12
            0x3C: 0, # LED13_OFF for LED13
            0x40: 0, # LED14_OFF for LED14
            0x44: 0, # LED15_OFF for LED15
        },
        'write_byte_data': {
            'address': 0x01,
            'reg': 0x01,
            'data': 0x01,
            'data_after': 0x0,
        },
    }

    @classmethod
    def setUpClass(cls):
        cls.custom_pca9685_class = CustomPca9685(1)

    def setUp(self):
        self.bus_i2c_mock = MagicMock(return_value=0)
        self.bus_read_mock = MagicMock(return_value=0)
        self.custom_pca9685_class.i2c.read_byte.side_effect = self.set_smbus_read_byte_data_test
        self.custom_pca9685_class.i2c.write_byte.side_effect = self.set_smbus_write_byte_data_test

    def set_pca9685_init_test(self, port=0):
        mock = self.bus_i2c_mock.init_test
        mock.return_value = 1
        return mock.return_value

    def set_smbus_read_byte_data_test(self, address=0x0, reg=0x0):
        mock = self.bus_read_mock.read_byte_data_test
        mock.return_value = self.methods.get('read_byte_data').get('data')
        return mock.return_value

    def set_smbus_write_byte_data_test(self, address=0x0, reg=0x0, data=0x0):
        self.methods[address][reg] = data

    def test_pca9685_init(self):
        self.assertEqual(self.custom_pca9685_class.get_pwm_frequency(),
            50, msg='Failed to initialize')
        self.assertEqual(self.custom_pca9685_class._output_enable_pin,
            7, msg='Failed to initialize output enable pin')
        self.assertEqual(self.custom_pca9685_class.model,
            "PCA9685", msg='Failed to initialize model name attribute')

    def test_set_frequency(self):
        self.custom_pca9685_class.set_frequency(100)
        self.assertEqual(self.custom_pca9685_class.get_pwm_frequency(),
            100, msg='Failed to initialize pwm frequency at 100')

    def test_pca9685_set_duty(self):

        for channel in range(0,15):
            duty = 20
            self.custom_pca9685_class.set_duty(channel, duty)
            value_on = 0
            value_off = int(duty * 4095 / 100)
            value_off = int( ((900-180)/(100)) * duty + 180)

            self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
                value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
            self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
                value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_bad_duty_overflow(self):
        channel = 0
        duty = 110.0
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int( ((900-180)/(100)) * 100 + 180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_bad_duty_underflow(self):
        channel = 0
        duty = -0.1
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int(180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_tuple(self):
        channel = 0
        duty = tuple((50,20))
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int(50 * 4095 / 100)
        value_off = int( ((900-180)/(100)) * 50 + 180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_array(self):
        channel = 0
        duty = [50]
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int( ((900-180)/(100)) * 50 + 180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_tuple_overflow(self):
        channel = 0
        duty = tuple((500,20))
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int( ((900-180)/(100)) * 100 + 180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_array_overflow(self):
        channel = 0
        duty = [500]
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = int( ((900-180)/(100)) * 100 + 180)

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_tuple_underflow(self):
        channel = 0
        duty = (-10)
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = 180

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_duty_array_underflow(self):
        channel = 0
        duty = [-10]
        self.custom_pca9685_class.set_duty(channel, duty)
        value_on = 0
        value_off = 180

        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
            value_on & 0xFF, msg=f'Failed to set duty on channel {channel}')
        self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
            value_off >> 8, msg=f'Failed to set duty on channel {channel}')

    def test_pca9685_set_channel_on_off(self):

        for channel in range(0,15):
            value_on = 10
            value_off = 100
            self.custom_pca9685_class.set_channel_on_off(channel, value_on, value_off)
            self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_on + 4 * channel),
                value_on & 0xFF, msg=f'Failed to set channel {channel} on')
            self.assertEqual(self.methods.get(0x40).get(CustomPca9685.led0_off + 4 * channel+1),
                value_off >> 8, msg=f'Failed to set channel {channel} off')


    def test_pca9685_freq(self):
        freq = 20
        self.custom_pca9685_class.set_freq(freq)

        prescale_value = Pca9685.osc_clk
        prescale_value /= 4096.0       # 12-bit
        prescale_value /= freq
        prescale_value -= 1.0
        prescale = math.floor(prescale_value + 0.5)
        oldmode = self.methods.get(0x40).get(0x00)
        if prescale < Pca9685.prescale_min:
            prescale = Pca9685.prescale_min
        if prescale > Pca9685.prescale_max:
            prescale = Pca9685.prescale_max

        self.assertEqual(self.methods.get(0x40).get(0xFE),
            int(math.floor(prescale)), msg='Failed to set prescale at set_frequency')

        self.assertEqual(self.methods.get(0x40).get(0x00),
            oldmode | 0x80, msg='Failed to set newmode at set_frequency')


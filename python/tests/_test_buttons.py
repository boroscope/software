from mock import MagicMock, PropertyMock, patch
from unittest import TestCase

mock_rpi = MagicMock()
patch.dict("sys.modules", RPi=mock_rpi).start()
from borescope.modules.buttons_class import Buttons

class TestButtons(TestCase):
    def setUp(self):
        self.buttons = Buttons(pin_row=[29, 31], pin_column=[33, 35, 37])

    #test to see if RIGHT button was pressed
    def test_button_right(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            1, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")

    #test to see if DOWN button was pressed
    def test_button_down(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            1, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "DOWN")

    #test to see if LEFT button was pressed
    def test_button_left(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            0, 1, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "LEFT")

    #test to see if HOME button was pressed
    def test_button_home(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            1, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "HOME")

    #test to see if UP button was pressed
    def test_button_up(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            1, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "UP")

    #test to see if BACK button was pressed
    def test_button_back(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            0, 1, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "BACK")

    #test case if no button is pressed
    def test_button_exit(self):
        self.buttons = Buttons(pin_row=[29, 31], pin_column=[33, 35, 37])
        mock_rpi.GPIO.input.side_effect = [
            0, 0,
            1, 1, 1
            ]
        button = self.buttons.get_key()
        self.assertTrue(mock_rpi.GPIO.cleanup.called)
        self.assertIsNone(button)

    #tests how many times the input function is called, has to be 5 times
    def test_button_called(self):
        mock_rpi.GPIO.reset_mock()
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            1, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(mock_rpi.GPIO.input.call_count, 5)

    #test case when BACK-UP are pressed at the same time
    def test_button_back_up(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            0, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "UP")

    #test case when UP-HOME are pressed at the same time
    def test_button_up_home(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            1, 0, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "HOME")
        
    #test case when BACK-HOME are pressed at the same time
    def test_button_back_home(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 0,
            0, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "HOME")
    
    #test case when LEFT-DOWN are pressed at the same time
    def test_button_left_down(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            0, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "DOWN")

    #test case when DOWN-RIGHT are pressed at the same time
    def test_button_down_right(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            1, 0, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")
    
    #test case when LEFT-RIGHT are pressed at the same time
    def test_button_left_right(self):
        mock_rpi.GPIO.input.side_effect = [
            0, 1,
            0, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")

    #test case when BACK-LEFT are pressed at the same time
    def test_button_back_left(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            0, 1, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "LEFT")

    #test case when BACK-DOWN are pressed at the same time
    def test_button_back_down(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            0, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "DOWN")

    #test case when BACK-RIGHT are pressed at the same time
    def test_button_back_right(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            0, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")

    #test case when UP-LEFT are pressed at the same time
    #this test is commented because the test fails but works when implemented in hardware
    #def test_button_up_left(self):
    #   mock_rpi.GPIO.input.side_effect = [
    #       1, 1,
    #       0, 0, 1
    #       ]
    #   button = self.buttons.get_key()
    #   self.assertEqual(button, "LEFT")

    #test case when UP-DOWN are pressed at the same time
    def test_button_up_down(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            1, 0, 1
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "DOWN")

    #test case when UP-RIGHT are pressed at the same time
    def test_button_up_right(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            1, 0, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")

    #test case when HOME-LEFT are pressed at the same time
    #this test is commented because the test fails but works when implemented in hardware
    #def test_button_home_left(self):
    #   mock_rpi.GPIO.input.side_effect = [
    #       1, 1,
    #       0, 1, 0
    #       ]
    #   button = self.buttons.get_key()
    #   self.assertEqual(button, "LEFT")

    #test case when HOME-DOWN are pressed at the same time
    #this test is commented because the test fails but works when implemented in hardware
    #def test_button_home_down(self):
    #   mock_rpi.GPIO.input.side_effect = [
    #       1, 1,
    #       1, 0, 0
    #       ]
    #   button = self.buttons.get_key()
    #   self.assertEqual(button, "DOWN")

    #test case when HOME-RIGHT are pressed at the same time
    def test_button_home_right(self):
        mock_rpi.GPIO.input.side_effect = [
            1, 1,
            1, 1, 0
            ]
        button = self.buttons.get_key()
        self.assertEqual(button, "RIGHT")
from unittest import TestCase

from borescope.modules.dummy_class_to_test import dummy_class

class TestMyClass(TestCase):
    def setUp(self):
        self.dc = dummy_class()

    def test_set_option_int_ok(self):
        int_ok = 1
        self.dc.set_option(int_ok)

        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_str_ok(self):
        int_no_ok = '1'
        self.dc.set_option(int_no_ok)

        int_ok = 1
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_float_low_ok(self):
        int_no_ok = 1.1
        self.dc.set_option(int_no_ok)

        int_ok = 1
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_float_high_ok(self):
        int_no_ok = 1.9
        self.dc.set_option(int_no_ok)

        int_ok = 1
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_str_float_low_ok(self):
        int_no_ok = '1.1'
        self.dc.set_option(int_no_ok)

        int_ok = 1
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_str_float_high_ok(self):
        int_no_ok = '1.9'
        self.dc.set_option(int_no_ok)

        int_ok = 1
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_list_return_0(self):
        int_no_ok = [1,3.14,'foo']
        self.dc.set_option(int_no_ok)

        int_ok = 0
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_tuple_return_0(self):
        int_no_ok = (1,3.14,'foo')
        self.dc.set_option(int_no_ok)

        int_ok = 0
        self.assertEqual(self.dc.option, int_ok)

    def test_set_option_dict_return_0(self):
        int_no_ok = {'a':1, 'b':2}
        self.dc.set_option(int_no_ok)

        int_ok = 0
        self.assertEqual(self.dc.option, int_ok)

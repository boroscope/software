from unittest import TestCase

from borescope.modules.dummy_class_to_test import dummy_class

class TestMyClass(TestCase):
    def setUp(self):
        self.dc = dummy_class()

    def test_set_option(self):
        self.dc.set_option(True)
        self.assertEqual(self.dc.option, True)

    def test_get_option(self):
        self.dc.option = True
        value = self.dc.get_option()
        self.assertEqual(value, True)

    def test_some_text_process(self):
        splited = self.dc.some_text_process('split this sentence')
        self.assertEqual(splited, ['split','this','sentence'])

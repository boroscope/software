from unittest import TestCase

from borescope.modules.config_stdin_parser import ConfigStdinParser
from types import SimpleNamespace

class CustomConfigStdinParser(ConfigStdinParser):
    def __init__(self):
        self.raw_args = None
        self.parsed_args = None

        # self.setup() was remove


class TestMyClass(TestCase):
    def setUp(self):
        self.csp = CustomConfigStdinParser()

    def test_setup_without_args(self):
        args = []
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_no_x(self):
        args = ['--no-X']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':True,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_no_imu(self):
        args = ['--no-imu']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':True,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_emu_imu(self):
        args = ['--emu-imu']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':True,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_camera(self):
        args = ['--camera', '/dev/video0']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':'/dev/video0',
                'no_camera':False,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_no_camera(self):
        args = ['--no-camera']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':True,
                'config_file':None,
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')

    def test_setup_config_file(self):
        args = ['--config-file', '/path/borescope.conf']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':'/path/borescope.conf',
                'generate_config_file':False
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')


    def test_setup_generate_config_file(self):
        args = ['--generate-config-file']
        self.csp.raw_args = args
        self.csp.setup()

        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':True
                }

        parsed_args = self.csp.parsed_args

        for key, value in mocked_args.items():
            self.assertEqual(parsed_args.__dict__.get(key),
                             value,
                             msg=f'wrong {key} got {value}')


    def test_get_args(self):
        mocked_args = {
                'no_x':False,
                'no_imu':False,
                'emu_imu':False,
                'video_dev':None,
                'no_camera':False,
                'config_file':None,
                'generate_config_file':True
                }

        self.csp.parsed_args = mocked_args

        parsed_args = self.csp.get_args()

        self.assertEqual(parsed_args, mocked_args)

from mock import MagicMock, PropertyMock
from unittest import TestCase
from unittest.mock import patch
from borescope.modules.interface_bus_i2c import BusI2C

class TestBusI2C(TestCase):
    methods = {
        'read_byte_data': {
            'address': 0x01,
            'reg': 0x01,
            'data': 0x02,
        },
        'write_byte_data': {
            'address': 0x01,
            'reg': 0x01,
            'data': 0x01,
            'data_after': 0x0,
        },
        'read_word_data': {
            'address': 0x01,
            'reg': 0x01,
            'data': 0x01,
            'word_data': (0x01 << 8) + 0x01,
        },
        'read_word_2c_data': {
            'address': 0x01,
            'reg': [0x00, 0x01, 0x02, 0x03],
            'data': [0x00, 0xFF, 0x20, 0xFE, 0xAF, None],
            'word_2c_data': [255, -224, 8446, -337, None],
        },
    }

    @classmethod
    def setUpClass(cls):
        cls.bus_mock = MagicMock()
        cls.bus_i2c = BusI2C(1)

    def setUp(self):
        self.bus_i2c_mock = MagicMock(return_value=0)
        self.bus_read_mock = MagicMock(return_value=0)

    def set_SMBus_init_test(self, port=0):
        mock = self.bus_i2c_mock.init_test
        mock.return_value = 1
        return mock.return_value

    def set_SMBus_read_byte_data_test(self, address=0x0, reg=0x0):
        mock = self.bus_read_mock.read_byte_data_test
        mock.return_value = self.methods.get('read_byte_data').get('data')
        return mock.return_value

    def set_SMBus_read_word_data_test(self, address=0x0, reg=0x0):
        mock = self.bus_read_mock.read_byte_data_test
        mock.return_value = self.methods.get('read_word_data').get('data')
        return mock.return_value

    def set_SMBus_read_word_2c_data_test(self, address=0x0, reg=0x0):
        mock = self.bus_read_mock.read_byte_data_test
        mock.return_value = self.methods.get('read_word_2c_data').get('data')[reg]
        return mock.return_value

    def set_SMBus_write_byte_data_test(self, address=0x0, reg=0x0, data=0x0):
        self.methods['write_byte_data']['data_after'] = data

    def set_SMBus_write_port_not_open_test(self, address=0x0, reg=0x0, data=0x0):
        raise AttributeError

    def set_SMBus_write_port_not_able_to_write_test(self, address=0x0, reg=0x0, data=0x0):
        raise OSError

    def set_SMBus_read_port_not_open_test(self, address=0x0, reg=0x0):
        raise AttributeError

    def set_SMBus_read_port_not_able_to_read_test(self, address=0x0, reg=0x0):
        raise OSError
        

    @patch('borescope.modules.interface_bus_i2c.smbus2.SMBus')
    def test_bus_init_good_port(self, mock_smbus2):
        mock_smbus2.side_effect = self.set_SMBus_init_test
        port = 1
        self.assertEqual(self.bus_i2c.bus_init(port),
            1, msg='Failed to initialize smbus on port {} '.format(port))
        self.assertEqual(self.bus_i2c.open,
            True, msg='open is not set to True after bus initialization{} ')

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_read_byte_data(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.read_byte_data.side_effect = self.set_SMBus_read_byte_data_test
        self.assertEqual(self.bus_i2c.read_byte(
            self.methods.get('read_byte_data').get('address'),
            self.methods.get('read_byte_data').get('reg')),
            self.methods.get('read_byte_data').get('data'),
            msg='Value out of range')

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_write_byte_data(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.write_byte_data.side_effect = self.set_SMBus_write_byte_data_test
        self.assertEqual(
        self.bus_i2c.write_byte(
            self.methods.get('write_byte_data').get('address'),
            self.methods.get('write_byte_data').get('reg'),
            self.methods.get('write_byte_data').get('data')),
        True, msg='write_byte error not True returned code')
        self.assertEqual(
            self.methods.get('write_byte_data').get('data'),
            self.methods.get('write_byte_data').get('data_after'),
            msg='Value out of range')

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_write_byte_data_port_not_open(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.write_byte_data.side_effect = self.set_SMBus_write_port_not_open_test
        self.assertEqual(
        self.bus_i2c.write_byte(
            self.methods.get('write_byte_data').get('address'),
            self.methods.get('write_byte_data').get('reg'),
            self.methods.get('write_byte_data').get('data')),
            None, msg="write_byte returned non None error")

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_write_byte_data_port_not_able_to_write(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.write_byte_data.side_effect = self.set_SMBus_write_port_not_able_to_write_test
        self.assertEqual(
        self.bus_i2c.write_byte(
            self.methods.get('write_byte_data').get('address'),
            self.methods.get('write_byte_data').get('reg'),
            self.methods.get('write_byte_data').get('data')),
            None, msg="write_byte returned non None error")

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_read_byte_data_port_not_open(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.read_byte_data.side_effect = self.set_SMBus_read_port_not_open_test
        self.assertEqual(
        self.bus_i2c.read_byte(
            self.methods.get('write_byte_data').get('address'),
            self.methods.get('write_byte_data').get('reg')),
            None, msg="read_byte returned non None error")

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_read_byte_data_port_not_able_to_read(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.read_byte_data.side_effect = self.set_SMBus_read_port_not_able_to_read_test
        self.assertEqual(
        self.bus_i2c.read_byte(
            self.methods.get('write_byte_data').get('address'),
            self.methods.get('write_byte_data').get('reg')),
            None, msg="read_byte returned non None error")

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_read_word_data(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.read_byte_data.side_effect = self.set_SMBus_read_word_data_test
        self.assertEqual(self.bus_i2c.read_word(
            self.methods.get('read_word_data').get('address'),
            self.methods.get('read_word_data').get('reg')),
            self.methods.get('read_word_data').get('word_data'),
            msg='Value out of range in read_word method')

    @patch('borescope.modules.interface_bus_i2c.smbus2')
    def test_read_word_2c_data(self, mock_bus):
        self.bus_i2c = BusI2C(1)
        self.bus_i2c.bus.read_byte_data.side_effect = self.set_SMBus_read_word_2c_data_test

        for i,reg in enumerate(self.methods.get('read_word_2c_data').get('reg')):
            self.assertEqual(self.bus_i2c.read_word_2c(
                self.methods.get('read_word_2c_data').get('address'),
                reg),
                self.methods.get('read_word_2c_data').get('word_2c_data')[i],
                msg='Value out of range in read_word_2c method at test case {}'.format(i))

from mock import MagicMock, PropertyMock, patch
from unittest import TestCase

from borescope.modules.device_ina219 import Ina219

class TestIna(TestCase):
    methods = {
        "set_config": {
            "mock_i2c": [True, None],
            "expected_values": [[1, 159], None],
            "tolerance": None,
        },
        "set_calibration": {
            "mock_i2c": [True, None],
            "expected_values": [[52, 109], None],
            "tolerance": None,
        },
        "vbus": {
            "mock_i2c": [[0, 0], [0xff, 0x40]],
            "expected_values": [0, 32],
            "tolerance": 0.05,
        },
        "vshunt": {
            "mock_i2c": [[0, 0], [0x7f, 0xff]],
            "expected_values": [0, 0.32],
            "tolerance": 0.05,
        },
        "current": {
            "mock_i2c": [[0, 0], [0x7f, 0xff]],
            "expected_values": [0, 1],
            "tolerance": 0.05,
            "active": 1
        },
        "power": {
            "mock_i2c": [[0, 0], [0xff, 0xff]],
            "expected_values": [0, 40],
            "tolerance": 0.05,
        },
        "bin2c_b16": {
            "mock": [0x7D00,  0x8300, 0x0FA0, 0xF060],
            "expected_values": [32000, -32000, 4000, -4000],
            "tolerance": None,
        }
    }

    @classmethod
    def setUpClass(cls):
        cls.ina219 = Ina219(i2c_port=None)

        #cls.ina219.discover()
        cls.ina219.config_val = cls.ina219.set_config()

        #cls.ina219.setup()
        cls.ina219.calibration_val = cls.ina219.set_calibration()

        cls.ina219.i2c.bus = MagicMock()
        #assert (cls.ina.i2c.write_i2c_block_data.call_count == 2)

    def setUp(self):
        self.i2c_read_mock = MagicMock(return_value=0)
        self.i2c_write_mock = MagicMock(return_value=0)

        self.set_i2c_read_block_data()
        self.set_i2c_write_block_data()

    def set_i2c_read_block_data(self, mock=None, return_value=0):
        if not mock:
            mock = self.i2c_read_mock
        mock.return_value = return_value
        self.ina219.i2c.read_block = mock

    def set_i2c_write_block_data(self, mock=None, return_value=0):
        if not mock:
            mock = self.i2c_write_mock
        mock.return_value = return_value
        #mock.side_effect = lambda x, y, z: z
        self.ina219.i2c.write_block = mock

    def param_get_test_i2c(self, parameter):
        method = self.methods.get(parameter)
        expected = method.get('expected_values')
        mocked = method.get('mock_i2c')
        tolerance = method.get('tolerance')

        for expected_value, mocked_value in zip(expected, mocked):
            self.set_i2c_read_block_data(return_value=mocked_value)
            self.set_i2c_write_block_data(return_value=mocked_value)

            method_return = getattr(self.ina219, parameter)()

            #print("\nMocked: ", mocked_value,
            #      "\nReturn: ", method_return,
            #      "\nExpected: ", expected_value,
            #      "\nTolerance: ", tolerance)

            if tolerance is not None:
                delta = (method_return * tolerance)
                self.assertAlmostEqual(
                    method_return,
                    expected_value,
                    delta=delta,
                    msg='Parameter {} out of range'.format(parameter)
                    )
            else:
                self.assertEqual(
                    method_return,
                    expected_value,
                    msg='Parameter {} out of range'.format(parameter)
                    )

    #def test_ina_set_calibration(self):
    #    self.param_get_test_i2c('set_calibration')

    #def test_ina_set_config(self):
    #    self.param_get_test_i2c('set_config')

    def test_ina_current(self):
        self.param_get_test_i2c('current')

    def test_ina_vbus(self):
        self.param_get_test_i2c('vbus')

    def test_ina_vshunt(self):
        self.param_get_test_i2c('vshunt')

    def test_ina_power(self):
        self.param_get_test_i2c('power')

    def test_bin2c_b16(self):
        parameter = "bin2c_b16"
        method = self.methods.get(parameter)
        expected = method.get('expected_values')
        mocked = method.get('mock')
        tolerance = method.get('tolerance')

        for expected_value, mocked_value in zip(expected, mocked):
            method_return = getattr(self.ina219, parameter)(mocked_value)

            #print("\nMocked: ", mocked_value,
            #      "\nReturn: ", method_return,
            #      "\nExpected: ", expected_value,
            #      "\nTolerance: ", tolerance)

            self.assertEqual(
                method_return,
                expected_value,
                msg='Parameter {} out of range'.format(parameter)
                )

from mock import MagicMock, PropertyMock
from unittest import TestCase

from borescope.modules.device_gy87 import Gy87

class TestGY87(TestCase):
    methods = {
        'get_gyro': {
            'zero': (0, 0, 0),
            'read_value_ok': [255, 54, 0, 225, 0, 47],
            'measured_value_ok': (-1.54198, 1.71755, 0.35877), # by mean of scale factor
            'read_value_bus_not_present': [None, None, None, None, None, None],
            'measured_value_bus_not_present': (0.0, 0.0, 0.0),
            'read_value_bus_broken': [255, 54, 0, None, None, None],
            'measured_value_bus_broken': (0.0, 0.0, 0.0),
        },
        'get_gyro_rad': {
            'zero': (0, 0, 0),
            'read_value_ok': [255,54,0,225,0,47],
            'measured_value_ok': (-0.02691, 0.02997, 0.00626), # by mean of scale factor
            'read_value_bus_not_present': [None, None, None, None, None, None],
            'measured_value_bus_not_present': (0.0, 0.0, 0.0),
            'read_value_bus_broken': [255, 54, 0, None, None, None],
            'measured_value_bus_broken': (0.0, 0.0, 0.0),
        },
        'get_accel': {
            'zero': (0, 0, 0),
            'read_value_ok': [6, 36, 205, 128, 213, 92],
            'measured_value_ok': (0.09594, -0.78906, -0.66625), # by mean of scale factor
            'read_value_bus_not_present': [None, None, None, None, None, None],
            'measured_value_bus_not_present': (0.0, 0.0, 1.0),
            'read_value_bus_broken': [6, 36, 205, None, None, None],
            'measured_value_bus_broken': (0.0, 0.0, 1.0),
        },
        'setup': {
            'read_value_bus_ok': '',
            'measured_value_bus_ok': 'gy87',
            'read_value_bus_not_present': None,
            'measured_value_bus_not_present': None,
        },
    }

    @classmethod
    def setUpClass(cls):
        cls.gy87 = Gy87(i2c_port=None)
        cls.gy87.i2c.bus = MagicMock()

    def setUp(self):
        self.gy87_i2c_read_mock = MagicMock(return_value=0)
        self.set_read_byte_data()

    def set_read_byte_data(self, mock=None, return_value=0):
        if not mock:
            mock = self.gy87_i2c_read_mock
        mock.return_value = return_value
        self.gy87.i2c.bus.read_byte_data = mock

    def set_simple_return(self, mock=None, return_value=0):
        if not mock:
            mock = MagicMock(return_value=0)
        mock.return_value = return_value

        return mock

    def set_multiples_read_byte_data(self, mock=None, side_effect=None):
        if not mock:
            mock = self.gy87_i2c_read_mock
        mock.side_effect = side_effect
        self.gy87.i2c.bus.read_byte_data = mock

    def single_param_get_test(self, method_to_test, *args):
        self.set_multiples_read_byte_data(side_effect=self.methods
                                          .get(method_to_test).get('read_value_ok'))

        if len(args) > 0:
            mocked_value = getattr(self.gy87, method_to_test)(args[0])
        else:
            mocked_value = getattr(self.gy87, method_to_test)()

        self.assertEqual(mocked_value,
                self.methods.get(method_to_test).get('measured_value_ok'),
                msg=f'Value {method_to_test} out of range')


    def multiples_param_get_test(self, method_to_test, *args):
        self.assertEqual(getattr(self.gy87,
                                 method_to_test)(),
                                 self.methods.get(method_to_test).get('zero'),
                                 msg=f'Value {method_to_test} out of range')

        self.set_multiples_read_byte_data(
                side_effect=self.methods.get(method_to_test).get('read_value_ok'))

        mocked_value = getattr(self.gy87, method_to_test)()
        measured_value = self.methods.get(method_to_test).get('measured_value_ok')

        for mo, me in zip(mocked_value, measured_value):
            self.assertAlmostEqual(mo, me, 4,
                    msg=f'Value {method_to_test} out of range')

    def multiples_param_get_enum_test(self, method_to_test, read_value, measured_value, *args):
        self.set_multiples_read_byte_data(
                side_effect=self.methods.get(method_to_test).get(read_value))

        mocked_value = getattr(self.gy87, method_to_test)()
        measured_value = self.methods.get(method_to_test).get(measured_value)

        for mo, me in zip(mocked_value, measured_value):
            self.assertAlmostEqual(mo, me, 4,
                    msg=f'Value {method_to_test} out of range')


    def single_param_get_enum_test(self, method_to_test, read_value, measured_value, *args):
        self.set_multiples_read_byte_data(side_effect=self.methods
                                          .get(method_to_test).get(read_value))

        if len(args) > 0:
            mocked_value = getattr(self.gy87, method_to_test)(args[0])
        else:
            mocked_value = getattr(self.gy87, method_to_test)()

        self.assertEqual(mocked_value,
                self.methods.get(method_to_test).get(measured_value),
                msg=f'Value {method_to_test} out of range')

    def test_get_gyro(self):
        self.multiples_param_get_test('get_gyro')

    def test_get_gyro_rad(self):
        self.multiples_param_get_test('get_gyro_rad')

    def test_get_accel(self):
        self.multiples_param_get_test('get_accel')

    def test_get_gyro_bus_not_present(self):
        self.multiples_param_get_enum_test('get_gyro',
                                        'read_value_bus_not_present',
                                        'measured_value_bus_not_present')

    def test_get_gyro_bus_broken(self):
        self.multiples_param_get_enum_test('get_gyro',
                                        'read_value_bus_broken',
                                        'measured_value_bus_broken')

    def test_get_gyro_rad_bus_not_present(self):
        self.multiples_param_get_enum_test('get_gyro_rad',
                                        'read_value_bus_not_present',
                                        'measured_value_bus_not_present')

    def test_get_gyro_rad_bus_broken(self):
        self.multiples_param_get_enum_test('get_gyro_rad',
                                        'read_value_bus_broken',
                                        'measured_value_bus_broken')

    def test_get_accel_bus_not_present(self):
        self.multiples_param_get_enum_test('get_accel',
                                        'read_value_bus_not_present',
                                        'measured_value_bus_not_present')

    def test_get_accel_bus_broken (self):
        self.multiples_param_get_enum_test('get_accel',
                                        'read_value_bus_broken',
                                        'measured_value_bus_broken')

    def test_setup_bus_present_and_not_present(self):
        self.gy87.i2c = self.set_simple_return(return_value=None)

        method_to_test = 'setup'
        test_event = 'measured_value_bus_ok'
        mocked_value = getattr(self.gy87, method_to_test)()

        self.assertEqual(mocked_value,
                self.methods.get(method_to_test).get(test_event),
                msg=f'Value {method_to_test} out of range')


        self.gy87.i2c.bus = None

        method_to_test = 'setup'
        test_event = 'measured_value_bus_not_present'
        mocked_value = getattr(self.gy87, method_to_test)()

        self.assertEqual(mocked_value,
                self.methods.get(method_to_test).get(test_event),
                msg=f'Value {method_to_test} out of range')


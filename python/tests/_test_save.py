import tempfile
import os
#  from os.path import dirname, join
from shutil import copy
from unittest import TestCase
from borescope.modules import save
from types import SimpleNamespace


class TestSave(TestCase):
    @property
    def hardcoded_options(self):
        return {"imu": "true",
                "camera": "/dev/video1",
                "i2c_port": "2",
                "display_title": "false",
                "display_angles": "true",
                "intensity": "200",
                "pwm": "200",
                "display_horizon": "true",
                "horizon_thickness": "1",
                "horizon_color": [0, 255,0]
                }

    def test_save_all(self):
        temp_file = tempfile.NamedTemporaryFile().name
        config_file_path = os.path.join(os.environ['HOME'],
            '.config',
            'borescope',
            'borescope.conf')
        copy(config_file_path, temp_file)
        options = SimpleNamespace(**self.hardcoded_options)
        save.save_all(options, temp_file)
        with open(temp_file, 'r') as file:
            for line in file.readlines():
                key_val = line.split(" = ")
                if len(key_val) != 2:
                    continue
                if key_val[0] in self.hardcoded_options.keys():
                    if key_val[0] == "horizon_color":
                        r, g, b = self.hardcoded_options.get(key_val[0])
                        self.assertEqual(key_val[-1].strip(), f'{r}-{g}-{b}')
                    else:
                        self.assertEqual(key_val[-1].strip(), self.hardcoded_options.get(key_val[0]))
                    continue
                self.assertNotEqual(key_val[-1].strip(), self.hardcoded_options.get(key_val[0]))

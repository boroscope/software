borescope package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   borescope.bstester
   borescope.gui
   borescope.modules

Submodules
----------

borescope.bs module
-------------------

.. automodule:: borescope.bs
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: borescope
   :members:
   :undoc-members:
   :show-inheritance:

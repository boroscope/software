# Contributing to Borescope

Thanks for contribute!

The following are guidelines for contributing to Borescope Project. These are guidelines, not rules. Use your best judgment, and feel free to propose changes to this document.

## Branching

For developers:

Branche names should start with `fix-issue-#` and finish with issue number.

## Coding Style

Code should follow PEP-8 Style Guide. Use `pylint` to measure the _quality_ of the code.

## Documenting

Borescope App uses Sphinx to automatically build documentation.

To build documentation locally, from borescope root directory go to `docs` directory

```
$ ls
bash  build  CONTRIBUTING.md  docs  LICENSE  python  README.md  requirements.txt  setup.py
$ cd docs
$ make html
```

After build, go to `build/html` directory and run the follow command

```
$ cd build/html
$ realpath index.html
```

Copy the output of last command in your favorite web browser and the documentation will be shown

If everything looks good, push changes and after a few minutes you will see the new documentation in
[readthedocs](https://borescope.readthedocs.io/) server

To skip CI when documentation is added use this option on push event:

```
$ git push -o ci.skip
```


# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0-rc.3] - 2024-12-24
- Only capture and show front camera

## [2.0.0-rc.2] - 2024-12-22
- Add background image all windows
- Add CIII and UTN logos to main window
- Change GPIO from RPi module for GPIO from interface_gpio
- Add method for set no connected pins as input
- Remove hardcoded style in systemWindow class

## [2.0.0-rc.1] - 2024-08-22
- Add GPIO.PWM methods to use LED drivers from ui_camera.py
- Change pins to match M1
- Change buttons order
- Cut string to show only the device and not all the path
- Added new methods to show the models and FPS of front, left and right cameras
- Remove string(value).lower() to not change the IDs capitalize letters
- Change raspberry pi model
- Change old version descriptions and add new developers
- Remove camera parameters from save.py in order to not change the borescope.conf file
- Add conditional to prevent opening of None cap
- Change resolution in the ui_main.py, ui_camera.py and ui_multicamera.py when they start without borescope configuration
- Change resolution in all ui scripts from bstester folder to open with the new resolution
- Change resolution in ui_test_camera.py from bstester folder to the new one
